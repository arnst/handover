#include "armar.h"

#include <VirtualRobot/IK/PoseQualityManipulability.h>
#include <VirtualRobot/Visualization/CoinVisualization/CoinVisualizationFactory.h>
#include <VirtualRobot/Visualization/CoinVisualization/CoinVisualization.h>

using namespace VirtualRobot;

Armar::Armar(SoSeparator *sceneSep) : Actor(sceneSep)
{
    handoverSpaceSep = new SoSeparator();
    handoverSpaceSep->ref();
    objectSep = new SoSeparator();
    objectSep->ref();
    sceneSep->addChild(handoverSpaceSep);
    sceneSep->addChild(objectSep);

    visualizeHS = true;
    visualizeObject = true;
    objectSide = Actor::NO_TCP;
    maxCacheSize = 500;
}

Armar::~Armar()
{
    handoverSpaceSep->unref();
    objectSep->unref();
}

void Armar::loadRobot(QString fileName)
{
    Actor::loadRobot(fileName);
    handoverSpace.reset(new WorkspaceRepresentation(robot));
}

void Armar::visualize()
{
    Actor::visualize();

    handoverSpaceSep->removeAllChildren();
    if (visualizeHS) {
        handoverSpaceSep->addChild(CoinVisualizationFactory::getCoinVisualization(handoverSpace, VirtualRobot::ColorMap(VirtualRobot::ColorMap::eHot)));
    }
    objectSep->removeAllChildren();
    if (visualizeObject && object && grasp) {
        object->setGlobalPose(grasp->getTargetPoseGlobal(robot));
        objectSep->addChild(CoinVisualizationFactory::getCoinVisualization(object, SceneObject::Full));
    }
}

void Armar::visualize(SoNode *wsLeft, SoNode *wsRight, SoNode* hs, SoNode* obj)
{
    Actor::visualize(wsLeft, wsRight);

    handoverSpaceSep->removeAllChildren();
    if (visualizeHS) {
        handoverSpaceSep->addChild(hs);
    }
    objectSep->removeAllChildren();
    if (visualizeObject && object && grasp) {
        objectSep->addChild(obj);
    }
}

void Armar::resetHandoverSpace()
{
    handoverSpace->reset();
}

void Armar::enableVisualizationHS(bool enable)
{
    visualizeHS = enable;
}

WorkspaceRepresentationPtr Armar::getHandoverSpace()
{
    return handoverSpace;
}

void Armar::setHandoverSpace(WorkspaceRepresentationPtr hs)
{
    this->handoverSpace = hs;
}

QString Armar::getName()
{
    return "Armar";
}

bool Armar::isEnabledVisualizationHS()
{
    return visualizeHS;
}

bool Armar::isEnabledVisualizationObject()
{
    return visualizeObject;
}

void Armar::equip(Actor::TCP side, ManipulationObjectPtr object)
{
    if (!object) return;

    unEquip();
    std::string eefString = (side == LEFT_TCP) ? "Hand L" : "Hand R";
    this->object = object;
    this->objectSide = side;

    if (!object->hasGraspSet(robot->getType(), eefString)) {
        VR_WARNING << "No grasps found for {Object: " << object->getName() << ", EEF: " << eefString << "}" << endl;
    }
}

void Armar::equip(ManipulationObjectPtr object, GraspPtr grasp)
{
    if (!object || !grasp) return;

    unEquip();
    this->object = object;
    this->grasp = grasp;
    this->objectSide = (grasp->getEefName() == "Hand L") ? Actor::LEFT_TCP : Actor::RIGHT_TCP;

    robot->getEndEffector(grasp->getEefName())->openActors();
    object->setGlobalPose(grasp->getTargetPoseGlobal(robot));
    robot->getEndEffector(grasp->getEefName())->closeActors(object);
}

void Armar::setGrasp(GraspPtr grasp)
{
    if (!object) return;

    this->grasp = grasp;
    objectSide = getTcpSide(grasp->getEefName());

    robot->getEndEffector(grasp->getEefName())->openActors();
    object->setGlobalPose(grasp->getTargetPoseGlobal(robot));
    robot->getEndEffector(grasp->getEefName())->closeActors(object);
}

void Armar::unEquip()
{
    object.reset();
    grasp.reset();
    if (robot->hasEndEffector(getEEF(objectSide))) {
        robot->getEndEffector(getEEF(objectSide))->openActors();
    }
    objectSide = Actor::NO_TCP;
}

void Armar::transformRobot(float x, float y, float z, float rotX, float rotY, float rotZ)
{
    Actor::transformRobot(x, y, z, rotX, rotY, rotZ);
    if (object && grasp) {
        object->setGlobalPose(grasp->getTargetPoseGlobal(robot));
    }
    // handover-space is only valid for the pose it was created for
    if (handoverSpace->getData()) {
        handoverSpace->reset();
    }
}

ManipulationObjectPtr Armar::getObject() const
{
    return object;
}

GraspPtr Armar::getGrasp()
{
    return grasp;
}

Actor::TCP Armar::getObjectSide()
{
    return objectSide;
}

bool Armar::cacheHSPose(Eigen::Matrix4f &pose)
{
    if (cachedHSPoses.size() < maxCacheSize)
    {
        cachedHSPoses.append(pose);
        return true;
    }
    return false;
}

Eigen::Matrix4f Armar::getRandomCachedHSPose()
{
    srand(time(NULL));
    int index = rand() % cachedHSPoses.size();
    Eigen::Matrix4f pose = cachedHSPoses.at(index);
    return pose;
}

bool Armar::hasCachedHSPose()
{
    return cachedHSPoses.size() > 0;
}

int Armar::getCurrentCacheSize()
{
    return cachedHSPoses.size();
}

bool Armar::clearCache()
{
    cachedHSPoses.clear();
}

std::string Armar::getEEF(Actor::TCP side)
{
    std::string eef = "";
    if (side == Actor::LEFT_TCP) {
        eef = "Hand L";
    }
    else if (side == Actor::RIGHT_TCP) {
        eef = "Hand R";
    }
    return eef;
}

Actor::TCP Armar::getTcpSide(std::string eef)
{
    Actor::TCP side = Actor::NO_TCP;

    if (eef == getEEF(Actor::LEFT_TCP)) {
        side = Actor::LEFT_TCP;
    }
    else if (eef == getEEF(Actor::RIGHT_TCP)) {
        side = Actor::RIGHT_TCP;
    }

    return side;
}

void Armar::resetRobotConfig()
{
    Actor::resetRobotConfig();

    if (grasp)
    {
        robot->getEndEffector(grasp->getEefName())->openActors();
        object->setGlobalPose(grasp->getTargetPoseGlobal(robot));
        robot->getEndEffector(grasp->getEefName())->closeActors(object);
    }
}


void Armar::enableVisualization(bool enable)
{
    sceneSep->removeAllChildren();

    if (enable)
    {
        sceneSep->addChild(handoverSpaceSep);
        sceneSep->addChild(objectSep);
        sceneSep->addChild(robotSep);
        sceneSep->addChild(manipRightSep);
        sceneSep->addChild(manipLeftSep);
    }
}
