#ifndef SETTINGS_H
#define SETTINGS_H

class Settings
{
public:
    struct Workspace {
        enum Measure {
            MANIPULABILITY,
            EXT_MANIPULABILITY,
            COMFORT,
            REACHABILITY
        };

        static Measure measureHuman;
        static Measure measureArmar;
        static unsigned int samplesHuman;
        static unsigned int samplesArmar;
        static unsigned int discrStepTrans;
        static float discrStepRot;
        static unsigned int smooth;
        static unsigned int samplesExtend;
    }; // Workspace

    struct Handoverspace {
        static unsigned int discrStepTrans;
        static float discrStepRot;
    }; // Handoverspace

private:
    Settings();
};

#endif // SETTINGS_H
