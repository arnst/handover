#include "actor.h"

#include <VirtualRobot/Import/RobotImporterFactory.h>
#include <VirtualRobot/RobotNodeSet.h>
#include <VirtualRobot/Visualization/CoinVisualization/CoinVisualizationFactory.h>
#include <VirtualRobot/Visualization/CoinVisualization/CoinVisualization.h>
#include <VirtualRobot/XML/RobotIO.h>

#include <Inventor/nodes/SoTransform.h>
#include <Inventor/misc/SoChildList.h>

#include <QFileInfo>

Actor::Actor(SoSeparator *sceneSep)
{
    this->sceneSep = sceneSep;
    robotSep = new SoSeparator();
    robotSep->ref();
    manipRightSep = new SoSeparator();
    manipRightSep->ref();
    manipLeftSep = new SoSeparator();
    manipLeftSep->ref();
    sceneSep->addChild(robotSep);
    sceneSep->addChild(manipRightSep);
    sceneSep->addChild(manipLeftSep);
    visualizeWS = true;
}

Actor::~Actor()
{
    robotSep->unref();
    manipRightSep->unref();
    manipLeftSep->unref();
}

void Actor::loadRobot(QString fileName)
{
//    std::string suffix = QFileInfo(fileName).suffix().toStdString();
//    RobotImporterFactoryPtr importer = RobotImporterFactory::fromFileExtension(suffix, NULL);
//    robot.reset();
//    robot = importer->loadFromFile(fileName.toStdString());

    robot = RobotIO::loadRobot(fileName.toStdString());
    startConfig = robot->getConfig();

    manipRight.reset(new Manipulability(robot));
    manipLeft.reset(new Manipulability(robot));
}

RobotPtr Actor::getRobot()
{
    return robot;
}
//TODO Auch für NO_TCP
void Actor::setManipulability(Actor::TCP side, ManipulabilityPtr manip)
{
    if (side == Actor::LEFT_TCP)
    {
        this->manipLeft = manip;
    }
    else if (side == Actor::RIGHT_TCP)
    {
        this->manipRight = manip;
    }
}
//TODO Auch für NO_TCP
ManipulabilityPtr Actor::getManipulability(Actor::TCP side)
{
    if (side == Actor::LEFT_TCP)
    {
        return manipLeft;
    }
    else if (side == Actor::RIGHT_TCP)
    {
        return manipRight;
    }
}

ManipulabilityPtr Actor::getManipLeft()
{
    return manipLeft;
}

ManipulabilityPtr Actor::getManipRight()
{
    return manipRight;
}

void Actor::visualize()
{
    robotSep->removeAllChildren();
    robotSep->addChild(robot->getVisualization<CoinVisualization>(SceneObject::Full)->getCoinVisualization());

    manipLeftSep->removeAllChildren();
    manipRightSep->removeAllChildren();
    if (visualizeWS)
    {
        manipLeftSep->addChild(CoinVisualizationFactory::getCoinVisualization(manipLeft, VirtualRobot::ColorMap(VirtualRobot::ColorMap::eHot)));
        manipRightSep->addChild(CoinVisualizationFactory::getCoinVisualization(manipRight, VirtualRobot::ColorMap(VirtualRobot::ColorMap::eHot)));
    }
}

void Actor::visualize(SoNode *wsLeft, SoNode *wsRight)
{
    robotSep->removeAllChildren();
    robotSep->addChild(robot->getVisualization<CoinVisualization>(SceneObject::Full)->getCoinVisualization());

    manipLeftSep->removeAllChildren();
    manipRightSep->removeAllChildren();
    if (visualizeWS)
    {
        manipLeftSep->addChild(wsLeft);
        manipRightSep->addChild(wsRight);
    }
}

void Actor::resetWorkspace()
{
    manipLeft->reset();
    manipRight->reset();
}

void Actor::enableVisualizationWS(bool enable)
{
    visualizeWS = enable;
}

bool Actor::isEnabledVisualizationWS()
{
    return visualizeWS;
}

void Actor::loadWorkspace(Actor::TCP side, QString fileName)
{
    switch(side)
    {
    case Actor::LEFT_TCP:
        loadWorkspaceLeft(fileName);
        break;
    case Actor::RIGHT_TCP:
        loadWorkspaceRight(fileName);
        break;
    case Actor::BOTH_TCP:
        VR_WARNING << "Trying to load workspace <" << fileName.toStdString() << "> for both tcp sides. Cancelled." << endl;
        return;
    default:
        return;
    }
}

void Actor::loadWorkspaceLeft(QString fileName)
{
    manipLeft.reset(new Manipulability(robot));
    manipLeft->load(fileName.toStdString());
}

void Actor::loadWorkspaceRight(QString fileName)
{
    manipRight.reset(new Manipulability(robot));
    manipRight->load(fileName.toStdString());
}

void Actor::saveWorkspace(Actor::TCP side, QString fileName)
{
    switch(side)
    {
    case Actor::LEFT_TCP:
        saveWorkspaceLeft(fileName);
        break;
    case Actor::RIGHT_TCP:
        saveWorkspaceRight(fileName);
        break;
    case Actor::BOTH_TCP:
        VR_WARNING << "Trying to save workspace <" << fileName.toStdString() << "> of both tcp sides. Cancelled." << endl;
        return;
    default:
        return;
    }
}

void Actor::saveWorkspaceLeft(QString fileName)
{
    if (manipLeft->getData()) manipLeft->save(fileName.toStdString());
}

void Actor::saveWorkspaceRight(QString fileName)
{
    if (manipRight->getData()) manipRight->save(fileName.toStdString());
}

void Actor::smoothWorkspace(unsigned int minNeighbors)
{
    if (manipLeft->getData())  manipLeft->smooth(minNeighbors);
    if (manipRight->getData()) manipRight->smooth(minNeighbors);
}

void Actor::resetRobotConfig()
{
    robot->setConfig(startConfig);
}

void Actor::transformRobot(float transX, float transY, float transZ, float rotX, float rotY, float rotZ)
{
    // we want to rotate around the robots local axis
    Eigen::Matrix4f globalPose = robot->getGlobalPose();
    float x = globalPose(0,3);
    float y = globalPose(1,3);
    float z = globalPose(2,3);

    globalPose.block<3,1>(0,3) = Eigen::Vector3f::Zero();
    robot->setGlobalPose(transform(globalPose, x + transX, y + transY, z + transZ, rotX, rotY, rotZ));
}


Eigen::Matrix4f Actor::transform(const Eigen::Matrix4f &pose, float transX, float transY, float transZ, float rotX, float rotY, float rotZ)
{
    Eigen::Affine3f transform;
    transform = Eigen::Translation<float, 3>(transX, transY, transZ) *
                Eigen::AngleAxisf(rotX, Eigen::Vector3f(1, 0, 0)) *
                Eigen::AngleAxisf(rotY, Eigen::Vector3f(0, 1, 0)) *
                Eigen::AngleAxisf(rotZ, Eigen::Vector3f(0, 0, 1));
    return transform * pose;
}
