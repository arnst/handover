#ifndef ACTORVISUALIZATIONFACTORY_H
#define ACTORVISUALIZATIONFACTORY_H

#include <VirtualRobot/Visualization/CoinVisualization/CoinVisualizationFactory.h>

using namespace VirtualRobot;

/**
 * Provides custom visualizations that are not provided by VirtualRobot::CoinVisualizationFactory.
 */
class ActorVisualizationFactory
{
public:
    enum ColorMode
    {
        SUMMED, // summed angles
        MAX // max entry in a 3D voxel
    };

    struct WorkspaceCutSummedAngles
    {
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
        WorkspaceRepresentationPtr workspace;
        Eigen::MatrixXi entries;
        float posZ; // global
        float minBounds[2]; // in global coord system
        float maxBounds[2]; // in global coord system
    };
    typedef boost::shared_ptr<WorkspaceCutSummedAngles> WorkspaceCutSummedAnglesPtr;

    static SoNode* getWorkspaceVisualization(WorkspaceRepresentationPtr ws, float cutZ, const VirtualRobot::ColorMap cm, bool transformToGlobalPose = true, ColorMode mode = SUMMED);

private:
    ActorVisualizationFactory() {}

    static ActorVisualizationFactory::WorkspaceCutSummedAnglesPtr createCut(WorkspaceRepresentationPtr ws, float posZ, float cellSize, ColorMode mode = SUMMED);
    static SoNode* getCutVisualization(WorkspaceCutSummedAnglesPtr cut, int maxEntry, VirtualRobot::ColorMap cm, const Eigen::Vector3f& normal = Eigen::Vector3f::UnitZ());

    static void smoothWorkspaceCut(WorkspaceCutSummedAnglesPtr cut);
};

#endif // ACTORVISUALIZATIONFACTORY_H
