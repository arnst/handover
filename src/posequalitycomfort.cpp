#include "posequalitycomfort.h"

#include <cmath>

#include <VirtualRobot/Nodes/RobotNode.h>
#include <VirtualRobot/Nodes/RobotNodeRevolute.h>

PoseQualityComfort::PoseQualityComfort(RobotNodeSetPtr rns, RobotNodeSetPtr rnsBodies) : PoseQualityMeasurement(rns)
{
    name = "PoseQualityComfort";
    this->rnsBodies = rnsBodies;
    logging = false;
    obstacleAlpha = 2;
    obstacleBeta = 2;

    checkForMaxTorque(200);
    extManip.reset(new PoseQualityExtendedManipulability(rns));
    extManip->considerObstacles(true, obstacleAlpha, obstacleBeta);
}

float PoseQualityComfort::getPoseQuality()
{
    float netTorque = getNetTorque();
    float torqueIndex = netTorque / maxTorque;

    if (torqueIndex > 1) {
        torqueIndex = 1;
    }
    if (torqueIndex < 0) {
        torqueIndex = 0;
    }

    torqueIndex = 1 - torqueIndex;
    highlight("Torque index = " + QString::number(torqueIndex).toStdString(), 0);
    log("Abs. Torque (total) = " + QString::number(netTorque).toStdString() + " Nm", 1);

    float extManipResult = extManip->getPoseQuality();
    highlight("Ext. Manip. = " + QString::number(extManipResult).toStdString(), 0);

    float distance = obstacleDir.norm();
    log("TCP Distance = " + QString::number(distance).toStdString() + "mm", 1);
    log("alpha = " + QString::number(obstacleAlpha).toStdString() + ", beta = " + QString::number(obstacleBeta).toStdString(), 1);

    float poseQuality = sqrt(torqueIndex * extManipResult);
    highlight("Pose Quality = " + QString::number(poseQuality).toStdString(), 0);
    return poseQuality;
}

float PoseQualityComfort::getNetTorque()
{
    float netTorque = 0;
    std::vector<RobotNodePtr> nodes = rns->getAllRobotNodes();
    std::vector<RobotNodePtr> bodies = rnsBodies->getAllRobotNodes();
    std::vector<float> torqueAllJoints;
    log("Kinematic chain: " + rns->getName(), 0);
    log("Estimated Max Torque = " + QString::number(maxTorque).toStdString() + " Nm", 0);

    for (int i = 0; i < nodes.size(); i++)
    {
        RobotNodePtr rn = nodes[i];
        if (!rn->isRotationalJoint())
        {
            VR_WARNING << "Skipping joint <" << rn->getName() << ">: non-rotational joint." << endl;
            continue;
        }

        RobotNodeRevolutePtr rnRevolute = boost::dynamic_pointer_cast<RobotNodeRevolute>(rn);
        Eigen::Vector3f axisGlobal = rnRevolute->getJointRotationAxis();
        Eigen::Vector3f jointPosGlobal = rnRevolute->getGlobalPose().block(0,3,3,1);
        axisGlobal.normalize();
        float torqueCurrentJoint = 0;

        // logging
        float x = (fabs(axisGlobal[0]) > 1e-6) ? axisGlobal[0] : 0;
        float y = (fabs(axisGlobal[1]) > 1e-6) ? axisGlobal[1] : 0;
        float z = (fabs(axisGlobal[2]) > 1e-6) ? axisGlobal[2] : 0;
        log(QString::number(i + 1).toStdString() + ". Joint: " + rn->getName(), 1);
        log("Gl. Pos. = (" + QString::number(jointPosGlobal[0]).toStdString() + ", " + QString::number(jointPosGlobal[1]).toStdString() + ", " + QString::number(jointPosGlobal[2]).toStdString() + ")", 2);
        log("Rotation axis = (" + QString::number(x).toStdString() + ", " + QString::number(y).toStdString() + ", " + QString::number(z).toStdString() + ")", 2);

        for (int n = 0; n<bodies.size(); n++)
        {
            RobotNodePtr rnBody = bodies[n];
            if (rnBody->getMass() <= 0)
            {
                VR_WARNING << "Skipping body <" << rnBody->getName() << ">: negative mass." << endl;
                continue;
            }

            if (isParent(rnBody, rn))
            {
                // distance vector r
                Eigen::Vector3f comGlobal = rnBody->getCoMGlobal();
                VirtualRobot::MathTools::BaseLine<Eigen::Vector3f> l(jointPosGlobal, axisGlobal);
                Eigen::Vector3f pointOnAxis = MathTools::nearestPointOnLine<Eigen::Vector3f>(l,comGlobal);
                Eigen::Vector3f r = comGlobal - pointOnAxis; // distance-vector from joint to CoM

                Eigen::Vector3f F(0,0,-9.81);
                r *= 0.001f; // mm -> m
                F *= rnBody->getMass();
                Eigen::Vector3f M = r.cross(F);
                float torqueCurrentBody = M.dot(axisGlobal);
                torqueCurrentJoint += torqueCurrentBody;
            }
        }
        torqueCurrentJoint = fabs(torqueCurrentJoint);
        log("Torque = " + QString::number(torqueCurrentJoint).toStdString() + " Nm", 2);
        torqueAllJoints.push_back(torqueCurrentJoint);
    }

    for(int i = 0; i < torqueAllJoints.size(); i++){
        netTorque += torqueAllJoints[i];
    }
    log("");

    return netTorque;
}

void PoseQualityComfort::checkForMaxTorque(int steps)
{
    std::vector<float> config;
    rns->getJointValues(config);
    RobotPtr robot = rns->getRobot();
    bool visuSate = robot->getUpdateVisualizationStatus();
    robot->setUpdateVisualization(false);

    srand(time(NULL));
    for (unsigned int i = 0; i < steps; i++)
    {
        randomizeRnsConfig();
        float netTorque = getNetTorque();
        if (maxTorque < netTorque) {
            maxTorque = netTorque;
        }
    }

    maxTorque *= 1.05;
    robot->setUpdateVisualization(visuSate);
    rns->setJointValues(config);
}

void PoseQualityComfort::randomizeRnsConfig()
{
    float minJointValue = 0;
    float maxJointValue = 0;
    std::vector<float> v(rns->getSize());

    for (int i = 0; i < rns->getSize(); i++)
    {
        minJointValue = (*rns)[i]->getJointLimitLo();
        maxJointValue = (*rns)[i]->getJointLimitHi();
        float r = (float) rand() / (float) RAND_MAX;
        v[i] = minJointValue + ((maxJointValue - minJointValue) * r);
    }

    rns->setJointValues(v);
}

void PoseQualityComfort::setLogWidget(QListWidget *logWidget)
{
    if (logWidget) {
        this->logWidget = logWidget;
        logging = true;
    }
}

bool PoseQualityComfort::isParent(RobotNodePtr child, RobotNodePtr parent)
{
    if (!child || !parent)
        return false;

    SceneObjectPtr currentParent = child;
    while (currentParent)
    {
        currentParent = currentParent->getParent();
        if (currentParent == parent)
            return true;
    }
    return false;
}


void PoseQualityComfort::log(std::string text, int lvl)
{
    if (!logging)
        return;

    QString prefix("");
    for (int i = 0; i < lvl; i++) {
        prefix += "     ";
    }

    logWidget->addItem(prefix + QString::fromStdString(text));
}

void PoseQualityComfort::highlight(std::string text, int lvl)
{
    if (!logging)
        return;

    log(text, lvl);
    QListWidgetItem* item = logWidget->item(logWidget->count() - 1);
    item->setBackgroundColor(Qt::yellow);
    QFont font(item->font());
    font.setBold(true);
    item->setFont(font);
}

void PoseQualityComfort::setObstacleDistanceVector(const Eigen::Vector3f &directionSurfaceToObstance)
{
    PoseQualityMeasurement::setObstacleDistanceVector(directionSurfaceToObstance);
    extManip->setObstacleDistanceVector(directionSurfaceToObstance);
}

void PoseQualityComfort::considerObstacles(bool enable, float alpha, float beta)
{
    this->obstacleAlpha = alpha;
    this->obstacleBeta = beta;
    extManip->considerObstacles(enable, obstacleAlpha, obstacleBeta);
}
