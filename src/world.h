#ifndef WORLD_H
#define WORLD_H

#include "human.h"
#include "armar.h"
#include "worldlistener.h"
#include "worldrenderer.h"

#include "VirtualRobot/ManipulationObject.h"

#include <QtCore>

#include <boost/enable_shared_from_this.hpp>

using namespace VirtualRobot;

class World : public QObject, public boost::enable_shared_from_this<World>
{

    Q_OBJECT

public:
    World(HumanPtr human, ArmarPtr armar);
    void addListener(WorldListener* listener);

    HumanPtr getHuman() const;
    void setHuman(const HumanPtr human);

    ArmarPtr getArmar() const;
    void setArmar(const ArmarPtr armar);

    ManipulationObjectPtr getEnvironment() const;
    void setEnvironment(const ManipulationObjectPtr environment);

    WorldRendererPtr getRenderer() const;
    void setRenderer(const WorldRendererPtr renderer);

    Eigen::Matrix4f getHandoverPose() const;
    void setHandoverPose(const Eigen::Matrix4f globalPose);

private:
    HumanPtr human;
    ArmarPtr armar;
    Eigen::Matrix4f handoverPose;
    ManipulationObjectPtr environment;
    QList<WorldListener*> listeners;
    WorldRendererPtr renderer;

public slots:
    void update();
    void lock();
    void unlock();
    void render();
};

typedef boost::shared_ptr<World> WorldPtr;

#endif // WORLD_H
