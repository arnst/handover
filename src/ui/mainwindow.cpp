#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "../tasks/taskcreatemanipulability.h" // TODO tasks weg
#include "../posequalitycomfort.h"
#include "../tasks/taskexecutor.h"
#include "../tasks/taskcreatehandoverspace.h"
#include "../tasks/taskvisualizehuman.h"
#include "../tasks/taskvisualizearmar.h"
#include "../tasks/taskloadworkspace.h"
#include "../tasks/tasksaveworkspace.h"
#include "../tasks/tasksmoothworkspace.h"
#include "../tasks/tasksolvehandoverik.h"
#include "../tasks/taskextendworkspace.h"
#include "../worldrenderer.h"

#include <math.h>

#include <VirtualRobot/RuntimeEnvironment.h>
#include <VirtualRobot/Import/RobotImporterFactory.h>
#include <VirtualRobot/Workspace/Reachability.h>
#include <VirtualRobot/RobotNodeSet.h>
#include <VirtualRobot/Visualization/CoinVisualization/CoinVisualizationFactory.h>
#include <VirtualRobot/IK/PoseQualityManipulability.h>
#include <VirtualRobot/IK/PoseQualityExtendedManipulability.h>
#include <VirtualRobot/XML/ObjectIO.h>
#include <VirtualRobot/ManipulationObject.h>
#include <VirtualRobot/IK/GenericIKSolver.h>

#include <QFileInfo>
#include <QThread>

#include <boost/make_shared.hpp>

using namespace VirtualRobot;
using namespace std;

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow)
{
    initSceneGraph();
    setupDataPaths();
    loadEnvironment();
    initActors();
    loadLadder();

    world = boost::make_shared<World>(human, armar);
    WorldRendererPtr renderer(new WorldRenderer(sceneSep));
    world->setRenderer(renderer);

    // synchronous
    human->visualize();
    armar->visualize();

    // for visualizationtasks
    qRegisterMetaType<HumanPtr>("HumanPtr");
    qRegisterMetaType<ArmarPtr>("ArmarPtr");

    setupUI();
    connectUI();
}

MainWindow::~MainWindow()
{
    sceneSep->unref();
    delete ui;
}

void MainWindow::initSceneGraph()
{
    sceneSep = new SoSeparator();
    sceneSep->ref();
    environmentSep = new SoSeparator();
    sceneSep->addChild(environmentSep);

    handoverPoseSep = new SoSeparator();
    sceneSep->addChild(handoverPoseSep);

    ladderSep = new SoSeparator();
    sceneSep->addChild(ladderSep);
}

void MainWindow::setupDataPaths()
{
    // find human model
    std::string fileName = "MMMData/Model/Winter/mmm2.xml";
    if (!RuntimeEnvironment::getDataFileAbsolute(fileName)) {
        cout << "WARNING! Could not find absolute path for: " << fileName << endl;
    }
    fileNameHuman = QString::fromStdString(fileName);

    // find armar model
    fileName = "robots/ArmarIII/ArmarIII.xml";
    if (!RuntimeEnvironment::getDataFileAbsolute(fileName)) {
        cout << "WARNING! Could not find absolute path for: " << fileName << endl;
    }
    fileNameArmar = QString::fromStdString(fileName);

    // find environment model
    fileName = "environment/KIT_kitchen.xml";
    if (!RuntimeEnvironment::getDataFileAbsolute(fileName)) {
        cout << "WARNING! Could not find absolute path for: " << fileName << endl;
    }
    fileNameEnvironment = QString::fromStdString(fileName);

    // find ladder
    fileName = "objects/Ladder.xml";
    if (!RuntimeEnvironment::getDataFileAbsolute(fileName)) {
        cout << "WARNING! Could not find absolute path for: " << fileName << endl;
    }
    fileNameLadder = QString::fromStdString(fileName);
}

void MainWindow::loadEnvironment()
{
    environment = ObjectIO::loadManipulationObject(fileNameEnvironment.toStdString());
}

void MainWindow::loadLadder()
{
    ladder = ObjectIO::loadManipulationObject(fileNameLadder.toStdString());
}

void MainWindow::initActors()
{
    // human
    SoSeparator* humanSep = new SoSeparator();
    sceneSep->addChild(humanSep);
    human.reset(new Human(humanSep));
    human->loadRobot(fileNameHuman);
    startPoseHuman = human->getRobot()->getGlobalPose();
    human->transformRobot(0 - 7000, 3000, 921.5f, 0, 0, -M_PI / 2);
//    human->transformRobot(-1650, -100, 921.5f + 550, 0, 0, -M_PI / 2);

    // armar
    SoSeparator* armarSep = new SoSeparator();
    sceneSep->addChild(armarSep);
    armar.reset(new Armar(armarSep));
    armar->loadRobot(fileNameArmar);
    startPoseArmar = armar->getRobot()->getGlobalPose();
    armar->transformRobot(1354.605 - 7000, 3000, 0, 0, 0, M_PI / 2);
//    armar->transformRobot(-1650, 500, 0, 0, 0, M_PI / 2);
}

void MainWindow::logComfort()
{
    RobotNodeSetPtr rns = human->getRobot()->getRobotNodeSet(ui->coBoxHumanKinematicChain->currentText().toStdString());
    RobotNodeSetPtr bodies = getBodies(rns);
    if (!rns || !bodies)
        return;

    if (!measure || measure->getRNS() != rns) {
        measure.reset(new PoseQualityComfort(rns, bodies));
    }
    ui->listComfortLog->clear();
    measure->setLogWidget(ui->listComfortLog);

    // here we determine the obstacle distance vector
    SceneObjectSetPtr staticColModel = human->getRobot()->getRobotNodeSet("LegsHipTorsoHead_ColModel");
    SceneObjectSetPtr dynamicColModel = human->getRobot()->getRobotNodeSet(rns->getTCP()->getName() == "LeftHandSegment_joint" ? "LeftHand_ColModel" : "RightHand_ColModel");

    int id1;
    int id2;
    Eigen::Vector3f p1;
    Eigen::Vector3f p2;
    float d = staticColModel->getCollisionChecker()->calculateDistance(staticColModel, dynamicColModel, p1, p2, &id1, &id2);
    Eigen::Matrix4f obstDistPos1 = Eigen::Matrix4f::Identity();
    Eigen::Matrix4f obstDistPos2 = Eigen::Matrix4f::Identity();
    obstDistPos1.block(0, 3, 3, 1) = p1;
    obstDistPos2.block(0, 3, 3, 1) = p2;

    // transform to tcp
    Eigen::Matrix4f p1_tcp = rns->getTCP()->toLocalCoordinateSystem(obstDistPos1);
    Eigen::Matrix4f p2_tcp = rns->getTCP()->toLocalCoordinateSystem(obstDistPos2);
    Eigen::Vector3f minDistVector = p1_tcp.block(0, 3, 3, 1) - p2_tcp.block(0, 3, 3, 1);

    measure->setObstacleDistanceVector(minDistVector);

    measure->getPoseQuality();
    ui->listComfortLog->scrollToBottom();
}

RobotNodeSetPtr MainWindow::getBodies(RobotNodeSetPtr rns)
{
    RobotNodeSetPtr bodies;
    if (human->getRobot()->hasRobotNodeSet(rns->getName() + "_Bodies")) {
        bodies = human->getRobot()->getRobotNodeSet(rns->getName() + "_Bodies");
    }
    return bodies;
}

bool MainWindow::hasBodies(RobotNodeSetPtr rns)
{
    return getBodies(rns) != NULL;
}

PoseQualityMeasurementPtr MainWindow::getMeasure(Settings::Workspace::Measure measure, RobotNodeSetPtr rns)
{
    PoseQualityMeasurementPtr ret;
    switch(measure)
    {
    case Settings::Workspace::MANIPULABILITY:
        ret.reset(new PoseQualityManipulability(rns));
        break;
    case Settings::Workspace::EXT_MANIPULABILITY:
        ret.reset(new PoseQualityExtendedManipulability(rns));
        ((PoseQualityExtendedManipulability*) ret.get())->considerObstacles(true, 4, 4);
        break;
    case Settings::Workspace::COMFORT:
        ret.reset(new PoseQualityComfort(rns, getBodies(rns)));
        break;
    default:
        VR_ERROR "Unknown Measure." << endl;
    }
    return ret;
}

void MainWindow::setHumanGazeLength(int mm)
{
    human->setGazeLength(mm);
}

void MainWindow::testHSPerformance(int graspCombinations)
{
//    if (!armar->getObject()) {
//        VR_ERROR << "No valid manipulation object found!" << endl;
//        return;
//    }

//    int countDiscr = 1;

//    float totalDuration = 0; // seconds
//    unsigned int totalVoxelFillCount = 0;
//    unsigned int totalVoxelEntries = 0;
//    unsigned int highestEntries = 0;
//    int steps = 100;
//    float durations[steps];
//    float voxelFillCounts[steps];

//    for (int i = 0; i < steps ; i++)
//    {
//        armar->resetHandoverSpace();
//        TaskExecutor* factory = new TaskExecutor();

//        int threads = QThread::idealThreadCount();
//        threads = (threads < 1) ? 1 : threads;
//        float range = 1.0f / threads;

//        boost::shared_ptr<boost::mutex> mutex(new boost::mutex());
//        for (int t = 0; t < threads; t++)
//        {
//            boost::shared_ptr<TaskCreateHandoverspace> task(new TaskCreateHandoverspace(armar, armar->getObjectSide(), human, graspCombinations));
//            QPair<float,float> interval(t * range, (t+1) * range);

//            task->setConcurrent(interval, mutex);
//            factory->addTask(task);
//            if (t < threads - 1) factory->newThread();
//        }
//        QElapsedTimer timer;
//        timer.start();
//        factory->run(); // synchron

//        float elapsed = timer.elapsed() / 1000.0f;
//        durations[i] = elapsed;
//        totalDuration += elapsed;
//        totalVoxelFillCount += armar->getHandoverSpace()->getData()->getVoxelFilledCount();
//        voxelFillCounts[i] = armar->getHandoverSpace()->getData()->getVoxelFilledCount();
//        highestEntries += armar->getHandoverSpace()->getMaxEntry();

//        WorkspaceRepresentationPtr hs = armar->getHandoverSpace();
//        for (int x = 0; x < hs->getNumVoxels(0); x++) {
//            for (int y = 0; y < hs->getNumVoxels(1); y++) {
//                for (int z = 0; z < hs->getNumVoxels(2); z++) {
//                    for (int a = 0; a < hs->getNumVoxels(3); a++) {
//                        for (int b = 0; b < hs->getNumVoxels(4); b++) {
//                            for (int c = 0; c < hs->getNumVoxels(5); c++) {
//                                totalVoxelEntries += hs->getVoxelEntry(x,y,z,a,b,c);
//                            }
//                        }
//                    }
//                }
//            }
//        }


//    }


//    for (int i = 0; i < countDiscr; i++)
//    {
//        float averageDuration = totalDuration / steps;
//        float averageVoxelFillCount = (float)totalVoxelFillCount / (float)steps;
//        float averageVoxelEntry = ((float)totalVoxelEntries / (float)steps) / averageVoxelFillCount;
//        float averageHighestEntry = (float)highestEntries / (float)steps;

//        float sumDurationsV = 0;
//        float sumVoxelfillCountsV = 0;
//        for (int j = 0; j < steps ; j++)
//        {
//            sumDurationsV += (durations[j] - averageDuration) * (durations[j] - averageDuration);
//            sumVoxelfillCountsV += (voxelFillCounts[j] - averageVoxelFillCount) * (voxelFillCounts[j] - averageVoxelFillCount);
//        }
//        float sdDuration = std::sqrt(sumDurationsV / (steps - 1));
//        float sdVoxelFillCount = std::sqrt(sumVoxelfillCountsV / (steps - 1));

//        cout << "***************************************************" << endl;
//        cout << "Grasp Combinations: " << graspCombinations << endl;
//        cout << "Discretization: " << Settings::Handoverspace::discrStepTrans << " mm   " << Settings::Handoverspace::discrStepRot << " rad" << endl;
//        cout << "Steps: " << steps << endl;
//        cout << "Total Duration : " << totalDuration << " s    Average Duration: " << averageDuration << " s" << endl;
//        cout << "Total Voxel Fill Count: " << totalVoxelFillCount << "    Average Voxel Fill Count: " << averageVoxelFillCount << endl;
//        cout << "Total Voxel entries: " << totalVoxelEntries << "    Average Voxel Entry: " << averageVoxelEntry << endl;
//        cout << "Total Highest Entries: " << highestEntries << "    Average Highest Entry: " << averageHighestEntry << endl;
//        cout << "Standard Deviation:    Duration: " << sdDuration << "    Voxel Fill Count: " << sdVoxelFillCount << endl;
//        cout << "***************************************************" << endl << endl << endl << endl << endl << endl << endl << endl;
//    }

}

void MainWindow::visualize()
{
    TaskExecutor* factory = new TaskExecutor();

    boost::shared_ptr<TaskVisualizeHuman> taskHuman(new TaskVisualizeHuman(human));
    taskHuman->setCutZ(ui->sliderWsHumanLeft->value() / 100.0, ui->sliderWsHumanRight->value() / 100.0);

    boost::shared_ptr<TaskVisualizeArmar> taskArmar(new TaskVisualizeArmar(armar));
    taskArmar->setCutZ(ui->sliderWsArmarLeft->value() / 100.0, ui->sliderWsArmarRight->value() / 100.0, ui->sliderHsVisu->value() / 100.0);

    factory->addTask(taskHuman);
    factory->newThread();
    factory->addTask(taskArmar);

    connect(taskHuman.get(), SIGNAL(resultReady(HumanPtr, SoNode*, SoNode*)), this, SLOT(handleResultHumanVisuWorker(HumanPtr,SoNode*,SoNode*)));
    connect(taskArmar.get(), SIGNAL(resultReady(ArmarPtr, SoNode*, SoNode*, SoNode*, SoNode*)), this, SLOT(handleResultArmarVisuWorker(ArmarPtr,SoNode*,SoNode*,SoNode*,SoNode*)));

    connect(factory, SIGNAL(started()), this, SLOT(lockRobotAccess()));
    connect(factory, SIGNAL(started()), ui->progressVisualization, SLOT(show()));
    connect(factory, SIGNAL(finished()), ui->progressVisualization, SLOT(hide()));
    connect(factory, SIGNAL(finished()), this, SLOT(unlockRobotAccess()));
    connect(factory, SIGNAL(finished()), factory, SLOT(deleteLater()));
    factory->start();
}

void MainWindow::visualizeArmar()
{
    TaskExecutor* factory = new TaskExecutor();
    boost::shared_ptr<TaskVisualizeArmar> taskArmar(new TaskVisualizeArmar(armar));
    taskArmar->setCutZ(ui->sliderWsArmarLeft->value() / 100.0, ui->sliderWsArmarRight->value() / 100.0, ui->sliderHsVisu->value() / 100.0);
    factory->addTask(taskArmar);

    connect(taskArmar.get(), SIGNAL(resultReady(ArmarPtr, SoNode*, SoNode*, SoNode*, SoNode*)), this, SLOT(handleResultArmarVisuWorker(ArmarPtr,SoNode*,SoNode*,SoNode*,SoNode*)));

    connect(factory, SIGNAL(started()), this, SLOT(lockRobotAccess()));
    connect(factory, SIGNAL(started()), ui->progressVisualization, SLOT(show()));
    connect(factory, SIGNAL(finished()), ui->progressVisualization, SLOT(hide()));
    connect(factory, SIGNAL(finished()), this, SLOT(unlockRobotAccess()));
    connect(factory, SIGNAL(finished()), factory, SLOT(deleteLater()));
    factory->start();
}

void MainWindow::visualizeHuman()
{
    TaskExecutor* factory = new TaskExecutor();
    boost::shared_ptr<TaskVisualizeHuman> taskHuman(new TaskVisualizeHuman(human));
    taskHuman->setCutZ(ui->sliderWsHumanLeft->value() / 100.0, ui->sliderWsHumanRight->value() / 100.0);
    factory->addTask(taskHuman);

    connect(taskHuman.get(), SIGNAL(resultReady(HumanPtr, SoNode*, SoNode*)), this, SLOT(handleResultHumanVisuWorker(HumanPtr,SoNode*,SoNode*)));

    connect(factory, SIGNAL(started()), this, SLOT(lockRobotAccess()));
    connect(factory, SIGNAL(started()), ui->progressVisualization, SLOT(show()));
    connect(factory, SIGNAL(finished()), ui->progressVisualization, SLOT(hide()));
    connect(factory, SIGNAL(finished()), this, SLOT(unlockRobotAccess()));
    connect(factory, SIGNAL(finished()), factory, SLOT(deleteLater()));
    factory->start();
}

void MainWindow::unlockRobotAccess()
{
    // human joints
    ui->groupHumanJoints->setEnabled(true);
    // workspace
    ui->widgetWorkspace->setEnabled(true);
    // handover space
    ui->widgetHandover->setEnabled(true);
    // visualization
    ui->widgetVisualization->setEnabled(true);
}

void MainWindow::lockRobotAccess()
{
    // human joints
    ui->groupHumanJoints->setEnabled(false);
    // workspace
    ui->widgetWorkspace->setEnabled(false);
    // handover space
    ui->widgetHandover->setEnabled(false);
    // visualization
    ui->widgetVisualization->setEnabled(false);
}

void MainWindow::setDistanceArmarHuman(int mm)
{
    float distance = fabs(human->getRobot()->getGlobalPose()(0, 3) - armar->getRobot()->getGlobalPose()(0, 3));
    armar->transformRobot(-distance + mm, 0, 0, 0, 0, 0);
//    visualizeHandoverPose(false);
}

void MainWindow::rotateHuman(float rad)
{
    human->transformRobot(0, 0, 0, 0, 0, rad);
    human->visualizeGaze();
//    visualizeHandoverPose(false);
}

void MainWindow::enableVisualizationHS(bool enable)
{
    armar->enableVisualizationHS(enable);
    visualizeArmar();
}

void MainWindow::enableVisualizationWS(bool enable)
{
    human->enableVisualizationWS(enable);
    armar->enableVisualizationWS(enable);
    visualize();
}

void MainWindow::toggleComfortLog()
{
    if (!ui->listComfortLog->isVisible()) {
        logComfort();
        ui->listComfortLog->show();
        ui->btnToggleTorqueLog->setText("Hide");
    }
    else {
        ui->listComfortLog->hide();
        ui->btnToggleTorqueLog->setText("Show Torque");
    }
}

void MainWindow::enableVisualizationEnvironment(bool enable)
{
    environmentSep->removeAllChildren();

    if (enable) {
        environmentSep->addChild(CoinVisualizationFactory::getCoinVisualization(environment, SceneObject::Full));
    }
}

void MainWindow::enableVisualizationLadder(bool enable)
{
    ladderSep->removeAllChildren();

    if (enable) {
        ladderSep->addChild(CoinVisualizationFactory::getCoinVisualization(ladder, SceneObject::Full));
    }
}

void MainWindow::testSolveHandoverIKPerformace()
{
//    float duration = 0;
//    unsigned int quality = 0;
//    unsigned int highestQuality = 0;
//    int steps = 100;
//    float durations[steps];

//    for (int i = 0; i < steps; i++)
//    {
//        QElapsedTimer timer;
//        timer.start();
//        TaskExecutor* factoryIK = new TaskExecutor();
//        TaskSolveHandoverIKPtr handoverIKWorker(new TaskSolveHandoverIK(armar, human, 16));
//        factoryIK->addTask(handoverIKWorker);

//        factoryIK->run();
//        float elapsed = timer.elapsed() / 1000.0f;
//        durations[i] = elapsed;
//        duration += elapsed;
//        quality += armar->getHandoverSpace()->getEntry(handoverIKWorker->getBestHandoverPose());
//        highestQuality += armar->getHandoverSpace()->getMaxEntry();
//        armar->resetRobotConfig();
//        human->resetRobotConfig();
//        delete factoryIK;



//        armar->resetHandoverSpace();
//        TaskExecutor* factoryHS = new TaskExecutor();

//        int threads = QThread::idealThreadCount();
//        threads = (threads < 1) ? 1 : threads;
//        float range = 1.0f / threads;

//        boost::shared_ptr<boost::mutex> mutex(new boost::mutex());
//        for (int i = 0; i < threads; i++)
//        {
//            boost::shared_ptr<TaskCreateHandoverspace> task(new TaskCreateHandoverspace(armar, armar->getObjectSide(), human, 1));
//            QPair<float,float> interval(i * range, (i+1) * range);

//            task->setConcurrent(interval, mutex);
//            factoryHS->addTask(task);
//            if (i < threads - 1) factoryHS->newThread();
//        }
//        factoryHS->run();
//        delete factoryHS;
//    }

//    float sumDurationsV = 0;
//    for (int j = 0; j < steps ; j++)
//    {
//        sumDurationsV += (durations[j] - (duration / steps)) * (durations[j] - (duration / steps));
//    }
//    float sdDuration = std::sqrt(sumDurationsV / (steps - 1));

//    cout << "***********************************************************" << endl;
//    cout << "STEPS: " << steps << endl;
//    cout << "SOLVE HANDOVER DURATION (s): " << duration / steps << endl;
//    cout << "AVERAGE HANDOVER POSE QUALITY: " << (float)quality / (float)steps << endl;
//    cout << "AVERAGE HIGHEST QUALITY: " << (float)highestQuality / (float)steps << endl;
//    cout << "STANDARD DEVIATION DURATION: " << sdDuration << endl;
//    cout << "***********************************************************" << endl;
}

void MainWindow::handleResultHumanVisuWorker(HumanPtr human, SoNode* wsLeft, SoNode* wsRight)
{
    human->visualize(wsLeft, wsRight);
}

void MainWindow::handleResultArmarVisuWorker(ArmarPtr armar, SoNode* wsLeft, SoNode* wsRight, SoNode* hs, SoNode *object)
{
    armar->visualize(wsLeft, wsRight, hs, object);
}

void MainWindow::selectKinematicChain(QString name)
{
    if (name.isEmpty() || !human->getRobot()->getRobotNodeSet(name.toStdString()))
        return;

    std::vector<RobotNodePtr> nodes = human->getRobot()->getRobotNodeSet(name.toStdString())->getAllRobotNodes();
    // update human joint selection
    ui->coBoxHumanJoint->clear();
    for (std::vector<RobotNodePtr>::iterator it = nodes.begin(); it != nodes.end(); ++it)
    {
        RobotNodePtr node = *it;
        ui->coBoxHumanJoint->addItem(QString::fromStdString(node->getName()));
    }
    selectJoint(QString::fromStdString(nodes[0]->getName()));
    logComfort();
}

void MainWindow::selectJoint(QString name)
{
    if (name.isEmpty())
        return;

    RobotNodePtr node = human->getRobot()->getRobotNode(name.toStdString());
    float jointValue    = node ? node->getJointValue()    : 0;
    float minJointValue = node ? node->getJointLimitLo()  : 0;
    float maxJointValue = node ? node->getJointLimitHi()  : 0;

    // update label
    QString minValue = QString::number(minJointValue).left(QString::number(minJointValue).indexOf(".") + 3);
    QString maxValue = QString::number(maxJointValue).left(QString::number(maxJointValue).indexOf(".") + 3);
    QString value = QString::number(jointValue).left(QString::number(jointValue).indexOf(".") + 3);
    QString text = value + "<font color=\"grey\"> [" + minValue + ", " + maxValue + "]</font>";
    ui->lblJointValueDisplay->setText(text);

    // update slider
    float sliderValue = node ? (std::fabs(jointValue - minJointValue) / std::fabs(maxJointValue - minJointValue)) * ui->sliderJointValue->maximum() : 0;
    ui->sliderJointValue->setValue(sliderValue);
}

void MainWindow::setJointValue()
{
    RobotNodePtr node = human->getRobot()->getRobotNode(ui->coBoxHumanJoint->currentText().toStdString());
    if (!node)
        return;

    float minJointValue = node->getJointLimitLo();
    float maxJointValue = node->getJointLimitHi();
    float newJointValue = minJointValue + ((ui->sliderJointValue->value() / ((float) ui->sliderJointValue->maximum())) * std::fabs(maxJointValue - minJointValue));

    node->setJointValue(newJointValue);
    // update label
    QString minValue = QString::number(minJointValue).left(QString::number(minJointValue).indexOf(".") + 3);
    QString maxValue = QString::number(maxJointValue).left(QString::number(maxJointValue).indexOf(".") + 3);
    QString newValue = QString::number(newJointValue).left(QString::number(newJointValue).indexOf(".") + 3);
    QString text = newValue + "<font color=\"grey\"> [" + minValue + ", " + maxValue + "]</font>";
    ui->lblJointValueDisplay->setText(text);

    logComfort();
    human->visualizeGaze();
    world->update();
}

void MainWindow::updateHumanHead()
{
    RobotNodePtr nodeUpperH = human->getRobot()->getRobotNode("BUNz_joint");
    RobotNodePtr nodeUpperV = human->getRobot()->getRobotNode("BUNx_joint");

    float newJointValueUpperH = nodeUpperH->getJointLimitHi() - ((ui->sliderHumanHeadHorizontal->value() / ((float) ui->sliderHumanHeadHorizontal->maximum())) * std::fabs(nodeUpperH->getJointLimitHi() - nodeUpperH->getJointLimitLo()));
    nodeUpperH->setJointValue(newJointValueUpperH);

    float newJointValueUpperV = nodeUpperV->getJointLimitLo() + ((ui->sliderHumanHeadVertical->value() / ((float) ui->sliderHumanHeadVertical->maximum())) * std::fabs(nodeUpperV->getJointLimitHi() - nodeUpperV->getJointLimitLo()));
    nodeUpperV->setJointValue(newJointValueUpperV);

    RobotNodePtr nodeLowerH = human->getRobot()->getRobotNode("BLNz_joint");
    RobotNodePtr nodeLowerV = human->getRobot()->getRobotNode("BLNx_joint");

    float newJointValueLowerH = nodeLowerH->getJointLimitHi() - ((ui->sliderHumanHeadHorizontal->value() / ((float) ui->sliderHumanHeadHorizontal->maximum())) * std::fabs(nodeLowerH->getJointLimitHi() - nodeLowerH->getJointLimitLo()));
    nodeLowerH->setJointValue(newJointValueLowerH);

    float newJointValueLowerV = nodeLowerV->getJointLimitLo() + ((ui->sliderHumanHeadVertical->value() / ((float) ui->sliderHumanHeadVertical->maximum())) * std::fabs(nodeLowerV->getJointLimitHi() - nodeLowerV->getJointLimitLo()));
    nodeLowerV->setJointValue(newJointValueLowerV);

    human->visualizeGaze();
}

void MainWindow::transfromPositive()
{
    if (ui->rBtnTransformHuman->isChecked())
    {
        if (ui->comboBoxTransform->currentText() == "x") {
            human->transformRobot(100, 0, 0, 0, 0, 0);
        } else if (ui->comboBoxTransform->currentText() == "y") {
            human->transformRobot(0, 100, 0, 0, 0, 0);
        } else if (ui->comboBoxTransform->currentText() == "z") {
            human->transformRobot(0, 0, 20, 0, 0, 0);
        } else if (ui->comboBoxTransform->currentText() == "roll") {
            human->transformRobot(0, 0, 0, 0.1, 0, 0);
        } else if (ui->comboBoxTransform->currentText() == "pitch") {
            human->transformRobot(0, 0, 0, 0, 0.1, 0);
        } else if (ui->comboBoxTransform->currentText() == "yaw") {
            human->transformRobot(0, 0, 0, 0, 0, 0.1);
        }
    }
    else if (ui->rBtnTransformRobot->isChecked())
    {
        if (ui->comboBoxTransform->currentText() == "x") {
            armar->transformRobot(100, 0, 0, 0, 0, 0);
        } else if (ui->comboBoxTransform->currentText() == "y") {
            armar->transformRobot(0, 100, 0, 0, 0, 0);
        } else if (ui->comboBoxTransform->currentText() == "z") {
            armar->transformRobot(0, 0, 20, 0, 0, 0);
        } else if (ui->comboBoxTransform->currentText() == "roll") {
            armar->transformRobot(0, 0, 0, 0.1, 0, 0);
        } else if (ui->comboBoxTransform->currentText() == "pitch") {
            armar->transformRobot(0, 0, 0, 0, 0.1, 0);
        } else if (ui->comboBoxTransform->currentText() == "yaw") {
            armar->transformRobot(0, 0, 0, 0, 0, 0.1);
        }
    }
    human->visualizeGaze();
//    visualizeHandoverPose(false);
    UpdateDistanceHumanRobot();
}

void MainWindow::TransformNegative()
{
    if (ui->rBtnTransformHuman->isChecked())
    {
        if (ui->comboBoxTransform->currentText() == "x") {
            human->transformRobot(-100, 0, 0, 0, 0, 0);
        } else if (ui->comboBoxTransform->currentText() == "y") {
            human->transformRobot(0, -100, 0, 0, 0, 0);
        } else if (ui->comboBoxTransform->currentText() == "z") {
            human->transformRobot(0, 0, -20, 0, 0, 0);
        } else if (ui->comboBoxTransform->currentText() == "roll") {
            human->transformRobot(0, 0, 0, -0.1, 0, 0);
        } else if (ui->comboBoxTransform->currentText() == "pitch") {
            human->transformRobot(0, 0, 0, 0, -0.1, 0);
        } else if (ui->comboBoxTransform->currentText() == "yaw") {
            human->transformRobot(0, 0, 0, 0, 0, -0.1);
        }
    }
    else if (ui->rBtnTransformRobot->isChecked())
    {
        if (ui->comboBoxTransform->currentText() == "x") {
            armar->transformRobot(-100, 0, 0, 0, 0, 0);
        } else if (ui->comboBoxTransform->currentText() == "y") {
            armar->transformRobot(0, -100, 0, 0, 0, 0);
        } else if (ui->comboBoxTransform->currentText() == "z") {
            armar->transformRobot(0, 0, -20, 0, 0, 0);
        } else if (ui->comboBoxTransform->currentText() == "roll") {
            armar->transformRobot(0, 0, 0, -0.1, 0, 0);
        } else if (ui->comboBoxTransform->currentText() == "pitch") {
            armar->transformRobot(0, 0, 0, 0, -0.1, 0);
        } else if (ui->comboBoxTransform->currentText() == "yaw") {
            armar->transformRobot(0, 0, 0, 0, 0, -0.1);
        }
    }
    human->visualizeGaze();
    //    visualizeHandoverPose(false);
    UpdateDistanceHumanRobot();
}

void MainWindow::UpdateDistanceHumanRobot()
{
    SceneObjectPtr armarTorso = armar->getRobot()->getRobotNode("Hip Yaw");
    SceneObjectPtr humanTorsoR = human->getRobot()->getRobotNode("RSCsegment_joint");
    SceneObjectPtr humanTorsoL = human->getRobot()->getRobotNode("LSCsegment_joint");

    int id1;
    int id2;
    Eigen::Vector3f p1;
    Eigen::Vector3f p2;
    float d1 = armarTorso->getCollisionChecker()->calculateDistance(armarTorso, humanTorsoR, p1, p2, &id1, &id2);
    float d2 = armarTorso->getCollisionChecker()->calculateDistance(armarTorso, humanTorsoL, p1, p2, &id1, &id2);

    float minDistance = (d1 <= d2) ? d1 : d2;
    ui->lblDistanceHumanRobot->setText(QString::number(minDistance) + " mm");
}

void MainWindow::setupUI()
{
    ui->setupUi(this);

    /***************** SotExaminerViewer ************************/
    examinerViewer = new SoQtExaminerViewer(ui->frameViewer, "examinerViewer", true, SoQtExaminerViewer::BUILD_POPUP);
    examinerViewer->setBackgroundColor(SbColor(1, 1, 1));
    examinerViewer->setFeedbackVisibility(true);
//    examinerViewer->setAccumulationBuffer(true);
//    examinerViewer->setAntialiasing(true, 4);
    examinerViewer->setSceneGraph(sceneSep);
    examinerViewer->viewAll();

    /*************** human nodes **************************/
    // human kinematic chain selection
    BOOST_FOREACH(const RobotNodeSetPtr &rns, human->getRobot()->getRobotNodeSets()) {
        if ((rns->isKinematicChain() && hasBodies(rns)) || rns->getName() == "All") {
            ui->coBoxHumanKinematicChain->addItem(QString::fromStdString(rns->getName()));
        }
    }

    // update human joint selection by setting current kinematic chain
    selectKinematicChain(ui->coBoxHumanKinematicChain->currentText());

    /*************** transfrom ******************************/
    ui->comboBoxTransform->addItem("x");
    ui->comboBoxTransform->addItem("y");
    ui->comboBoxTransform->addItem("z");
    ui->comboBoxTransform->addItem("roll");
    ui->comboBoxTransform->addItem("pitch");
    ui->comboBoxTransform->addItem("yaw");
    UpdateDistanceHumanRobot();

    /*************** human head *****************************/
    // init slider
    RobotNodePtr nodeH = human->getRobot()->getRobotNode("BUNz_joint");
    RobotNodePtr nodeV = human->getRobot()->getRobotNode("BUNx_joint");

    float sliderValueH = (std::fabs(nodeH->getJointValue() - nodeH->getJointLimitLo()) / std::fabs(nodeH->getJointLimitHi() - nodeH->getJointLimitLo())) * ui->sliderHumanHeadHorizontal->maximum();
    ui->sliderHumanHeadHorizontal->setValue(sliderValueH);

    float sliderValueV = (std::fabs(nodeV->getJointValue() - nodeV->getJointLimitLo()) / std::fabs(nodeV->getJointLimitHi() - nodeV->getJointLimitLo())) * ui->sliderHumanHeadHorizontal->maximum();
    ui->sliderHumanHeadVertical->setValue(sliderValueV);

    /************* visualization *****************************/
    ui->progressVisualization->setVisible(false);
    enableVisualizationWS(ui->checkBoxDisplayWS->isChecked());
    enableVisualizationHS(ui->checkBoxDisplayHS->isChecked());
    enableVisualizationEnvironment(ui->checkBoxDisplayEnvironment->isChecked());
    ui->sliderHumanGaze->setValue(human->getGazeLength());

    /************* widgets visibility ***********************/
    ui->widgetWorkspace->setVisible(ui->actionWorkspace->isChecked());
    ui->widgetHandover->setVisible(ui->actionHandover->isChecked());

    /************* world listeners **************************/

    world->addListener(ui->widgetWorkspace);
    world->addListener(ui->widgetHandover);
}

void MainWindow::connectUI()
{
    // human nodes
    connect(ui->coBoxHumanJoint, SIGNAL(currentIndexChanged(QString)), this, SLOT(selectJoint(QString)));
    connect(ui->coBoxHumanKinematicChain, SIGNAL(currentIndexChanged(QString)), this, SLOT(selectKinematicChain(QString)));
    connect(ui->sliderJointValue, SIGNAL(sliderMoved(int)), this, SLOT(setJointValue()));
    connect(ui->btnToggleTorqueLog, SIGNAL(clicked()), this, SLOT(toggleComfortLog()));

    // transfrom
    connect(ui->btnTransformPositive, SIGNAL(clicked()), this, SLOT(transfromPositive()));
    connect(ui->btnTransformNegative, SIGNAL(clicked()), this, SLOT(TransformNegative()));

    // human head
    connect(ui->sliderHumanHeadHorizontal, SIGNAL(sliderMoved(int)), this, SLOT(updateHumanHead()));
    connect(ui->sliderHumanHeadVertical, SIGNAL(sliderMoved(int)), this, SLOT(updateHumanHead()));

    // visualization
    connect(ui->checkBoxDisplayHS, SIGNAL(toggled(bool)), this, SLOT(enableVisualizationHS(bool)));
    connect(ui->checkBoxDisplayWS, SIGNAL(toggled(bool)), this, SLOT(enableVisualizationWS(bool)));
    connect(ui->checkBoxDisplayEnvironment, SIGNAL(toggled(bool)), this, SLOT(enableVisualizationEnvironment(bool)));
    connect(ui->sliderHumanGaze, SIGNAL(sliderMoved(int)), this, SLOT(setHumanGazeLength(int)));

    connect(ui->sliderWsHumanLeft, SIGNAL(sliderReleased()), this, SLOT(visualizeHuman()));
    connect(ui->sliderWsHumanRight, SIGNAL(sliderReleased()), this, SLOT(visualizeHuman()));
    connect(ui->sliderWsArmarLeft, SIGNAL(sliderReleased()), this, SLOT(visualizeArmar()));
    connect(ui->sliderWsArmarRight, SIGNAL(sliderReleased()), this, SLOT(visualizeArmar()));
    connect(ui->sliderHsVisu, SIGNAL(sliderReleased()), this, SLOT(visualizeArmar()));

}

