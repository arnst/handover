#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "../human.h"
#include "../armar.h"
#include "widgets/workspacesettingsdialog.h"
#include "../posequalitycomfort.h"
#include "../settings.h"
#include "../world.h"

#include <QMainWindow>

#include <VirtualRobot/Visualization/CoinVisualization/CoinVisualization.h>

#include <boost/shared_ptr.hpp>

#include <Inventor/Qt/viewers/SoQtExaminerViewer.h>
#include <Inventor/nodes/SoSeparator.h>

using namespace VirtualRobot;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();  

private:
    WorldPtr world;
    QString fileNameHuman;
    QString fileNameArmar;
    QString fileNameEnvironment;
    QString fileNameLadder;

    Ui::MainWindow *ui;
    SoQtExaminerViewer *examinerViewer;

    SoSeparator* sceneSep;
    SoSeparator* environmentSep;
    SoSeparator* handoverPoseSep;
    SoSeparator* ladderSep;

    HumanPtr human;
    ArmarPtr armar;
    ManipulationObjectPtr environment;
    ManipulationObjectPtr ladder;

    Eigen::Matrix4f startPoseArmar;
    Eigen::Matrix4f startPoseHuman;

    PoseQualityComfortPtr measure;

    void initSceneGraph();
    void setupUI();
    void connectUI();
    void setupDataPaths();
    void loadEnvironment();
    void loadLadder();
    void initActors();
    void logComfort();
    RobotNodeSetPtr getBodies(RobotNodeSetPtr rns);
    bool hasBodies(RobotNodeSetPtr rns);
    PoseQualityMeasurementPtr getMeasure(Settings::Workspace::Measure measure, RobotNodeSetPtr rns);

private slots:
    void visualize();
    void visualizeArmar();
    void visualizeHuman();
    void selectJoint(QString name);
    void selectKinematicChain(QString name);
    void setJointValue();
    void testHSPerformance(int graspCombinations);
    void unlockRobotAccess();
    void lockRobotAccess();
    void setDistanceArmarHuman(int mm);
    void rotateHuman(float rad);
    void enableVisualizationHS(bool enable);
    void enableVisualizationWS(bool enable);
    void toggleComfortLog();
    void enableVisualizationEnvironment(bool enable);
    void enableVisualizationLadder(bool enable);
    void testSolveHandoverIKPerformace();
    void setHumanGazeLength(int mm);
    void updateHumanHead();
    void transfromPositive();
    void TransformNegative();
    void UpdateDistanceHumanRobot();

    /**
     * The graph scene should not be modified from within another thread.
     * Therefore the visualization-task posts its results back to the gui-thread.
     * Those results are then delegated to the respective Actor.
     */
    void handleResultHumanVisuWorker(HumanPtr human, SoNode* wsLeft, SoNode* wsRight);
    void handleResultArmarVisuWorker(ArmarPtr armar, SoNode* wsLeft, SoNode* wsRight, SoNode* hs, SoNode* object);
};

#endif // MAINWINDOW_H
