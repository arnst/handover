#include "workspacesettingsdialog.h"
#include "ui_workspacesettingsdialog.h"

#include "../../settings.h"

WorkspaceSettingsDialog::WorkspaceSettingsDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::WorkspaceSettingsDialog)
{
    setupUI();
}

WorkspaceSettingsDialog::~WorkspaceSettingsDialog()
{
    delete ui;
}

void WorkspaceSettingsDialog::setupUI()
{
    ui->setupUi(this);
    ui->coBoxMeasureHuman->addItem("Comfort");
    ui->coBoxMeasureHuman->addItem("Ext. Manipulability");
    ui->coBoxMeasureHuman->addItem("Reachability");

    ui->coBoxMeasureArmar->addItem("Manipulability");
    ui->coBoxMeasureArmar->addItem("Ext. Manipulability");
    ui->coBoxMeasureArmar->addItem("Reachability");

    // measure human
    if (Settings::Workspace::measureHuman == Settings::Workspace::COMFORT) {
        ui->coBoxMeasureHuman->setCurrentIndex(0);
    } else if (Settings::Workspace::measureHuman == Settings::Workspace::EXT_MANIPULABILITY) {
        ui->coBoxMeasureHuman->setCurrentIndex(1);
    } else if (Settings::Workspace::measureHuman == Settings::Workspace::REACHABILITY) {
        ui->coBoxMeasureHuman->setCurrentIndex(2);
    }

    // measure armar
    if (Settings::Workspace::measureArmar == Settings::Workspace::MANIPULABILITY) {
        ui->coBoxMeasureArmar->setCurrentIndex(0);
    } else if (Settings::Workspace::measureArmar == Settings::Workspace::EXT_MANIPULABILITY) {
        ui->coBoxMeasureArmar->setCurrentIndex(1);
    } else if (Settings::Workspace::measureArmar == Settings::Workspace::REACHABILITY) {
        ui->coBoxMeasureArmar->setCurrentIndex(2);
    }

    ui->edtSamplesHuman->setValue(Settings::Workspace::samplesHuman);
    ui->edtSamplesArmar->setValue(Settings::Workspace::samplesArmar);

    ui->edtTranslation->setValue(Settings::Workspace::discrStepTrans);
    ui->edtRotation->setValue(Settings::Workspace::discrStepRot);

    ui->edtSmooth->setValue(Settings::Workspace::smooth);

    ui->edtSamplesExtend->setValue(Settings::Workspace::samplesExtend);

    connect(ui->buttonBox, SIGNAL(accepted()), this, SLOT(apply()));
    connect(ui->buttonBox, SIGNAL(rejected()), this, SLOT(reject()));
}

void WorkspaceSettingsDialog::apply()
{
    // measure human
    if (ui->coBoxMeasureHuman->currentText() == "Comfort") {
        Settings::Workspace::measureHuman = Settings::Workspace::COMFORT;
    } else if (ui->coBoxMeasureHuman->currentText() == "Ext. Manipulability") {
        Settings::Workspace::measureHuman = Settings::Workspace::EXT_MANIPULABILITY;
    } else if (ui->coBoxMeasureHuman->currentText() == "Reachability") {
        Settings::Workspace::measureHuman = Settings::Workspace::REACHABILITY;
    }

    // measure armar
    if (ui->coBoxMeasureArmar->currentText() == "Manipulability") {
        Settings::Workspace::measureArmar = Settings::Workspace::MANIPULABILITY;
    } else if (ui->coBoxMeasureArmar->currentText() == "Ext. Manipulability") {
        Settings::Workspace::measureArmar = Settings::Workspace::EXT_MANIPULABILITY;
    } else if (ui->coBoxMeasureArmar->currentText() == "Reachability") {
        Settings::Workspace::measureArmar = Settings::Workspace::REACHABILITY;
    }

    Settings::Workspace::samplesHuman = ui->edtSamplesHuman->value();
    Settings::Workspace::samplesArmar = ui->edtSamplesArmar->value();

    Settings::Workspace::discrStepTrans = ui->edtTranslation->value();
    Settings::Workspace::discrStepRot = ui->edtRotation->value();

    Settings::Workspace::smooth = ui->edtSmooth->value();

    Settings::Workspace::samplesExtend = ui->edtSamplesExtend->value();

    accept();
}

