#include "workspacewidget.h"
#include "ui_workspacewidget.h"
#include "workspacesettingsdialog.h"
#include "../../human.h"
#include "../../armar.h"
#include "../../posequalitycomfort.h"
#include "../../posequalityreachability.h"
#include "../../tasks/taskloadworkspace.h"
#include "../../tasks/tasksaveworkspace.h"
#include "../../tasks/taskextendworkspace.h"
#include "../../tasks/tasksmoothworkspace.h"

#include "VirtualRobot/IK/PoseQualityManipulability.h"
#include "VirtualRobot/IK/PoseQualityExtendedManipulability.h"

WorkspaceWidget::WorkspaceWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::WorkspaceWidget)
{
    setupUI();
    connectUI();
}

WorkspaceWidget::~WorkspaceWidget()
{
    delete ui;
}

void WorkspaceWidget::setupUI()
{
    ui->setupUi(this);
    ui->progressBar->hide();
}

void WorkspaceWidget::connectUI()
{
    connect(ui->btnCreate, SIGNAL(clicked()), this, SLOT(createWorkspace()));
    connect(ui->btnReset, SIGNAL(clicked()), this, SLOT(resetWorkspace()));
    connect(ui->btnLoad, SIGNAL(clicked()), this, SLOT(loadWorkspace()));
    connect(ui->btnSave, SIGNAL(clicked()), this, SLOT(saveWorkspace()));
    connect(ui->btnPrint, SIGNAL(clicked()), this, SLOT(printWorkspace()));
    connect(ui->btnSettings, SIGNAL(clicked()), this, SLOT(openSettings()));
    connect(ui->btnExtend, SIGNAL(clicked()), this, SLOT(extendWorkspace()));
    connect(ui->btnSmooth, SIGNAL(clicked()), this, SLOT(smoothWorkspace()));
}

void WorkspaceWidget::connectFactory(TaskExecutor *factory)
{
    connect(factory, SIGNAL(started()), world.get(), SLOT(lock()));
    connect(factory, SIGNAL(started()), this, SLOT(showProgress()));
    connect(factory, SIGNAL(finished()), this, SLOT(hideProgress()));
    connect(factory, SIGNAL(finished()), world.get(), SLOT(unlock()));
    connect(factory, SIGNAL(finished()), world.get(), SLOT(update()));
    connect(factory, SIGNAL(finished()), factory, SLOT(deleteLater()));
}

void WorkspaceWidget::onWorldUpdate()
{

}

void WorkspaceWidget::onWorldLock()
{
    ui->mainWidget->setEnabled(false);
}

void WorkspaceWidget::onWorldUnlock()
{
    ui->mainWidget->setEnabled(true);
}


void WorkspaceWidget::createWorkspace()
{
    TaskExecutor* factory = new TaskExecutor();
    HumanPtr human = world->getHuman();
    ArmarPtr armar = world->getArmar();

    bool humanLeftSide = ui->checkBoxHumanLeftCreate->isChecked();
    bool humanRightSide = ui->checkBoxHumanRightCreate->isChecked();
    bool armarLeftSide = ui->checkBoxArmarLeftCreate->isChecked();
    bool armarRightSide = ui->checkBoxArmarRightCreate->isChecked();

    // human left side
    if (humanLeftSide)
    {
        RobotNodeSetPtr rns = human->getRobot()->getRobotNodeSet("LeftArm");
        PoseQualityMeasurementPtr measure = getMeasure(Settings::Workspace::measureHuman, rns);
        // collision models
        RobotNodeSetPtr staticColModel = human->getRobot()->getRobotNodeSet("LegsHipTorsoHead_ColModel");
        RobotNodeSetPtr dynamicColModel = human->getRobot()->getRobotNodeSet("LeftHand_ColModel");

        boost::shared_ptr<TaskCreateManipulability> task(new TaskCreateManipulability(human, Actor::LEFT_TCP));
        task->setParams(measure, rns, Settings::Workspace::discrStepTrans, Settings::Workspace::discrStepRot, Settings::Workspace::samplesHuman, staticColModel, dynamicColModel, rns->getKinematicRoot(), rns->getTCP(), true);
        factory->addTask(task);
    }

    // human right side
    if (humanRightSide)
    {
        RobotNodeSetPtr rns = human->getRobot()->getRobotNodeSet("RightArm");
        PoseQualityMeasurementPtr measure = getMeasure(Settings::Workspace::measureHuman, rns);
        // collision models
        RobotNodeSetPtr staticColModel = human->getRobot()->getRobotNodeSet("LegsHipTorsoHead_ColModel");
        RobotNodeSetPtr dynamicColModel = human->getRobot()->getRobotNodeSet("RightHand_ColModel");

        boost::shared_ptr<TaskCreateManipulability> task(new TaskCreateManipulability(human, Actor::RIGHT_TCP));
        task->setParams(measure, rns, Settings::Workspace::discrStepTrans, Settings::Workspace::discrStepRot, Settings::Workspace::samplesHuman, staticColModel, dynamicColModel, rns->getKinematicRoot(), rns->getTCP(), true);
        factory->addTask(task);
    }

    if (humanLeftSide || humanRightSide) {
        factory->newThread();
    }

    // armar left side
    if (armarLeftSide)
    {
        RobotNodeSetPtr rns = armar->getRobot()->getRobotNodeSet("TorsoLeftArm");
        PoseQualityMeasurementPtr measure = getMeasure(Settings::Workspace::measureArmar, rns);
        // collision models
        RobotNodeSetPtr staticColModel = armar->getRobot()->getRobotNodeSet("PlatformTorsoHeadColModel");
        RobotNodeSetPtr dynamicColModel = armar->getRobot()->getRobotNodeSet("LeftHandColModel");

        boost::shared_ptr<TaskCreateManipulability> task(new TaskCreateManipulability(armar, Actor::LEFT_TCP));
        task->setParams(measure, rns, Settings::Workspace::discrStepTrans, Settings::Workspace::discrStepRot, Settings::Workspace::samplesArmar, staticColModel, dynamicColModel, rns->getKinematicRoot(), rns->getTCP(), true);
        factory->addTask(task);
    }

    // armar right side
    if (armarRightSide)
    {
        RobotNodeSetPtr rns = armar->getRobot()->getRobotNodeSet("TorsoRightArm");
        PoseQualityMeasurementPtr measure = getMeasure(Settings::Workspace::measureArmar, rns);
        // collision models
        RobotNodeSetPtr staticColModel = armar->getRobot()->getRobotNodeSet("PlatformTorsoHeadColModel");
        RobotNodeSetPtr dynamicColModel = armar->getRobot()->getRobotNodeSet("RightHandColModel");

        boost::shared_ptr<TaskCreateManipulability> task(new TaskCreateManipulability(armar, Actor::RIGHT_TCP));
        task->setParams(measure, rns, Settings::Workspace::discrStepTrans, Settings::Workspace::discrStepRot, Settings::Workspace::samplesArmar, staticColModel,dynamicColModel, rns->getKinematicRoot(), rns->getTCP(), true);
        factory->addTask(task);
    }

    connectFactory(factory);
    factory->start();
}

void WorkspaceWidget::resetWorkspace()
{
    world->getHuman()->resetWorkspace();
    world->getArmar()->resetWorkspace();
    world->update();
}

void WorkspaceWidget::loadWorkspace()
{
    TaskExecutor* factory = new TaskExecutor();
    HumanPtr human = world->getHuman();
    ArmarPtr armar = world->getArmar();

    TaskLoadWorkspacePtr taskHumanLeft(new TaskLoadWorkspace(human, Actor::LEFT_TCP, ui->edtLoadHumanLeft->text()));
    TaskLoadWorkspacePtr taskHumanRight(new TaskLoadWorkspace(human, Actor::RIGHT_TCP, ui->edtLoadHumanRight->text()));
    TaskLoadWorkspacePtr taskArmarLeft(new TaskLoadWorkspace(armar, Actor::LEFT_TCP, ui->edtLoadArmarLeft->text()));
    TaskLoadWorkspacePtr taskArmarRight(new TaskLoadWorkspace(armar, Actor::RIGHT_TCP, ui->edtLoadArmarRight->text()));

    factory->addTask(taskHumanLeft);
    factory->addTask(taskHumanRight);
    factory->newThread();
    factory->addTask(taskArmarLeft);
    factory->addTask(taskArmarRight);

    connectFactory(factory);
    factory->start();
//    factory->run();
}

void WorkspaceWidget::saveWorkspace()
{
    TaskExecutor* factory = new TaskExecutor();
    HumanPtr human = world->getHuman();
    ArmarPtr armar = world->getArmar();

    TaskSaveWorkspacePtr taskHumanLeft(new TaskSaveWorkspace(human, Actor::LEFT_TCP, ui->edtSaveHumanLeft->text()));
    TaskSaveWorkspacePtr taskHumanRight(new TaskSaveWorkspace(human, Actor::RIGHT_TCP, ui->edtSaveHumanRight->text()));
    TaskSaveWorkspacePtr taskArmarLeft(new TaskSaveWorkspace(armar, Actor::LEFT_TCP, ui->edtSaveArmarLeft->text()));
    TaskSaveWorkspacePtr taskArmarRight(new TaskSaveWorkspace(armar, Actor::RIGHT_TCP, ui->edtSaveArmarRight->text()));

    factory->addTask(taskHumanLeft);
    factory->addTask(taskHumanRight);
    factory->newThread();
    factory->addTask(taskArmarLeft);
    factory->addTask(taskArmarRight);

    connectFactory(factory);
    factory->start();
}

void WorkspaceWidget::printWorkspace()
{
    HumanPtr human = world->getHuman();
    ArmarPtr armar = world->getArmar();

    human->getManipulability(Actor::LEFT_TCP)->print();
    human->getManipulability(Actor::RIGHT_TCP)->print();
    armar->getManipulability(Actor::LEFT_TCP)->print();
    armar->getManipulability(Actor::RIGHT_TCP)->print();
}

void WorkspaceWidget::extendWorkspace()
{
    Actor::TCP humanTcpSide;
    Actor::TCP armarTcpSide;

    // determine human sides
    if (ui->checkBoxHumanLeftExtend->isChecked() && ui->checkBoxHumanRightExtend->isChecked()) {
        humanTcpSide = Actor::BOTH_TCP;
    } else if (ui->checkBoxHumanLeftExtend->isChecked()) {
        humanTcpSide = Actor::LEFT_TCP;
    } else if (ui->checkBoxHumanRightExtend->isChecked()) {
        humanTcpSide = Actor::RIGHT_TCP;
    } else {
        humanTcpSide = Actor::NO_TCP;
    }

    // determine armar sides
    if (ui->checkBoxArmarLeftExtend->isChecked() && ui->checkBoxArmarRightExtend->isChecked()) {
        armarTcpSide = Actor::BOTH_TCP;
    } else if (ui->checkBoxArmarLeftExtend->isChecked()) {
        armarTcpSide = Actor::LEFT_TCP;
    } else if (ui->checkBoxArmarRightExtend->isChecked()) {
        armarTcpSide = Actor::RIGHT_TCP;
    } else {
        armarTcpSide = Actor::NO_TCP;
    }

    TaskExecutor* factory = new TaskExecutor();
    HumanPtr human = world->getHuman();
    ArmarPtr armar = world->getArmar();
    bool extendHumanWS = true;
    bool extendArmarWS = true;

    if (humanTcpSide == Actor::BOTH_TCP) {
        TaskExtendWorkspacePtr taskHumanLeft(new TaskExtendWorkspace(human, Actor::LEFT_TCP, Settings::Workspace::samplesExtend, true));
        TaskExtendWorkspacePtr taskHumanRight(new TaskExtendWorkspace(human, Actor::RIGHT_TCP, Settings::Workspace::samplesExtend, true));
        factory->addTask(taskHumanLeft);
        factory->addTask(taskHumanRight);
    } else if (humanTcpSide == Actor::LEFT_TCP) {
        TaskExtendWorkspacePtr taskHumanLeft(new TaskExtendWorkspace(human, Actor::LEFT_TCP, Settings::Workspace::samplesExtend, true));
        factory->addTask(taskHumanLeft);
    } else if (humanTcpSide == Actor::RIGHT_TCP) {
        TaskExtendWorkspacePtr taskHumanRight(new TaskExtendWorkspace(human, Actor::RIGHT_TCP, Settings::Workspace::samplesExtend, true));
        factory->addTask(taskHumanRight);
    } else {
        extendHumanWS = false;
    }

    if (extendHumanWS) factory->newThread();

    if (armarTcpSide == Actor::BOTH_TCP) {
        TaskExtendWorkspacePtr taskArmarLeft(new TaskExtendWorkspace(armar, Actor::LEFT_TCP, Settings::Workspace::samplesExtend, true));
        TaskExtendWorkspacePtr taskArmarRight(new TaskExtendWorkspace(armar, Actor::RIGHT_TCP, Settings::Workspace::samplesExtend, true));
        factory->addTask(taskArmarLeft);
        factory->addTask(taskArmarRight);
    } else if (armarTcpSide == Actor::LEFT_TCP) {
        TaskExtendWorkspacePtr taskArmarLeft(new TaskExtendWorkspace(armar, Actor::LEFT_TCP, Settings::Workspace::samplesExtend, true));
        factory->addTask(taskArmarLeft);
    } else if (armarTcpSide == Actor::RIGHT_TCP) {
        TaskExtendWorkspacePtr taskArmarRight(new TaskExtendWorkspace(armar, Actor::RIGHT_TCP, Settings::Workspace::samplesExtend, true));
        factory->addTask(taskArmarRight);
    } else {
        extendArmarWS = false;
    }

    if (!extendHumanWS && !extendArmarWS) {
        VR_WARNING << "Nothing extended." << endl;
        return;
    }

    connectFactory(factory);
    factory->start();
}

void WorkspaceWidget::smoothWorkspace()
{
    HumanPtr human = world->getHuman();
    ArmarPtr armar = world->getArmar();

    TaskExecutor* factory = new TaskExecutor();
    TaskSmoothWorkspacePtr taskHuman(new TaskSmoothWorkspace(human, Settings::Workspace::smooth));
    TaskSmoothWorkspacePtr taskArmar(new TaskSmoothWorkspace(armar, Settings::Workspace::smooth));

    factory->addTask(taskHuman);
    factory->newThread();
    factory->addTask(taskArmar);

    connectFactory(factory);
    factory->start();
}

void WorkspaceWidget::openSettings()
{
    WorkspaceSettingsDialog dialog;
    dialog.exec();
}

PoseQualityMeasurementPtr WorkspaceWidget::getMeasure(Settings::Workspace::Measure measure, RobotNodeSetPtr rns)
{
    PoseQualityMeasurementPtr ret;
    switch(measure)
    {
    case Settings::Workspace::MANIPULABILITY:
        ret.reset(new PoseQualityManipulability(rns));
        break;
    case Settings::Workspace::EXT_MANIPULABILITY:
        ret.reset(new PoseQualityExtendedManipulability(rns));
        ((PoseQualityExtendedManipulability*) ret.get())->considerObstacles(true, 4, 4);
        break;
    case Settings::Workspace::COMFORT:
        ret.reset(new PoseQualityComfort(rns, getBodies(rns)));
        break;
    case Settings::Workspace::REACHABILITY:
        ret.reset(new PoseQualityReachability(rns));
        break;
    default:
        VR_ERROR "Unknown Measure." << endl;
    }
    return ret;
}

RobotNodeSetPtr WorkspaceWidget::getBodies(RobotNodeSetPtr rns)
{
    HumanPtr human = world->getHuman();
    RobotNodeSetPtr bodies;
    if (human->getRobot()->hasRobotNodeSet(rns->getName() + "_Bodies")) {
        bodies = human->getRobot()->getRobotNodeSet(rns->getName() + "_Bodies");
    }
    return bodies;
}

void WorkspaceWidget::showProgress()
{
    ui->progressBar->show();
}

void WorkspaceWidget::hideProgress()
{
    ui->progressBar->hide();
}

void WorkspaceWidget::on_btnBrowseLoadHumanLeft_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Select Workspace file (Human left side)"), "/home/harry/workspace/handover/data/workspace", tr("Workspace Files (*.bin);;All Files (*)"));
    if (!fileName.isEmpty()) {
        ui->edtLoadHumanLeft->setText(fileName);
    }
}

void WorkspaceWidget::on_btnBrowseLoadHumanRight_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Select Workspace file (Human right side)"), "/home/harry/workspace/handover/data/workspace", tr("Workspace Files (*.bin);;All Files (*)"));
    if (!fileName.isEmpty()) {
        ui->edtLoadHumanRight->setText(fileName);
    }
}

void WorkspaceWidget::on_btnBrowseLoadArmarLeft_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Select Workspace file (Armar left side)"), "/home/harry/workspace/handover/data/workspace", tr("Workspace Files (*.bin);;All Files (*)"));
    if (!fileName.isEmpty()) {
        ui->edtLoadArmarLeft->setText(fileName);
    }
}

void WorkspaceWidget::on_btnBrowseLoadArmarRight_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Select Workspace file (Armar right side)"), "/home/harry/workspace/handover/data/workspace", tr("Workspace Files (*.bin);;All Files (*)"));
    if (!fileName.isEmpty()) {
        ui->edtLoadArmarRight->setText(fileName);
    }
}

void WorkspaceWidget::on_btnBrowseSaveHumanLeft_clicked()
{
    QString fileName = QFileDialog::getSaveFileName(this, tr("Save Workspace (Human left side)"), "/home/harry/workspace/handover/data/workspace", tr("Workspace Files (*.bin);;All Files (*)"));
    if (!fileName.isEmpty() && !fileName.endsWith(".bin")) {
        fileName.append(".bin");
    }
    if (!fileName.isEmpty()) {
        ui->edtSaveHumanLeft->setText(fileName);
    }
}

void WorkspaceWidget::on_btnBrowseSaveHumanRight_clicked()
{
    QString fileName = QFileDialog::getSaveFileName(this, tr("Save Workspace (Human right side)"), "/home/harry/workspace/handover/data/workspace", tr("Workspace Files (*.bin);;All Files (*)"));
    if (!fileName.isEmpty() && !fileName.endsWith(".bin")) {
        fileName.append(".bin");
    }
    if (!fileName.isEmpty()) {
        ui->edtSaveHumanRight->setText(fileName);
    }
}

void WorkspaceWidget::on_btnBrowseSaveArmarLeft_clicked()
{
    QString fileName = QFileDialog::getSaveFileName(this, tr("Save Workspace (Armar left side)"), "/home/harry/workspace/handover/data/workspace", tr("Workspace Files (*.bin);;All Files (*)"));
    if (!fileName.isEmpty() && !fileName.endsWith(".bin")) {
        fileName.append(".bin");
    }
    if (!fileName.isEmpty()) {
        ui->edtSaveArmarLeft->setText(fileName);
    }
}

void WorkspaceWidget::on_btnBrowseSaveArmarRight_clicked()
{
    QString fileName = QFileDialog::getSaveFileName(this, tr("Save Workspace (Armar right side)"), "/home/harry/workspace/handover/data/workspace", tr("Workspace Files (*.bin);;All Files (*)"));
    if (!fileName.isEmpty() && !fileName.endsWith(".bin")) {
        fileName.append(".bin");
    }
    if (!fileName.isEmpty()) {
        ui->edtSaveArmarRight->setText(fileName);
    }
}
