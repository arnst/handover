#include "handoverwidget.h"
#include "ui_handoverwidget.h"

#include "equipdialog.h"
#include "../../settings.h"
#include "../../world.h"
#include "../../human.h"
#include "../../tasks/taskcreatehandoverspace.h"
#include "../../tasks/tasksolvehandoverik.h"

#include <cmath>

HandoverWidget::HandoverWidget(QWidget *parent) : QWidget(parent),
    ui(new Ui::HandoverWidget)
{
    ui->setupUi(this);
    connectUI();

    ui->progressBar->hide();
    ui->lblObjectCaption->hide();
    ui->lblObject->hide();
    ui->lblGraspCaption->hide();
    ui->lblGrasp->hide();
    ui->edtDiscrStepTrans->setValue(Settings::Handoverspace::discrStepTrans);
    ui->edtDiscrStepRot->setValue(Settings::Handoverspace::discrStepRot);

    currentRotation = 0;
}

HandoverWidget::~HandoverWidget()
{
    delete ui;
}

void HandoverWidget::connectUI()
{
    connect(ui->btnCreate, SIGNAL(clicked()), this, SLOT(createHandoverspace()));
    connect(ui->btnReset, SIGNAL(clicked()), this , SLOT(resetHandoverspace()));
    connect(ui->btnEquip, SIGNAL(clicked()), this, SLOT(equipArmar()));
    connect(ui->btnRemoveObject, SIGNAL(clicked()), this, SLOT(unEquipArmar()));
    connect(ui->btnSolveIK, SIGNAL(clicked()), this, SLOT(solveHandoverIK()));
    connect(ui->btnPrintHS, SIGNAL(clicked()), this, SLOT(printHS()));
    connect(ui->btnResetHandoverConfig, SIGNAL(clicked()), this, SLOT(resetHandoverConfig()));
    connect(ui->btnTestHS, SIGNAL(clicked()), this, SLOT(testHSPerformance()));

    connect(ui->edtDiscrStepTrans, SIGNAL(valueChanged(int)), this, SLOT(setTransStepSize(int)));
    connect(ui->edtDiscrStepRot, SIGNAL(valueChanged(double)), this, SLOT(setRotStepSize(double)));
}

void HandoverWidget::connectFactory(TaskExecutor *factory)
{
    connect(factory, SIGNAL(started()), world.get(), SLOT(lock()));
    connect(factory, SIGNAL(started()), this, SLOT(showProgress()));
    connect(factory, SIGNAL(finished()), this, SLOT(hideProgress()));
    connect(factory, SIGNAL(finished()), world.get(), SLOT(unlock()));
    connect(factory, SIGNAL(finished()), world.get(), SLOT(update()));
    connect(factory, SIGNAL(finished()), factory, SLOT(deleteLater()));
}

void HandoverWidget::onWorldUpdate()
{
    for (int i = 0; i < clonedHumanRobots.size(); i++)
    {
        RobotPtr clone = clonedHumanRobots.at(i);
        clone->setConfig(world->getHuman()->getRobot()->getConfig());
    }
}

void HandoverWidget::onWorldLock()
{
}

void HandoverWidget::onWorldUnlock()
{
}

void HandoverWidget::showProgress()
{
    ui->progressBar->show();
}

void HandoverWidget::hideProgress()
{
    ui->progressBar->hide();
}

void HandoverWidget::createHandoverspace()
{
    HumanPtr human = world->getHuman();
    ArmarPtr armar = world->getArmar();
    int graspCombinations = ui->edtGraspCombinations->value();

    if (!armar->getObject()) {
        VR_ERROR << "No valid manipulation object found!" << endl;
        return;
    }

    armar->resetHandoverSpace();
//    human->enableVisualization(false);
    TaskExecutor* factory = new TaskExecutor();

    int threads = QThread::idealThreadCount();
    threads = (threads < 1) ? 1 : threads;
    float range = 1.0f / threads;

    if (clonedHumanRobots.size() == 0)
    {
        for (int i = 0; i < threads; i++)
        {
            // since robots are not threadsafe, we just create our local copy
            CollisionCheckerPtr cc(new CollisionChecker());
            RobotPtr robot = human->getRobot()->clone("tmpHuman_" + QString::number(i).toStdString(), cc);
            clonedHumanRobots.push_back(robot);
        }
    }

    boost::shared_ptr<boost::mutex> mutex(new boost::mutex());
    for (int i = 0; i < threads; i++)
    {
        TaskCreateHandoverspacePtr task(new TaskCreateHandoverspace(armar, armar->getObjectSide(), human, graspCombinations));
        QPair<float,float> interval(i * range, (i+1) * range);

        task->setConcurrent(interval, mutex, clonedHumanRobots.at(i));
        factory->addTask(task);
        if (i < threads - 1) factory->newThread();
    }

    connectFactory(factory);
    factory->start();
}

void HandoverWidget::resetHandoverspace()
{
    world->getArmar()->resetHandoverSpace();
    world->update();
}

void HandoverWidget::equipArmar()
{
    ArmarPtr armar = world->getArmar();

    EquipDialog dialog(armar);
    if (dialog.exec() == QDialog::Accepted)
    {
        ui->lblObjectCaption->show();
        ui->lblObject->show();
        ui->lblObject->setText(QString::fromStdString(dialog.getObject()->getName()));

        if (dialog.usingAllGrasps()) {
            armar->equip(dialog.getTcpSide(), dialog.getObject());
            ui->lblGraspCaption->show();
            ui->lblGrasp->show();
            ui->lblGrasp->setText("-All-");
        }
        else {
            armar->equip(dialog.getObject(), dialog.getGrasp());
            ui->lblGraspCaption->show();
            ui->lblGrasp->show();
            ui->lblGrasp->setText(QString::fromStdString(dialog.getGrasp()->getName()));
        }

        resetHandoverspace();
    }
    world->update();
}

void HandoverWidget::unEquipArmar()
{
    ui->lblObjectCaption->hide();
    ui->lblObject->hide();
    ui->lblObject->clear();

    ui->lblGraspCaption->hide();
    ui->lblGrasp->hide();
    ui->lblGrasp->clear();

    world->getArmar()->unEquip();
    world->update();
}

void HandoverWidget::solveHandoverIK()
{
    TaskExecutor* factory = new TaskExecutor();
    HumanPtr human = world->getHuman();
    ArmarPtr armar = world->getArmar();

    TaskSolveHandoverIKPtr planner(new TaskSolveHandoverIK(armar, human, 16));
    factory->addTask(planner);

    factory->run(); // synchronous
    world->setHandoverPose(planner->getBestHandoverPose());
    world->update();

    delete factory;
}

void HandoverWidget::printHS()
{
    world->getArmar()->getHandoverSpace()->print();
}

void HandoverWidget::resetHandoverConfig()
{
    world->getArmar()->resetRobotConfig();
    world->getHuman()->resetRobotConfig();
    world->update();
}

void HandoverWidget::setTransStepSize(int value)
{
    Settings::Handoverspace::discrStepTrans = value;
}

void HandoverWidget::setRotStepSize(double value)
{
    Settings::Handoverspace::discrStepRot = value;
}

void HandoverWidget::enableArmarVisualization()
{
    world->getHuman()->enableVisualization(true);
}




void HandoverWidget::testHSPerformance()
{
    ArmarPtr armar = world->getArmar();
    HumanPtr human = world->getHuman();

    if (!armar->getObject()) {
        VR_ERROR << "No valid manipulation object found!" << endl;
        return;
    }

    int graspCombinations = 1;
    int countDiscr = 1;

    float totalDuration = 0; // seconds
    unsigned int totalVoxelFillCount = 0;
    unsigned int totalVoxelEntries = 0;
    unsigned int highestEntries = 0;
    int steps = 10;
    float durations[steps];
    float voxelFillCounts[steps];

    for (int i = 0; i < steps ; i++)
    {
        armar->resetHandoverSpace();
        TaskExecutor* factory = new TaskExecutor();

        int threads = QThread::idealThreadCount();
        threads = (threads < 1) ? 1 : threads;
        float range = 1.0f / threads;

        if (clonedHumanRobots.size() == 0)
        {
            for (int i = 0; i < threads; i++)
            {
                // since robots are not threadsafe, we just create our local copy
                CollisionCheckerPtr cc(new CollisionChecker());
                RobotPtr robot = human->getRobot()->clone("tmpHuman_" + QString::number(i).toStdString(), cc);
                clonedHumanRobots.push_back(robot);
            }
        }

        boost::shared_ptr<boost::mutex> mutex(new boost::mutex());
        for (int t = 0; t < threads; t++)
        {
            boost::shared_ptr<TaskCreateHandoverspace> task(new TaskCreateHandoverspace(armar, armar->getObjectSide(), human, graspCombinations));
            QPair<float,float> interval(t * range, (t+1) * range);

            task->setConcurrent(interval, mutex, clonedHumanRobots.at(t));
            factory->addTask(task);
            if (t < threads - 1) factory->newThread();
        }
        QElapsedTimer timer;
        timer.start();
        factory->run(); // synchron

        float elapsed = timer.elapsed() / 1000.0f;
        durations[i] = elapsed;
        totalDuration += elapsed;
        totalVoxelFillCount += armar->getHandoverSpace()->getData()->getVoxelFilledCount();
        voxelFillCounts[i] = armar->getHandoverSpace()->getData()->getVoxelFilledCount();
        highestEntries += armar->getHandoverSpace()->getMaxEntry();

        WorkspaceRepresentationPtr hs = armar->getHandoverSpace();
        for (int x = 0; x < hs->getNumVoxels(0); x++) {
            for (int y = 0; y < hs->getNumVoxels(1); y++) {
                for (int z = 0; z < hs->getNumVoxels(2); z++) {
                    for (int a = 0; a < hs->getNumVoxels(3); a++) {
                        for (int b = 0; b < hs->getNumVoxels(4); b++) {
                            for (int c = 0; c < hs->getNumVoxels(5); c++) {
                                totalVoxelEntries += hs->getVoxelEntry(x,y,z,a,b,c);
                            }
                        }
                    }
                }
            }
        }


    }


    for (int i = 0; i < countDiscr; i++)
    {
        float averageDuration = totalDuration / steps;
        float averageVoxelFillCount = (float)totalVoxelFillCount / (float)steps;
        float averageVoxelEntry = ((float)totalVoxelEntries / (float)steps) / averageVoxelFillCount;
        float averageHighestEntry = (float)highestEntries / (float)steps;

        float sumDurationsV = 0;
        float sumVoxelfillCountsV = 0;
        for (int j = 0; j < steps ; j++)
        {
            sumDurationsV += (durations[j] - averageDuration) * (durations[j] - averageDuration);
            sumVoxelfillCountsV += (voxelFillCounts[j] - averageVoxelFillCount) * (voxelFillCounts[j] - averageVoxelFillCount);
        }
        float sdDuration = std::sqrt(sumDurationsV / (steps - 1));
        float sdVoxelFillCount = std::sqrt(sumVoxelfillCountsV / (steps - 1));

        cout << "***************************************************" << endl;
        cout << "Grasp Combinations: " << graspCombinations << endl;
        cout << "Discretization: " << Settings::Handoverspace::discrStepTrans << " mm   " << Settings::Handoverspace::discrStepRot << " rad" << endl;
        cout << "Steps: " << steps << endl;
        cout << "Total Duration : " << totalDuration << " s    Average Duration: " << averageDuration << " s" << endl;
        cout << "Total Voxel Fill Count: " << totalVoxelFillCount << "    Average Voxel Fill Count: " << averageVoxelFillCount << endl;
        cout << "Total Voxel entries: " << totalVoxelEntries << "    Average Voxel Entry: " << averageVoxelEntry << endl;
        cout << "Total Highest Entries: " << highestEntries << "    Average Highest Entry: " << averageHighestEntry << endl;
        cout << "Standard Deviation:    Duration: " << sdDuration << "    Voxel Fill Count: " << sdVoxelFillCount << endl;
        cout << "***************************************************" << endl << endl << endl << endl << endl << endl << endl << endl;
    }
}
