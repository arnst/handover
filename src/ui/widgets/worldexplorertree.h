#ifndef WORLDEXPLORERTREE_H
#define WORLDEXPLORERTREE_H

#include "../../actor.h"
#include "../../worldlistener.h"
#include "../../world.h"

#include <QtGui>

class WorldExplorerTree : public QTreeWidget, public WorldListener
{
    Q_OBJECT

public:
    explicit WorldExplorerTree(QWidget *parent = 0);

    virtual void onWorldUpdate();
    virtual void onWorldLock();
    virtual void onWorldUnlock();

signals:

public slots:

};

#endif // WORLDEXPLORERTREE_H
