#ifndef WORLDEXPLORERWIDGET_H
#define WORLDEXPLORERWIDGET_H

#include <QWidget>

namespace Ui {
class WorldExplorerWidget;
}

class WorldExplorerWidget : public QWidget
{
    Q_OBJECT

public:
    explicit WorldExplorerWidget(QWidget *parent = 0);
    ~WorldExplorerWidget();

private:
    Ui::WorldExplorerWidget *ui;
};

#endif // WORLDEXPLORERWIDGET_H
