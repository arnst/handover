#include "equipdialog.h"
#include "ui_equipdialog.h"

#include <VirtualRobot/XML/ObjectIO.h>

EquipDialog::EquipDialog(ArmarPtr armar, QWidget *parent) : QDialog(parent),
    ui(new Ui::EquipDialog), useAllGrasps("-All-")
{
    this->armar = armar;
    objectFileName = "";
    setupUI();
}

EquipDialog::~EquipDialog()
{
    delete ui;
}

void EquipDialog::setupUI()
{
    ui->setupUi(this);
    ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(false);
    ui->lblWarningNoGrasps->setText("<font color=\"red\"> No valid grasps found.</font>");
    ui->lblWarningNoGrasps->hide();

    connect(ui->btnSelectObject, SIGNAL(clicked()), this, SLOT(selectObject()));
    connect(ui->comboBoxGrasps, SIGNAL(currentIndexChanged(QString)), this, SLOT(setGrasp(QString)));
}

GraspPtr EquipDialog::getGrasp()
{
    return grasp;
}

bool EquipDialog::usingAllGrasps()
{
    return ui->comboBoxGrasps->currentText() == useAllGrasps;
}

Actor::TCP EquipDialog::getTcpSide()
{
    return (ui->rBtnLeftHand->isChecked()) ? Actor::LEFT_TCP : Actor::RIGHT_TCP;
}

ManipulationObjectPtr EquipDialog::getObject()
{
    return object;
}

void EquipDialog::selectObject()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Select Manipulation Object"), "/home/harry/workspace/handover/data/objects", tr("Object Files (*.xml);;All Files (*)"));

    if (!fileName.isEmpty())
    {
        ui->btnSelectObject->setText(QFileInfo(fileName).fileName());
        object = ObjectIO::loadManipulationObject(fileName.toStdString());
        loadGraspList(getTcpSide());
    }
    else
    {
        if (object) {
            ui->btnSelectObject->setText(QFileInfo(QString::fromStdString(object->getFilename())).fileName());
        }
        else {
            ui->btnSelectObject->setText("Select Object");
            ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(false);
        }
    }
}

void EquipDialog::setGrasp(QString name)
{
    if (!object)
        return;

    grasp.reset();
    std::string eefString = ui->rBtnLeftHand->isChecked() ? "Hand L" : "Hand R";
    if (object->hasGraspSet(armar->getRobot()->getType(), eefString))
    {
        GraspSetPtr graspSet = object->getGraspSet(armar->getRobot()->getType(), eefString);
        grasp = graspSet->getGrasp(name.toStdString());
    }
}

void EquipDialog::loadGraspList(Actor::TCP side)
{
    if (!object)
        return;

    std::string eefString = (side == Actor::LEFT_TCP) ? "Hand L" : "Hand R";
    ui->comboBoxGrasps->clear();

    if (object->hasGraspSet(armar->getRobot()->getType(), eefString))
    {
        GraspSetPtr graspSet = object->getGraspSet(armar->getRobot()->getType(), eefString);

        ui->comboBoxGrasps->addItem(useAllGrasps);
        for (int i = 0; i < graspSet->getSize(); i++)
        {
            ui->comboBoxGrasps->addItem(QString::fromStdString(graspSet->getGrasp(i)->getName()));
        }
        ui->lblWarningNoGrasps->hide();
        ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(true);
    }
    else
    {
        VR_WARNING << "No grasps found for {Object: " << object->getName() << ", Robot: " << armar->getRobot()->getType() << ", EEF: " << eefString << "}" << endl;
        ui->lblWarningNoGrasps->show();
        ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(false);
    }
}

void EquipDialog::on_rBtnLeftHand_toggled(bool checked)
{
    if (checked) loadGraspList(Actor::LEFT_TCP);
}

void EquipDialog::on_rBtnRightHand_toggled(bool checked)
{
    if (checked) loadGraspList(Actor::RIGHT_TCP);
}


























