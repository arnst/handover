#ifndef EQUIPDIALOG_H
#define EQUIPDIALOG_H

#include "../../actor.h"
#include "../../armar.h"

#include <VirtualRobot/ManipulationObject.h>
#include <VirtualRobot/Grasping/Grasp.h>

#include <QtGui>

namespace Ui {
class EquipDialog;
}

using namespace VirtualRobot;

class EquipDialog : public QDialog
{
    Q_OBJECT

public:
    explicit EquipDialog(ArmarPtr armar, QWidget *parent = 0);
    ~EquipDialog();
    GraspPtr getGrasp();
    bool usingAllGrasps();
    Actor::TCP getTcpSide();
    ManipulationObjectPtr getObject();

private slots:
    void selectObject();
    void setGrasp(QString name);

    void on_rBtnLeftHand_toggled(bool checked);

    void on_rBtnRightHand_toggled(bool checked);

private:
    Ui::EquipDialog *ui;
    const QString useAllGrasps;

    ManipulationObjectPtr object;
    GraspPtr grasp;

    ArmarPtr armar;
    QString objectFileName;

    void setupUI();
    void loadGraspList(Actor::TCP side);
};

#endif // EQUIPDIALOG_H
