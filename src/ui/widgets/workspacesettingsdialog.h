#ifndef WORKSPACESETTINGSDIALOG_H
#define WORKSPACESETTINGSDIALOG_H

#include <QDialog>

namespace Ui {
class WorkspaceSettingsDialog;
}

class WorkspaceSettingsDialog : public QDialog
{
    Q_OBJECT

public:
    explicit WorkspaceSettingsDialog(QWidget *parent = 0);
    ~WorkspaceSettingsDialog();

private:
    Ui::WorkspaceSettingsDialog *ui;

    void setupUI();

private slots:
    void apply();
};

#endif // WORKSPACESETTINGSDIALOG_H
