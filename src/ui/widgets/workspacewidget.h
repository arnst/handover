#ifndef WORKSPACEWIDGET_H
#define WORKSPACEWIDGET_H

#include "../../actor.h"
#include "../../worldlistener.h"
#include "../../world.h"
#include "../../settings.h"
#include "../../tasks/taskexecutor.h"

#include "VirtualRobot/RobotNodeSet.h"
#include "VirtualRobot/IK/PoseQualityMeasurement.h"

#include <QtGui>

namespace Ui {
class WorkspaceWidget;
}

class WorkspaceWidget : public QWidget, public WorldListener
{
    Q_OBJECT

public:
    explicit WorkspaceWidget(QWidget *parent = 0);
    ~WorkspaceWidget();

    virtual void onWorldUpdate();
    virtual void onWorldLock();
    virtual void onWorldUnlock();

private:
    Ui::WorkspaceWidget *ui;

    void setupUI();
    void connectUI();
    void connectFactory(TaskExecutor *factory);
    PoseQualityMeasurementPtr getMeasure(Settings::Workspace::Measure measure, RobotNodeSetPtr rns);
    RobotNodeSetPtr getBodies(RobotNodeSetPtr rns);

public slots:
    void showProgress();
    void hideProgress();

private slots:
    void createWorkspace();
    void resetWorkspace();
    void loadWorkspace();
    void saveWorkspace();
    void printWorkspace();
    void extendWorkspace();
    void smoothWorkspace();
    void openSettings();
    void on_btnBrowseLoadHumanLeft_clicked();
    void on_btnBrowseLoadHumanRight_clicked();
    void on_btnBrowseLoadArmarLeft_clicked();
    void on_btnBrowseLoadArmarRight_clicked();
    void on_btnBrowseSaveHumanLeft_clicked();
    void on_btnBrowseSaveHumanRight_clicked();
    void on_btnBrowseSaveArmarLeft_clicked();
    void on_btnBrowseSaveArmarRight_clicked();
};

#endif // WORKSPACEWIDGET_H
