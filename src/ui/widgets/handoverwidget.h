#ifndef HANDOVERWIDGET_H
#define HANDOVERWIDGET_H

#include "../../worldlistener.h"
#include "../../actor.h"
#include "../../tasks/taskexecutor.h"

#include <QtGui>

namespace Ui {
class HandoverWidget;
}

class HandoverWidget : public QWidget, public WorldListener
{
    Q_OBJECT

public:
    explicit HandoverWidget(QWidget *parent = 0);
    ~HandoverWidget();

    virtual void onWorldUpdate();
    virtual void onWorldLock();
    virtual void onWorldUnlock();

private:
    Ui::HandoverWidget *ui;

    float currentRotation;
    std::vector< RobotPtr > clonedHumanRobots;

    void connectUI();
    void connectFactory(TaskExecutor *factory);

public slots:
    void showProgress();
    void hideProgress();

private slots:
    void createHandoverspace();
    void resetHandoverspace();
    void equipArmar();
    void unEquipArmar();
    void solveHandoverIK();
    void printHS();
    void resetHandoverConfig();
    void setTransStepSize(int value);
    void setRotStepSize(double value);
    void enableArmarVisualization();
    void testHSPerformance();
};

#endif // HANDOVERWIDGET_H
