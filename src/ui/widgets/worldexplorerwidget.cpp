#include "worldexplorerwidget.h"
#include "ui_worldexplorerwidget.h"

WorldExplorerWidget::WorldExplorerWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::WorldExplorerWidget)
{
    ui->setupUi(this);
}

WorldExplorerWidget::~WorldExplorerWidget()
{
    delete ui;
}
