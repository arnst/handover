#include "actorvisualizationfactory.h"

#include <Inventor/So.h>
#include <VirtualRobot/Nodes/RobotNode.h>

SoNode* ActorVisualizationFactory::getWorkspaceVisualization(WorkspaceRepresentationPtr ws, float cutZ, const VirtualRobot::ColorMap cm, bool transformToGlobalPose, ColorMode mode)
{
    SoSeparator* res = new SoSeparator;
    if (!ws->getData() || (cutZ == 0))
        return res;

    res->ref();
    float numVoxels[6];
    float spaceSize[6];
    float minBounds[6];

    for (int i = 0; i < 6; i++)
    {
        numVoxels[i] = ws->getNumVoxels(i);
        spaceSize[i] = ws->getMaxBound(i) - ws->getMinBound(i);
        minBounds[i] = ws->getMinBound(i);
    }

    if (!ws || numVoxels[0] <= 0 || numVoxels[1] <= 0 || numVoxels[2] <= 0)
    {
        res->unrefNoDelete();
        return res;
    }

    Eigen::Vector3f size;
    size(0) = spaceSize[0] / numVoxels[0];
    size(1) = spaceSize[1] / numVoxels[1];
    size(2) = spaceSize[2] / numVoxels[2];
    float minS = size(0);

    if (size(1) < minS)
    {
        minS = size(1);
    }

    if (size(2) < minS)
    {
        minS = size(2);
    }

    VirtualRobot::VisualizationFactory::Color color = VirtualRobot::VisualizationFactory::Color::None();
    float radius = minS * 0.5f * 0.75f;
    Eigen::Vector3f voxelPosition;
    int step = 1;
    int maxValue = 0;

    if (mode == SUMMED)
    {
        // determine max summed values of a 3D-Voxel
        for (int a = 0; a < numVoxels[0]; a += step)
        {
            for (int b = 0; b < numVoxels[1]; b += step)
            {
                for (int c = 0; c < numVoxels[2]; c += step)
                {
                    int value = ws->sumAngleReachabilities(a, b, c);

                    if (value >= maxValue)
                    {
                        maxValue = value;
                    }
                }
            }
        }
    }
    else if (mode == MAX)
    {
        maxValue = ws->getMaxEntry();
    }

    Eigen::Vector3f resPos;

    for (int a = 0; a < numVoxels[0]; a += step)
    {
        voxelPosition(0) = minBounds[0] + (a + 0.5f) * size(0);

        for (int b = 0; b < numVoxels[1]; b += step)
        {
            voxelPosition(1) = minBounds[1] + (b + 0.5f) * size(1);

            int zEnd = numVoxels[2] * cutZ;

            for (int c = 0; c < zEnd; c += step)
            {
                voxelPosition(2) = minBounds[2] + (c + 0.5f) * size(2);
                int value = 0;

                if (mode == SUMMED)
                {
                    value = ws->sumAngleReachabilities(a, b, c);
                }
                else if (mode == MAX)
                {
                    value = ws->getMaxEntry(a, b, c);
                }

                if (value > 0)
                {
                    resPos = voxelPosition;

                    if (transformToGlobalPose)
                    {
                        ws->toGlobalVec(resPos);
                    }

                    float intensity = (float)value;

                    if (maxValue > 0)
                    {
                        intensity /= maxValue;
                    }

                    if (intensity > 1.0f)
                    {
                        intensity = 1.0f;
                    }

                    color = cm.getColor(intensity);

                    SoNode* n = CoinVisualizationFactory::CreateVertexVisualization(resPos, radius, color.transparency, color.r, color.g, color.b);

                    if (n)
                    {
                        res->addChild(n);
                    }
                }
            }
        }
    }

    // place the workspace cut on top of the visualized workspace
    float posZ = ws->getMinBound(2) + (fabs(ws->getMaxBound(2) - ws->getMinBound(2)) * cutZ);
    posZ += ws->getBaseNode()->getGlobalPose()(2,3); // translate to global z
    WorkspaceCutSummedAnglesPtr cut = createCut(ws, posZ, ws->getDiscretizeParameterTranslation() / 4.0f, mode);
    smoothWorkspaceCut(cut);
    res->addChild(getCutVisualization(cut, maxValue, ColorMap(ColorMap::eHot), Eigen::Vector3f::UnitZ()));

    res->unrefNoDelete();
    return res;
}

ActorVisualizationFactory::WorkspaceCutSummedAnglesPtr ActorVisualizationFactory::createCut(WorkspaceRepresentationPtr ws, float posZ, float cellSize, ColorMode mode)
{
    WorkspaceCutSummedAnglesPtr cut(new WorkspaceCutSummedAngles());
    cut->workspace = ws;
    cut->posZ = posZ;

    Eigen::Vector3f minBB, maxBB;
    ws->getWorkspaceExtends(minBB, maxBB);
    cut->minBounds[0] = minBB(0);
    cut->maxBounds[0] = maxBB(0);
    cut->minBounds[1] = minBB(1);
    cut->maxBounds[1] = maxBB(1);

    THROW_VR_EXCEPTION_IF(cellSize <= 0.0f, "Invalid parameter");

    float sizeX = cut->maxBounds[0] - cut->minBounds[0];
    int numVoxelsX = (int)(sizeX / cellSize);
    float sizeY = cut->maxBounds[1] - cut->minBounds[1];
    int numVoxelsY = (int)(sizeY / cellSize);

    cut->entries.resize(numVoxelsX, numVoxelsY);


    for (int x = 0; x < numVoxelsX; x++)
    {
        float posX = cut->minBounds[0] + (float)x * cellSize + 0.5f * cellSize;

        for (int y = 0; y < numVoxelsY; y++)
        {
            float posY = cut->minBounds[1] + (float)y * cellSize + 0.5f * cellSize;
//            Eigen::Matrix4f globalPose = Eigen::Matrix4f::Identity();
            Eigen::Matrix4f globalPose = ws->getBaseNode()->getGlobalPose();
            globalPose(0,3) = posX;
            globalPose(1,3) = posY;
            globalPose(2,3) = posZ;
            unsigned int v[6];
            ws->getVoxelFromPose(globalPose, v);
            if (mode == SUMMED)
            {
                cut->entries(x, y) = ws->sumAngleReachabilities(v[0], v[1], v[2]);
            }
            else if (mode == MAX)
            {
                if (ws->hasEntry(v[0], v[1], v[2])) {
                    cut->entries(x, y) = ws->getMaxEntry(v[0], v[1], v[2]);
                } else {
                    cut->entries(x, y) = 0;
                }
            }
        }
    }

    return cut;
}

SoNode* ActorVisualizationFactory::getCutVisualization(ActorVisualizationFactory::WorkspaceCutSummedAnglesPtr cut, int maxEntry, ColorMap cm, const Eigen::Vector3f &normal)
{
    SoSeparator* res = new SoSeparator;
    if (!cut)
        return res;

    res->ref();
    SoUnits* u = new SoUnits();
    u->units = SoUnits::MILLIMETERS;
    res->addChild(u);

    //Eigen::Matrix4f gp = cutXY->referenceGlobalPose;
    Eigen::Matrix4f gp = Eigen::Matrix4f::Identity();
    // set z component
    gp(2, 3) = cut->posZ;

    int nX = cut->entries.rows();
    int nY = cut->entries.cols();

    float sizeX = (cut->maxBounds[0] - cut->minBounds[0]) / (float)nX;
    float sizeY = (cut->maxBounds[1] - cut->minBounds[1]) / (float)nY;


    float ro, gr, bl;
    SoCube* cube = new SoCube();

    if (normal(0) > 0)
    {
        cube->width = sizeX;
        cube->height = 1.0;
        cube->depth = sizeY;
    }
    else if (normal(1) > 0)
    {
        cube->width = 1.0f;
        cube->height = sizeY;
        cube->depth = sizeX;
    }
    else
    {
        cube->width = sizeX;
        cube->height = sizeY;
        cube->depth = 1.0f;
    }

    if (maxEntry == 0)
    {
        maxEntry = 1;
    }

    SoDrawStyle* ds = new SoDrawStyle;
    ds->style = SoDrawStyle::POINTS;

    // back-face culling
    SoShapeHints* shapeHints = new SoShapeHints;
    //shapeHints->vertexOrdering = SoShapeHints::COUNTERCLOCKWISE;
    shapeHints->vertexOrdering = SoShapeHints::UNKNOWN_ORDERING;
    shapeHints->shapeType = SoShapeHints::SOLID;

    SoBaseColor* bc = new SoBaseColor;
    bc->rgb.setValue(0, 0, 0);

    // keep a solid color
    SoLightModel* lightModel = new SoLightModel;
    lightModel->model = SoLightModel::BASE_COLOR;

    for (int x = 0; x < nX; x++)
    {
        float xPos = cut->minBounds[0] + (float)x * sizeX + 0.5f * sizeX; // center of voxel

        for (int y = 0; y < nY; y++)
        {
            int v = cut->entries(x, y);

            if (v > 0)
            {
                float yPos = cut->minBounds[1] + (float)y * sizeY + 0.5f * sizeY; // center of voxel
                gp(0, 3) = xPos;
                gp(1, 3) = yPos;

                SoSeparator* sep1 = new SoSeparator();
                SoMatrixTransform* matTr = CoinVisualizationFactory::getMatrixTransform(gp); // we are in mm unit environment -> no conversion to m needed

                float intensity = (float)v;
                intensity /= maxEntry;

                if (intensity > 1.0f)
                {
                    intensity = 1.0f;
                }

                VirtualRobot::VisualizationFactory::Color color = cm.getColor(intensity);

                SoMaterial* mat = new SoMaterial();

                ro = color.r;
                gr = color.g;
                bl = color.b;

                mat->diffuseColor.setValue(ro, gr, bl);
                mat->ambientColor.setValue(ro, gr, bl);

                if (intensity > 0)
                {
                    sep1->addChild(matTr);
                    sep1->addChild(mat);
                    sep1->addChild(cube);

                    SoSeparator* pSepLines = new SoSeparator;
                    sep1->addChild(pSepLines);

                    pSepLines->addChild(ds);
                    pSepLines->addChild(shapeHints);
                    pSepLines->addChild(lightModel);
                    pSepLines->addChild(bc);
                    pSepLines->addChild(cube);

                    res->addChild(sep1);
                }
            }
        }
    }

    res->unrefNoDelete();
    return res;
}

void ActorVisualizationFactory::smoothWorkspaceCut(ActorVisualizationFactory::WorkspaceCutSummedAnglesPtr cut)
{
    int nY = cut->entries.rows();
    int nX = cut->entries.cols();

    if (nX < 3 || nY < 3)
    {
        VR_ERROR << "Invalid WorkspaceCut dimensions!" << endl;
    }

    Eigen::MatrixXi tmp = cut->entries;

    Eigen::Matrix<float,7,7> gauss; // kernel
//    gauss(0,0) = 0.0625f; // 1/16
//    gauss(0,1) = 0.125f; // 2/16
//    gauss(0,2) = 0.0625f; // 1/16
//    gauss(1,0) = 0.125f; // 2/16
//    gauss(1,1) = 0.25f; // 4/16
//    gauss(1,2) = 0.125f; // 2/16
//    gauss(2,0) = 0.0625f; // 1/16
//    gauss(2,1) = 0.125f; // 2/16
//    gauss(2,2) = 0.0625f; // 1/16

    gauss << 0.00000067, 0.00002292, 0.00019117, 0.00038771, 0.00019117, 0.00002292, 0.00000067,
            0.00002292, 0.00078633, 0.00655965, 0.01330373, 0.00655965, 0.00078633, 0.00002292,
            0.00019117, 0.00655965, 0.05472157, 0.11098164, 0.05472157, 0.00655965, 0.00019117,
            0.00038771, 0.01330373, 0.11098164, 0.22508352, 0.11098164, 0.01330373, 0.00038771,
            0.00019117, 0.00655965, 0.05472157, 0.11098164, 0.05472157, 0.00655965, 0.00019117,
            0.00002292, 0.00078633, 0.00655965, 0.01330373, 0.00655965, 0.00078633, 0.00002292,
            0.00000067, 0.00002292, 0.00019117, 0.00038771, 0.00019117, 0.00002292, 0.00000067;

//    for (int y = 1; y < nY - 1; y++)
//    {
//        for (int x = 1; x < nX - 1; x++)
//        {
//            float smoothedValue = 0;

//            // top row
//            if (y >= 1 && x >= 1)       smoothedValue += gauss(0,0) * tmp(y-1,x-1);
//            if (y >= 1)                 smoothedValue += gauss(0,1) * tmp(y-1,x);
//            if (y >= 1 && x <= nX-2)    smoothedValue += gauss(0,2) * tmp(y-1,x+1);
//            // middle row
//            if (x >= 1)                 smoothedValue += gauss(1,0) * tmp(y,x-1);
//                                        smoothedValue += gauss(1,1) * tmp(y,x);
//            if (x <= nX-2)              smoothedValue += gauss(1,2) * tmp(y,x+1);
//            // bot row
//            if (y <= nY-2 && x >= 1)    smoothedValue += gauss(2,0) * tmp(y+1, x-1);
//            if (y <= nY-2)              smoothedValue += gauss(2,1) * tmp(y+1,x);
//            if (y <= nY-2 && x <= nX-2) smoothedValue += gauss(2,2) * tmp(y+1,x+1);

//            cut->entries(y,x) = (int) smoothedValue;
//        }
//    }

    for (int y = 3; y < nY - 3; y++)
    {
        for (int x = 3; x < nX - 3; x++)
        {
            float smoothedValue = 0;

            // 1. row
            smoothedValue += gauss(0,0) * tmp(y-3,x-3);
            smoothedValue += gauss(0,1) * tmp(y-3,x-2);
            smoothedValue += gauss(0,2) * tmp(y-3,x-1);
            smoothedValue += gauss(0,3) * tmp(y-3,x);
            smoothedValue += gauss(0,4) * tmp(y-3,x+1);
            smoothedValue += gauss(0,5) * tmp(y-3,x+2);
            smoothedValue += gauss(0,6) * tmp(y-3,x+3);
            // 2. row
            smoothedValue += gauss(1,0) * tmp(y-2,x-3);
            smoothedValue += gauss(1,1) * tmp(y-2,x-2);
            smoothedValue += gauss(1,2) * tmp(y-2,x-1);
            smoothedValue += gauss(1,3) * tmp(y-2,x);
            smoothedValue += gauss(1,4) * tmp(y-2,x+1);
            smoothedValue += gauss(1,5) * tmp(y-2,x+2);
            smoothedValue += gauss(1,6) * tmp(y-2,x+3);
            // 3. row
            smoothedValue += gauss(2,0) * tmp(y-1,x-3);
            smoothedValue += gauss(2,1) * tmp(y-1,x-2);
            smoothedValue += gauss(2,2) * tmp(y-1,x-1);
            smoothedValue += gauss(2,3) * tmp(y-1,x);
            smoothedValue += gauss(2,4) * tmp(y-1,x+1);
            smoothedValue += gauss(2,5) * tmp(y-1,x+2);
            smoothedValue += gauss(2,6) * tmp(y-1,x+3);
            // 4. row
            smoothedValue += gauss(3,0) * tmp(y,x-3);
            smoothedValue += gauss(3,1) * tmp(y,x-2);
            smoothedValue += gauss(3,2) * tmp(y,x-1);
            smoothedValue += gauss(3,3) * tmp(y,x);
            smoothedValue += gauss(3,4) * tmp(y,x+1);
            smoothedValue += gauss(3,5) * tmp(y,x+2);
            smoothedValue += gauss(3,6) * tmp(y,x+3);
            // 5. row
            smoothedValue += gauss(4,0) * tmp(y+1,x-3);
            smoothedValue += gauss(4,1) * tmp(y+1,x-2);
            smoothedValue += gauss(4,2) * tmp(y+1,x-1);
            smoothedValue += gauss(4,3) * tmp(y+1,x);
            smoothedValue += gauss(4,4) * tmp(y+1,x+1);
            smoothedValue += gauss(4,5) * tmp(y+1,x+2);
            smoothedValue += gauss(4,6) * tmp(y+1,x+3);
            // 6. row
            smoothedValue += gauss(5,0) * tmp(y+2,x-3);
            smoothedValue += gauss(5,1) * tmp(y+2,x-2);
            smoothedValue += gauss(5,2) * tmp(y+2,x-1);
            smoothedValue += gauss(5,3) * tmp(y+2,x);
            smoothedValue += gauss(5,4) * tmp(y+2,x+1);
            smoothedValue += gauss(5,5) * tmp(y+2,x+2);
            smoothedValue += gauss(5,6) * tmp(y+2,x+3);
            // 7. row
            smoothedValue += gauss(6,0) * tmp(y+3,x-3);
            smoothedValue += gauss(6,1) * tmp(y+3,x-2);
            smoothedValue += gauss(6,2) * tmp(y+3,x-1);
            smoothedValue += gauss(6,3) * tmp(y+3,x);
            smoothedValue += gauss(6,4) * tmp(y+3,x+1);
            smoothedValue += gauss(6,5) * tmp(y+3,x+2);
            smoothedValue += gauss(6,6) * tmp(y+3,x+3);

            cut->entries(y,x) = (int) smoothedValue;
        }
    }
}













































