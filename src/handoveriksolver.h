#ifndef HANDOVERIKSOLVER_H
#define HANDOVERIKSOLVER_H

#include <VirtualRobot/IK/GenericIKSolver.h>

using namespace VirtualRobot;

class HandoverIKSolver : public GenericIKSolver
{
public:
    HandoverIKSolver(RobotNodeSetPtr rns, JacobiProvider::InverseJacobiMethod invJacMethod = JacobiProvider::eSVD);
    virtual bool solve(const Eigen::Matrix4f& globalPose, CartesianSelection selection = All, int maxLoops = 1);
    void setJointWeights(Eigen::VectorXf weights);

protected:
    Eigen::VectorXf jointWeights;

    void randomizeJointsWeighted();
};

typedef boost::shared_ptr<HandoverIKSolver> HandoverIKSolverPtr;

#endif // HANDOVERIKSOLVER_H
