#include "world.h"
#include <boost/make_shared.hpp>

World::World(HumanPtr human, ArmarPtr armar)
{
    this->human = human;
    this->armar = armar;
}

void World::addListener(WorldListener *listener)
{
    listener->setWorld(shared_from_this());
    listeners.append(listener);
}

void World::update()
{
    render();

    BOOST_FOREACH(WorldListener* listener, listeners)
    {
        listener->onWorldUpdate();
    }
}

void World::lock()
{
    BOOST_FOREACH(WorldListener* listener, listeners)
    {
        listener->onWorldLock();
    }
}

void World::unlock()
{
    BOOST_FOREACH(WorldListener* listener, listeners)
    {
        listener->onWorldUnlock();
    }
}

void World::render()
{
    renderer->render();
}

WorldRendererPtr World::getRenderer() const
{
    return renderer;
}

void World::setRenderer(const WorldRendererPtr renderer)
{
    this->renderer = renderer;
    renderer->setWorld(shared_from_this());
}

HumanPtr World::getHuman() const
{
    return human;
}

void World::setHuman(const HumanPtr human)
{
    this->human = human;
}
ArmarPtr World::getArmar() const
{
    return armar;
}

void World::setArmar(const ArmarPtr armar)
{
    this->armar = armar;
}
ManipulationObjectPtr World::getEnvironment() const
{
    return environment;
}

void World::setEnvironment(const ManipulationObjectPtr environment)
{
    this->environment = environment;
}


Eigen::Matrix4f World::getHandoverPose() const
{
    return handoverPose;
}

void World::setHandoverPose(const Eigen::Matrix4f globalPose)
{
    handoverPose = globalPose;
}






