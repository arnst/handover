#ifndef ARMAR_H
#define ARMAR_H

#include "actor.h"
#include "human.h"

#include <VirtualRobot/ManipulationObject.h>
#include <VirtualRobot/Grasping/Grasp.h>

using namespace VirtualRobot;

class Armar : public Actor
{
public:
    Armar(SoSeparator* sceneSep);
    ~Armar();
    virtual void loadRobot(QString fileName);
    void resetHandoverSpace();
    void enableVisualizationHS(bool enable);
    WorkspaceRepresentationPtr getHandoverSpace();
    void setHandoverSpace(WorkspaceRepresentationPtr hs);
    bool isEnabledVisualizationHS();
    bool isEnabledVisualizationObject();
    void equip(Actor::TCP side, ManipulationObjectPtr object);
    void equip(ManipulationObjectPtr object, GraspPtr grasp);
    void setGrasp(GraspPtr grasp);
    void unEquip();
    virtual std::string getEEF(Actor::TCP side);
    virtual Actor::TCP getTcpSide(std::string eef);
    virtual void visualize();
    virtual void visualize(SoNode *wsLeft, SoNode *wsRight, SoNode *hs, SoNode *obj);
    virtual QString getName();
    virtual void transformRobot(float x, float y, float z, float rotX, float rotY, float rotZ);
    virtual void resetRobotConfig();

    /** @return the object, armar is currently holding. May be NULL.*/
    ManipulationObjectPtr getObject() const;
    GraspPtr getGrasp();
    Actor::TCP getObjectSide();

    bool cacheHSPose(Eigen::Matrix4f &pose);
    Eigen::Matrix4f getRandomCachedHSPose();
    bool hasCachedHSPose();
    int getCurrentCacheSize();
    bool clearCache();

    virtual void enableVisualization(bool enable);

private:
    ManipulationObjectPtr object;
    Actor::TCP objectSide;
    GraspPtr grasp; // grasp used for the current object
    WorkspaceRepresentationPtr handoverSpace;

    SoSeparator* handoverSpaceSep;
    SoSeparator* objectSep;

    bool visualizeHS; // whether to visualize handover-space
    bool visualizeObject;

    QList< Eigen::Matrix4f > cachedHSPoses;
    int maxCacheSize;
};

typedef boost::shared_ptr<Armar> ArmarPtr;

#endif // ARMAR_H
