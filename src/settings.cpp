#include "settings.h"

// workspace
Settings::Workspace::Measure Settings::Workspace::measureHuman = Settings::Workspace::COMFORT;
Settings::Workspace::Measure Settings::Workspace::measureArmar = Settings::Workspace::EXT_MANIPULABILITY;
unsigned int Settings::Workspace::samplesHuman = 1000;
unsigned int Settings::Workspace::samplesArmar = 1000;
unsigned int Settings::Workspace::discrStepTrans = 100;
float Settings::Workspace::discrStepRot = 1.2f;
unsigned int Settings::Workspace::smooth = 2;
unsigned int Settings::Workspace::samplesExtend = 100000;

// handoverspace
unsigned int Settings::Handoverspace::discrStepTrans = 100;
float Settings::Handoverspace::discrStepRot = 1.2f;
