#include "posequalityrosenbaum.h"

PoseQualityRosenbaum::PoseQualityRosenbaum(RobotNodeSetPtr rns) : PoseQualityMeasurement(rns)
{
    refJointValues = rns->getJointValues();
    refTcpPose = rns->getTCP()->getGlobalPose();
    maxTravelCost = getMaxTravelCost();
    maxSafetyCost = 400;
}

float PoseQualityRosenbaum::getPoseQuality()
{
    float c_spatial = 1 - (getSpatialErrorCost() / 1000);
    c_spatial = (c_spatial < 0) ? 0 : c_spatial;
    c_spatial = pow(c_spatial, 2);

    float c_travel = 1 - (getTravelCost() / maxTravelCost);
    c_travel = (c_travel < 0) ? 0 : c_travel;

    float c_safety = 1 - (getSafetyCost() / maxSafetyCost);
    c_safety = (c_safety < 0) ? 0 : c_safety;
    c_safety *= c_safety;

    float poseQuality = c_spatial * c_travel * c_safety;

    return poseQuality;
}

float PoseQualityRosenbaum::getSpatialErrorCost()
{
    float spatialErrorCost = 0;

    Eigen::Vector3f currentTcpPosition = rns->getTCP()->getGlobalPose().block<3,1>(0,3);
    Eigen::Vector3f refTcpPosition = refTcpPose.block<3,1>(0,3);

    spatialErrorCost = (currentTcpPosition - refTcpPosition).norm();

    return spatialErrorCost;
}

float PoseQualityRosenbaum::getTravelCost()
{
    float travelCost = 0;

    for (int i = 0; i < refJointValues.size(); i++)
    {
        travelCost += fabs(refJointValues[i] - (*rns)[i]->getJointValue());
    }

    return travelCost;
}

float PoseQualityRosenbaum::getMaxTravelCost()
{
    float maxTravelCost = 0;

    for (int i = 0; i < rns->getSize(); i++)
    {
        maxTravelCost += std::abs(rns->getNode(i)->getJointLimitHi() - rns->getNode(i)->getJointLimitLo());
    }

    return maxTravelCost;
}

float PoseQualityRosenbaum::getSafetyCost()
{
    float c_safety = maxSafetyCost - obstacleDir.norm();
    c_safety = (c_safety < 0) ? 0 : c_safety;

    return c_safety;
}
