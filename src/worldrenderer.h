#ifndef WORLDRENDERER_H
#define WORLDRENDERER_H

#include "human.h"
#include "armar.h"

#include <boost/weak_ptr.hpp>

#include <Inventor/nodes/SoSeparator.h>

#include <QtCore>

class World;

class WorldRenderer : public QObject
{
    Q_OBJECT

public:
    WorldRenderer(SoSeparator *sceneSep);
    void setWorld(boost::weak_ptr<World> world);

private:
    boost::weak_ptr<World> world;

    SoSeparator* sceneSep;
    SoSeparator* environmentSep;
    SoSeparator* handoverPoseSep;
    SoSeparator* ladderSep;

    float wsHumanHeightLeft;
    float wsHumanHeightRight;
    float wsArmarHeightLeft;
    float wsArmarHeightRight;
    float hsHeight;

    void initSceneGraph();
    boost::shared_ptr<World> getWorld();

public slots:
    void render();
    void renderArmar();
    void renderHuman();
    void renderHandoverPose();

private slots:
    void handleResultVisualizationTask(HumanPtr human, SoNode* wsLeft, SoNode* wsRight);
    void handleResultVisualizationTask(ArmarPtr human, SoNode* wsLeft, SoNode* wsRight, SoNode* hs, SoNode* object);
};

typedef boost::shared_ptr<WorldRenderer> WorldRendererPtr;

#endif // WORLDRENDERER_H
