#include <iostream>

#include <../src/ui/mainwindow.h>

#include <VirtualRobot/RuntimeEnvironment.h>
#include <Inventor/Qt/SoQt.h>

using namespace VirtualRobot;

int main(int argc, char* argv[])
{
    SoDB::init();
    SoQt::init(argc, argv, "Viewer"); // initialize Open Inventor stuff and Qt e.g. QApplication

    RuntimeEnvironment::addDataPath("/home/harry/workspace/handover/data/");

    MainWindow *mainWindow = new MainWindow();
//    SoQt::show(mainWindow);
    mainWindow->showMaximized();

//    QRect geometry = mainWindow->geometry();
//    geometry.setHeight(900);
//    geometry.setWidth(1600);
//    mainWindow->setGeometry(geometry);
    SoQt::mainLoop(); // same as QApplication::exec();

    delete mainWindow;
    return 0;
}

