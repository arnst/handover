#ifndef FACTORY_H
#define FACTORY_H

#include "taskcreatemanipulability.h"

#include <QtCore>

using namespace VirtualRobot;

class TaskExecutor : public QThread
{
    Q_OBJECT

public:
    TaskExecutor(unsigned int lvl = 0, QObject *parent = 0);
    void addTask(TaskPtr task);
    void newThread();
    virtual void run();

private:
    static boost::mutex mutexPrint;
    QList<TaskPtr> tasks;
    QList<TaskExecutor*> subFactories;
    QList<float> taskDurations; // seconds
    float totalDuration; // seconds
    unsigned int lvl;

    void printTasks();
    void summarize();
};

#endif // MANIPULABILITYCONSTRUCTION_H
