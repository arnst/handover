#ifndef HANDOVERSPACEWORKER_H
#define HANDOVERSPACEWORKER_H

#include "task.h"
#include "../armar.h"
#include "../human.h"
#include "../posequalityrosenbaum.h"

#include <QtCore>

#include <Eigen/Dense>

/**
 * Creates the handover-space between the specified Actors and fills it with data.
 */
class TaskCreateHandoverspace : public Task
{
public:
    TaskCreateHandoverspace(ArmarPtr armar, Actor::TCP armarTcpSide, HumanPtr human, int graspCombinations = 16);
    virtual void work();
    virtual std::string getDescription();
    virtual void setConcurrent(QPair<float,float> interval, boost::shared_ptr<boost::mutex> mutex, RobotPtr clonedRobot);
    void cloneHuman();

private:
    struct Sphere {
        Eigen::Vector3f pos;
        float r;
    };

    ArmarPtr armar;
    Actor::TCP armarTcpSide;
    HumanPtr human;
    RobotPtr robotHuman;
    const unsigned int graspCombinations;
    boost::shared_ptr<boost::mutex> mutex;
    QPair<float,float> interval;
    int concurrencyID;
    bool isConcurrent;
    unsigned int discrStepTrans;
    float discrStepRot;
    bool trackGazeDirection;

    int numIKSolving;
    float numIKSolvingSuccess;
    int voxelFillCount;
    std::vector<float> jointValuesLeft;
    std::vector<float> jointValuesRight;

    PoseQualityRosenbaumPtr measureHumanLeft;
    PoseQualityRosenbaumPtr measureHumanRight;

    void createHandoverSpace();
    Eigen::Matrix4f sampleHumanGraspingPose(Eigen::Matrix4f& armarGraspingPose, ManipulationObjectPtr object, Actor::TCP humanTcpSide);
    Sphere getSphere(ManipulationObjectPtr object);
    Sphere getSphere(WorkspaceRepresentationPtr ws);
    float getVisibility(Eigen::Vector3f globalPos);
    bool solveIK(RobotNodeSetPtr rns, Eigen::Matrix4f targetPose);
    Eigen::Vector3f getObstacleDistanceVector(Actor::TCP side);
};

typedef boost::shared_ptr<TaskCreateHandoverspace> TaskCreateHandoverspacePtr;

#endif // HANDOVERSPACEWORKER_H
