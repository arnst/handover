#ifndef LOADWORKSPACEWORKER_H
#define LOADWORKSPACEWORKER_H

#include "task.h"
#include "../actor.h"

/**
 * Loads workspace data and assigns it to the specified Actor.
 */
class TaskLoadWorkspace : public Task
{
public:
    TaskLoadWorkspace(ActorPtr actor, Actor::TCP side, QString fileName);
    virtual void work();
    virtual std::string getDescription();

private:
    ActorPtr actor;
    Actor::TCP side;
    QString fileName;

    void loadWorkspace();
};

typedef boost::shared_ptr<TaskLoadWorkspace> TaskLoadWorkspacePtr;

#endif // LOADWORKSPACEWORKER_H
