#ifndef EXTENDWORKSPACEWORKER_H
#define EXTENDWORKSPACEWORKER_H

#include "task.h"
#include "../actor.h"

using namespace VirtualRobot;

class TaskExtendWorkspace : public Task
{
public:
    TaskExtendWorkspace(ActorPtr actor, Actor::TCP tcpSide, unsigned int samples, bool checkForSelfCollisions = true);
    virtual void work();
    virtual std::string getDescription();

private:
    ActorPtr actor;
    Actor::TCP tcpSide;
    unsigned int samples;
    bool checkForSelfCollisions;

    void extendWorkspace();
};

typedef boost::shared_ptr<TaskExtendWorkspace> TaskExtendWorkspacePtr;

#endif // EXTENDWORKSPACEWORKER_H
