#include "tasksolvehandoverik.h"

#include "../handoveriksolver.h"
#include "../settings.h"

#include <VirtualRobot/Workspace/WorkspaceRepresentation.h>
#include <VirtualRobot/IK/GenericIKSolver.h>

TaskSolveHandoverIK::TaskSolveHandoverIK(ArmarPtr armar, HumanPtr human, unsigned int graspSamples)
{
    this->armar = armar;
    this->human = human;
    this->graspSamples = graspSamples;
    this->maxErrorPositionMM = 30;
    this->maxErrorOrientationRad = 0.2f;
    this->jacobianStepSize = 0.2;
    this->jacobianMaxLoop = 50;
    this->ikRandomSeeds = 16;
    this->bestHandoverPose = Eigen::Matrix4f::Identity();

    srand(time(NULL)); // to randomize grasps
}


void TaskSolveHandoverIK::work()
{
    solveIK();
}

std::string TaskSolveHandoverIK::getDescription()
{
    return "Solve Handover IK";
}

Eigen::Matrix4f TaskSolveHandoverIK::getBestHandoverPose()
{
    return bestHandoverPose;
}

void TaskSolveHandoverIK::solveIK()
{
    if (!armar->getHandoverSpace()->getData()) {
        VR_ERROR << "Handover-space not created yet." << endl;
        return;
    }

    for (int i = 0; i < 32; i++)
    {
        findBestHandoverPose();

        if (solveIKArmar()) {
            break;
        }
        armar->resetRobotConfig();
        ignoreList.push_back(bestHandoverPose);
    }
//    solveIKHuman();
}

/****************************************************** armar ********************************************************************/

bool TaskSolveHandoverIK::solveIKArmar()
{
    bool success = false;
    WorkspaceRepresentationPtr hs = armar->getHandoverSpace();

    // if armar has no fixed grasp assigned to him previously,
    // then we just sample random grasps and use the grasp with the best result
    if (!armar->getGrasp())
    {
        GraspPtr bestGraspArmar;
        unsigned char maxEntry = 0;
        for (int i = 0; i < graspSamples; i++)
        {
            GraspPtr graspArmar = getRandomGrasp(armar, armar->getObject(), armar->getObjectSide());
            Eigen::Matrix4f objectPose = graspArmar->getObjectTargetPoseGlobal(bestHandoverPose);

            GraspPtr graspHumanLeft = getRandomGrasp(human, armar->getObject(), Actor::LEFT_TCP);
            GraspPtr graspHumanRight = getRandomGrasp(human, armar->getObject(), Actor::RIGHT_TCP);

            Eigen::Matrix4f tcpPoseHumanLeft = graspHumanLeft->getTcpPoseGlobal(objectPose);
            Eigen::Matrix4f tcpPoseHumanRight = graspHumanRight->getTcpPoseGlobal(objectPose);

            unsigned char entryLeft = human->getManipulability(Actor::LEFT_TCP)->getEntry(tcpPoseHumanLeft);
            entryLeft *= getVisibility(tcpPoseHumanLeft.block<3,1>(0,3));
            unsigned char entryRight = human->getManipulability(Actor::RIGHT_TCP)->getEntry(tcpPoseHumanRight);
            entryRight *= getVisibility(tcpPoseHumanRight.block<3,1>(0,3));
            unsigned char entry = (entryLeft > entryRight) ? entryLeft : entryRight;

            if (entry > maxEntry) {
                maxEntry = entry;
                bestGraspArmar = graspArmar;
            }
        }
        armar->setGrasp(bestGraspArmar);
    }

    HandoverIKSolverPtr ikSolver(new HandoverIKSolver(hs->getNodeSet(), JacobiProvider::eTranspose));

    if ((ikSolver->getRobotNodeSet()->getName() == "TorsoLeftArm") || (ikSolver->getRobotNodeSet()->getName() == "TorsoRightArm"))
    {
        unsigned int nDoF = hs->getNodeSet()->getSize();
        Eigen::VectorXf jointWeights(nDoF);
        for (int i = 0; i < nDoF; i++) {
            jointWeights(i) = 1;
        }
        jointWeights(0) = 0.1f; // hip pitch
        jointWeights(1) = 0.05f; // hip roll
        jointWeights(1) = 0.1f; // hip jaw
        ikSolver->setJointWeights(jointWeights);
    }

    ikSolver->setVerbose(true);
    ikSolver->setupJacobian(jacobianStepSize, jacobianMaxLoop);
    ikSolver->setMaximumError(maxErrorPositionMM, maxErrorOrientationRad);

    success = ikSolver->solve(bestHandoverPose, IKSolver::All, ikRandomSeeds);
    if (success) {
        cout << "Solving IK for Armar: OK" << endl;
    } else {
        cout << "Solving IK for Armar: FAILED" << endl;
    }

    cout << "Best Handover Pose: " << endl << bestHandoverPose << endl << endl;
    cout << "Armar TCP Pose: " << endl << ikSolver->getRobotNodeSet()->getTCP()->getGlobalPose() << endl << endl;
    Eigen::Vector3f dist = bestHandoverPose.block<3,1>(0,3) - ikSolver->getRobotNodeSet()->getTCP()->getGlobalPose().block<3,1>(0,3);
    cout << "ARMAR DISTANCE NORM: " << dist.norm() << endl << endl;

    return success;
}

/****************************************************** human ********************************************************************/

void TaskSolveHandoverIK::solveIKHuman()
{
    Eigen::Matrix4f objectPose = armar->getGrasp()->getTargetPoseGlobal(armar->getRobot());
//    Eigen::Matrix4f objectPose = armar->getGrasp()->getObjectTargetPoseGlobal(bestHandoverPose);

    unsigned char maxEntry = 0;
    Eigen::Matrix4f bestPose = Eigen::Matrix4f::Identity();
    Actor::TCP bestSide;
    // we try some grasps with the humans left eef and store the best entry we can find
    for (int i = 0; i < graspSamples; i++)
    {
        GraspPtr graspHuman = getRandomGrasp(human, armar->getObject(), Actor::LEFT_TCP);
        Eigen::Matrix4f tcpPose = graspHuman->getTcpPoseGlobal(objectPose);
        unsigned char entryLeft = human->getManipulability(Actor::LEFT_TCP)->getEntry(tcpPose);

        if (maxEntry < entryLeft) {
            maxEntry = entryLeft;
            bestPose = tcpPose;
            bestSide = Actor::LEFT_TCP;
        }
    }
    // same with the right eef
    for (int i = 0; i < graspSamples; i++)
    {
        GraspPtr graspHuman = getRandomGrasp(human, armar->getObject(), Actor::RIGHT_TCP);
        Eigen::Matrix4f tcpPose = graspHuman->getTcpPoseGlobal(objectPose);
        unsigned char entryRight = human->getManipulability(Actor::RIGHT_TCP)->getEntry(tcpPose);

        if (maxEntry < entryRight) {
            maxEntry = entryRight;
            bestPose = tcpPose;
            bestSide = Actor::RIGHT_TCP;
        }
    }

    HandoverIKSolverPtr ikSolver(new HandoverIKSolver(human->getManipulability(bestSide)->getNodeSet(), JacobiProvider::eTranspose));

    if ((ikSolver->getRobotNodeSet()->getName() == "TorsoLeftArm") || (ikSolver->getRobotNodeSet()->getName() == "TorsoRightArm"))
    {
        unsigned int nDoF = human->getManipulability(bestSide)->getNodeSet()->getSize();
        Eigen::VectorXf jointWeights(nDoF);
        for (int i = 0; i < nDoF; i++) {
            jointWeights(i) = 1;
        }
        jointWeights(0) = 0.1f; // hip pitch
        jointWeights(1) = 0.1f; // hip jaw
        jointWeights(1) = 0.05f; // hip roll
        ikSolver->setJointWeights(jointWeights);
    }

    ikSolver->setupJacobian(jacobianStepSize, jacobianMaxLoop);
    ikSolver->setMaximumError(maxErrorPositionMM, maxErrorOrientationRad);
    if (ikSolver->solve(bestPose, IKSolver::All, ikRandomSeeds)) {
        cout << "Solving IK for Human: OK" << endl;
    } else {
        cout << "Solving IK for Human: FAILED" << endl;
    }

    cout << "Best Handover Pose: " << endl << bestPose << endl << endl;
    cout << "Human TCP Pose: " << endl << ikSolver->getRobotNodeSet()->getTCP()->getGlobalPose() << endl << endl;
    Eigen::Vector3f dist = bestPose.block<3,1>(0,3) - ikSolver->getRobotNodeSet()->getTCP()->getGlobalPose().block<3,1>(0,3);
    cout << "HUMAN DISTANCE NORM: " << dist.norm() << endl << endl;
}

GraspPtr TaskSolveHandoverIK::getRandomGrasp(ActorPtr actor, ManipulationObjectPtr object, Actor::TCP side)
{
    GraspSetPtr graspSet = object->getGraspSet(actor->getRobot()->getType(), actor->getEEF(side));
    return graspSet->getGrasp(rand() % graspSet->getSize());
}

float TaskSolveHandoverIK::getVisibility(Eigen::Vector3f globalPos)
{
    float visibility = 0;

    Eigen::Vector3f faceToVoxel = globalPos - human->getGazeOriginGlobal();
    float cosAlpha = (faceToVoxel.dot(human->getGazeDirection())) / (faceToVoxel.norm() * human->getGazeDirection().norm());
    visibility = std::max(cosAlpha, 0.0f);

    return visibility;
}

void TaskSolveHandoverIK::findBestHandoverPose()
{
    WorkspaceRepresentationPtr hs = armar->getHandoverSpace();
    unsigned char maxEntry = 0;
    Eigen::Matrix4f bestPose = Eigen::Matrix4f::Identity();
    // find a good pose in armars handover-space
    int maxLoop = (hs->getData()->getVoxelFilledCount() > 10000) ? 10000 : hs->getData()->getVoxelFilledCount();
    for (int i = 0; i < maxLoop; i++)
    {
        Eigen::Matrix4f pose;
        if ((hs->getData()->getVoxelFilledCount() < 500) && armar->hasCachedHSPose()) {
            VR_INFO << "Using cache to retrieve random handover-poses" << endl;
            pose = armar->getRandomCachedHSPose();
        }
        else {
            pose = hs->sampleCoveredPose();
        }

        if (isIgnored(pose)) {
            continue;
        }

        unsigned char entry = hs->getEntry(pose);

        if (entry > maxEntry) {
            maxEntry = entry;
            bestPose = pose;
        }
    }
    bestHandoverPose = bestPose;
}

bool TaskSolveHandoverIK::isIgnored(Eigen::Matrix4f handoverPose)
{
    BOOST_FOREACH(Eigen::Matrix4f pose, ignoreList)
    {
        if (pose == handoverPose)
            return true;
    }
    return false;
}


