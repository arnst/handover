#include "taskvisualizearmar.h"

#include "../actorvisualizationfactory.h"

#include <Inventor/So.h>

TaskVisualizeArmar::TaskVisualizeArmar(ArmarPtr armar, float cutZWsLeft, float cutZWsRight, float cutZHs)
{
    this->armar = armar;
    this->cutZWsLeft = cutZWsLeft;
    this->cutZWsRight = cutZWsRight;
    this->cutZHs = cutZHs;
}

void TaskVisualizeArmar::work()
{
    buildVisualization();
}

void TaskVisualizeArmar::buildVisualization()
{
    SoSeparator* wsLeft = new SoSeparator();
    SoSeparator* wsRight = new SoSeparator();
    SoSeparator* hs = new SoSeparator();
    SoSeparator* objectSep = new SoSeparator();

    wsLeft->ref();
    wsRight->ref();
    hs->ref();
    objectSep->ref();

    // visualize workspace
    if (armar->isEnabledVisualizationWS())
    {
        if (armar->getManipLeft()->getData()) {
            wsLeft->addChild(ActorVisualizationFactory::getWorkspaceVisualization(armar->getManipLeft(), cutZWsLeft, ColorMap(ColorMap::eHot)));
        }
        if (armar->getManipRight()->getData()) {
            wsRight->addChild(ActorVisualizationFactory::getWorkspaceVisualization(armar->getManipRight(), cutZWsRight, ColorMap(ColorMap::eHot)));
        }
    }
    // visualize handover-space
    if (armar->isEnabledVisualizationHS())
    {
        if (armar->getHandoverSpace()->getData()) {
            hs->addChild(ActorVisualizationFactory::getWorkspaceVisualization(armar->getHandoverSpace(), cutZHs, ColorMap(ColorMap::eHot), true, ActorVisualizationFactory::MAX));
        }
    }
    // visualize object
    if (armar->isEnabledVisualizationObject() && armar->getObject() && armar->getGrasp())
    {
        armar->getObject()->setGlobalPose(armar->getGrasp()->getTargetPoseGlobal(armar->getRobot()));
        objectSep->addChild(CoinVisualizationFactory::getCoinVisualization(armar->getObject(), SceneObject::Full));
    }

    wsLeft->unrefNoDelete();
    wsRight->unrefNoDelete();
    hs->unrefNoDelete();
    objectSep->unrefNoDelete();

    emit resultReady(armar, wsLeft, wsRight, hs, objectSep);
}

std::string TaskVisualizeArmar::getDescription()
{
    return "Visualize Armar";
}

void TaskVisualizeArmar::setCutZ(float wsLeft, float wsRight, float hs)
{
    this->cutZWsLeft = wsLeft;
    this->cutZWsRight = wsRight;
    this->cutZHs = hs;
}
