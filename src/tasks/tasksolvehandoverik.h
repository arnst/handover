#ifndef HANDOVERIKWORKER_H
#define HANDOVERIKWORKER_H

#include "task.h"
#include "../armar.h"
#include "../human.h"

using namespace VirtualRobot;

class TaskSolveHandoverIK : public Task
{
public:
    TaskSolveHandoverIK(ArmarPtr armar, HumanPtr human, unsigned int graspSamples = 32);
    virtual void work();
    virtual std::string getDescription();
    Eigen::Matrix4f getBestHandoverPose();

private:
    ArmarPtr armar;
    HumanPtr human;
    unsigned int graspSamples;
    float maxErrorPositionMM;
    float maxErrorOrientationRad;
    float jacobianStepSize;
    int jacobianMaxLoop;
    int ikRandomSeeds;

    Eigen::Matrix4f bestHandoverPose;
    std::vector<Eigen::Matrix4f> ignoreList;

    void solveIK();
    bool solveIKArmar();
    void solveIKHuman();
    GraspPtr getRandomGrasp(ActorPtr actor, ManipulationObjectPtr object, Actor::TCP side);
    float getVisibility(Eigen::Vector3f globalPos);
    void findBestHandoverPose();
    bool isIgnored(Eigen::Matrix4f handoverPose);
};

typedef boost::shared_ptr<TaskSolveHandoverIK> TaskSolveHandoverIKPtr;

#endif // HANDOVERIKWORKER_H
