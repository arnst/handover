#include "taskcreatemanipulability.h"

#include <VirtualRobot/IK/PoseQualityExtendedManipulability.h>
#include "../actor.h"

TaskCreateManipulability::TaskCreateManipulability(boost::shared_ptr<Actor> actor, Actor::TCP side)
{
    this->actor = actor;
    this->side = side;
}

void TaskCreateManipulability::setParams(PoseQualityMeasurementPtr measure,
                                     RobotNodeSetPtr rns,
                                     float discrStepTrans,
                                     float discrStepRot,
                                     int samples,
                                     RobotNodeSetPtr staticCollisionModel,
                                     RobotNodeSetPtr dynamicCollisionModel,
                                     RobotNodePtr baseNode,
                                     RobotNodePtr tcpNode,
                                     bool adjustOnOverflow)
{
    this->measure = measure;
    this->rns = rns;
    this->discrStepTrans = discrStepTrans;
    this->discrStepRot = discrStepRot;
    this->samples = samples;
    this->staticCollisionModel = staticCollisionModel;
    this->dynamicCollisionModel = dynamicCollisionModel;
    this->baseNode = baseNode;
    this->tcpNode = tcpNode;
    this->adjustOnOverflow = adjustOnOverflow;
    this->paramsInitialized = true;
}

void TaskCreateManipulability::work()
{
    createManipulability();
}

std::string TaskCreateManipulability::getDescription()
{
    return "Create Manipulability (" + actor->getName().toStdString() + "-" + ((side == Actor::LEFT_TCP) ? "left" : "right") + ")";
}

void TaskCreateManipulability::createManipulability()
{
    if (side == Actor::BOTH_TCP) {
        VR_WARNING << "BOTH_TCP not allowed here." << endl;
        return;
    }

    if (!paramsInitialized) {
        VR_WARNING << "Params not set." << endl;
        return;
    }

    float minB[6];
    float maxB[6];
    float maxManip;
    ManipulabilityPtr manip = actor->getManipulability(side);
    manip->reset();
    manip->setManipulabilityMeasure(measure);

    manip->initSelfDistanceCheck(staticCollisionModel, dynamicCollisionModel);
    manip->checkForParameters(rns,2000,minB,maxB,maxManip,baseNode,tcpNode);
    manip->initialize(rns,discrStepTrans,discrStepRot,minB,maxB, staticCollisionModel,dynamicCollisionModel, baseNode, tcpNode, adjustOnOverflow);
    // Manipulabiliy::initialize(...) does not set a correct maxManipulability if we use extended Manipulability with distance checking.
    // Therefore we just set the maxManipulabilty by hand to 1.
    manip->setMaxManipulability(1);
    manip->addRandomTCPPoses(samples);
//    manip->print();
}


