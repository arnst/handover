#include "taskvisualizehuman.h"

#include "../actorvisualizationfactory.h"

#include <VirtualRobot/Visualization/CoinVisualization/CoinVisualizationFactory.h>

TaskVisualizeHuman::TaskVisualizeHuman(HumanPtr human, float cutZWsLeft, float cutZWsRight)
{
    this->human = human;
    this->cutZWsLeft = cutZWsLeft;
    this->cutZWsRight = cutZWsRight;
}

void TaskVisualizeHuman::work()
{
    buildVisualization();
}

void TaskVisualizeHuman::buildVisualization()
{
    SoSeparator* wsLeft = new SoSeparator();
    SoSeparator* wsRight = new SoSeparator();
    wsLeft->ref();
    wsRight->ref();

    // visualize workspace
    if (human->isEnabledVisualizationWS())
    {
        if (human->getManipLeft()->getData()) {
            wsLeft->addChild(ActorVisualizationFactory::getWorkspaceVisualization(human->getManipLeft(), cutZWsLeft, ColorMap(ColorMap::eHot)));
        }
        if (human->getManipRight()->getData()) {
            wsRight->addChild(ActorVisualizationFactory::getWorkspaceVisualization(human->getManipRight(), cutZWsRight, ColorMap(ColorMap::eHot)));
        }
    }

    wsLeft->unrefNoDelete();
    wsRight->unrefNoDelete();

    emit resultReady(human, wsLeft, wsRight);
}

std::string TaskVisualizeHuman::getDescription()
{
    return "Visualize Human";
}

void TaskVisualizeHuman::setCutZ(float wsLeft, float wsRight)
{
    this->cutZWsLeft = wsLeft;
    this->cutZWsRight = wsRight;
}
