#ifndef SMOOTHWORKSPACEWORKER_H
#define SMOOTHWORKSPACEWORKER_H

#include "task.h"
#include "../actor.h"

/**
 * Smooths the manipulability data of the specified Actor.
 */
class TaskSmoothWorkspace : public Task
{
public:
    TaskSmoothWorkspace(ActorPtr actor, unsigned int minNeighbors);
    void work();
    virtual std::string getDescription();

private:
    ActorPtr actor;
    unsigned int minNeighbors;

    void smooth();
};

typedef boost::shared_ptr<TaskSmoothWorkspace> TaskSmoothWorkspacePtr;

#endif // SMOOTHWORKSPACEWORKER_H
