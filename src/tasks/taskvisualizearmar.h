#ifndef ARMARVISUALIZATIONWORKER_H
#define ARMARVISUALIZATIONWORKER_H

#include "task.h"
#include "../armar.h"

#include <VirtualRobot/Visualization/CoinVisualization/CoinVisualizationFactory.h>

#include <QObject>

/**
 * Visualizes the specified Armar instance with all its workspaces, objects, handover-space, etc.
 */
class TaskVisualizeArmar : public QObject, public Task
{
    Q_OBJECT

public:
    /**
     * @param armar
     * @param cutZWsLeft  indicates how much of the workspace attached to the left arm should be drawn, e.g. provide 0.5f to draw half of the workspace.
     * @param cutZWsRight same goes for the right arm
     * @param cutZHs and same goes for the handover-space
     */
    TaskVisualizeArmar(ArmarPtr armar, float cutZWsLeft = 1, float cutZWsRight = 1, float cutZHs = 1);
    virtual void work();
    virtual std::string getDescription();

    void setCutZ(float wsLeft, float wsRight, float hs);

signals:
    void resultReady(ArmarPtr armar, SoNode* wsLeft, SoNode* wsRight, SoNode* hs, SoNode* object);

private:
    ArmarPtr armar;
    float cutZWsLeft;
    float cutZWsRight;
    float cutZHs;

    void buildVisualization();
};

#endif // ARMARVISUALIZATIONWORKER_H
