#include "taskexecutor.h"

boost::mutex TaskExecutor::mutexPrint;

TaskExecutor::TaskExecutor(unsigned int lvl, QObject *parent) : QThread(parent)
{
    this->lvl = lvl;
}

void TaskExecutor::addTask(TaskPtr task)
{
    if (subFactories.isEmpty())
    {
        tasks.append(task);
    }
    else
    {
        subFactories.last()->addTask(task);
    }
}

void TaskExecutor::newThread()
{
    subFactories.append(new TaskExecutor(subFactories.size() + 1, this));
}

void TaskExecutor::run()
{
    QElapsedTimer timerRun;
    timerRun.start();

    BOOST_FOREACH(TaskExecutor *subFactory, subFactories)
    {
        subFactory->start();
    }

    BOOST_FOREACH(TaskPtr task, tasks)
    {
        QElapsedTimer timerTask;
        timerTask.start();
        task->work();
        taskDurations.append(timerTask.elapsed() / 1000.0f);
    }

    printTasks();

    BOOST_FOREACH(TaskExecutor *subFactory, subFactories)
    {
        subFactory->wait();
    }

    totalDuration = timerRun.elapsed() / 1000.0f;
    summarize();
}

void TaskExecutor::printTasks()
{
    mutexPrint.lock();
    for (int i = 0; i < tasks.size(); i++)
    {
        cout << "Task: " << tasks[i]->getDescription() << "\t[Duration: " << taskDurations[i] << " s]\t[Thread: " << lvl << "]" << endl;
    }
    mutexPrint.unlock();
}

void TaskExecutor::summarize()
{
    if (lvl == 0) {
        cout << "Total Duration: " << totalDuration << " s" << endl;
        cout << "-----------------------------------------------------------------------------" << endl;
    }
}

