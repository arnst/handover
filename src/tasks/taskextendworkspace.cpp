#include "taskextendworkspace.h"

TaskExtendWorkspace::TaskExtendWorkspace(ActorPtr actor, Actor::TCP tcpSide, unsigned int samples, bool checkForSelfCollisions)
{
    this->actor = actor;
    this->tcpSide = tcpSide;
    this->samples = samples;
    this->checkForSelfCollisions = checkForSelfCollisions;

    if ((tcpSide != Actor::LEFT_TCP) && (tcpSide != Actor::RIGHT_TCP)) {
        VR_ERROR << "Invalid TCP-Side!" << endl;
    }
}

void TaskExtendWorkspace::work()
{
    extendWorkspace();
}

void TaskExtendWorkspace::extendWorkspace()
{
    if (!actor->getManipulability(tcpSide)->getData()) {
        VR_ERROR << getDescription() << " No manipulability data!" << endl;
        return;
    }
    actor->getManipulability(tcpSide)->addRandomTCPPoses(samples, checkForSelfCollisions);
}

std::string TaskExtendWorkspace::getDescription()
{
    return "Extend Workspace (" + actor->getName().toStdString() + "-" + ((tcpSide == Actor::LEFT_TCP) ? "left" : "right") + ")";
}

