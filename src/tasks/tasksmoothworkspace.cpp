#include "tasksmoothworkspace.h"

TaskSmoothWorkspace::TaskSmoothWorkspace(ActorPtr actor, unsigned int minNeighbors)
{
    this->actor = actor;
    this->minNeighbors = minNeighbors;
}

void TaskSmoothWorkspace::work()
{
    smooth();
}

std::string TaskSmoothWorkspace::getDescription()
{
    return "Smooth Workspace (" + actor->getName().toStdString() + ")";
}

void TaskSmoothWorkspace::smooth()
{
    actor->smoothWorkspace(minNeighbors);
}
