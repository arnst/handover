#ifndef MANIPULABILITYWORKER_H
#define MANIPULABILITYWORKER_H

#include "task.h"
#include "../actor.h"

#include <VirtualRobot/SceneObjectSet.h>
#include <VirtualRobot/Nodes/RobotNode.h>
#include <VirtualRobot/RobotNodeSet.h>
#include <VirtualRobot/IK/PoseQualityMeasurement.h>

using namespace VirtualRobot;

class Actor;

/**
 * Creates a Manipulability instance for the specified Actor and fills it with data.
 */
class TaskCreateManipulability : public Task
{
public:
    TaskCreateManipulability(boost::shared_ptr<Actor> actor, Actor::TCP side);

    /**
     * @param measure
     * @param rns
     * @param discrStepTrans translational discretization in [mm].
     * @param discrStepRot rotational discretization in [rad].
     * @param samples number of random tcp poses to be sampled.
     * @param staticCollisionModel actually we need SceneobjectSetPtr, but Manipulability::initSelfDistanceCheck(...) kinda wants RobotNodeSetptr.
     * @param dynamicCollisionModel same here.
     * @param baseNode
     * @param tcpNode
     * @param adjustOnOverflow
     */
    void setParams(PoseQualityMeasurementPtr measure,
                   RobotNodeSetPtr rns,
                   float discrStepTrans,
                   float discrStepRot,
                   int samples = 1000,
                   RobotNodeSetPtr staticCollisionModel = RobotNodeSetPtr(),
                   RobotNodeSetPtr dynamicCollisionModel = RobotNodeSetPtr(),
                   RobotNodePtr baseNode = RobotNodePtr(),
                   RobotNodePtr tcpNode = RobotNodePtr(),
                   bool adjustOnOverflow = true);

    virtual void work();
    virtual std::string getDescription();

private:
    boost::shared_ptr<Actor> actor;
    PoseQualityMeasurementPtr measure;
    Actor::TCP side;

    bool paramsInitialized;
    RobotNodeSetPtr rns;
    float discrStepTrans;
    float discrStepRot;
    int samples;
    RobotNodeSetPtr staticCollisionModel;
    RobotNodeSetPtr dynamicCollisionModel;
    RobotNodePtr baseNode;
    RobotNodePtr tcpNode;
    bool adjustOnOverflow;

    void createManipulability();
};

#endif // MANIPULABILITYWORKER_H
