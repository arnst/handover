#include "taskloadworkspace.h"

TaskLoadWorkspace::TaskLoadWorkspace(ActorPtr actor, Actor::TCP side, QString fileName)
{
    this->actor = actor;
    this->side = side;
    this->fileName = fileName;
}

void TaskLoadWorkspace::work()
{
    loadWorkspace();
}

std::string TaskLoadWorkspace::getDescription()
{
    return "Load Workspace (" + fileName.toStdString() + ")";
}

void TaskLoadWorkspace::loadWorkspace()
{
    if (!fileName.isEmpty())
    {
        actor->loadWorkspace(side, fileName);
//        actor->getWorkspace(side)->print();
    }
}
