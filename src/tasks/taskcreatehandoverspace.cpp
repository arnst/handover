#include "taskcreatehandoverspace.h"
#include "../settings.h"
#include "../handoveriksolver.h"

#include <VirtualRobot/ManipulationObject.h>
#include <VirtualRobot/Grasping/Grasp.h>

#include <cmath>
#include <limits.h>

TaskCreateHandoverspace::TaskCreateHandoverspace(ArmarPtr armar, Actor::TCP armarTcpSide, HumanPtr human, int graspCombinations)
    :graspCombinations(graspCombinations), interval(0, 1), mutex(new boost::mutex()), concurrencyID(1)
{
    this->armar = armar;
    this->armarTcpSide = armarTcpSide;
    this->human = human;
    this->robotHuman = human->getRobot();
    this->discrStepTrans = Settings::Handoverspace::discrStepTrans;
    this->discrStepRot = Settings::Handoverspace::discrStepRot;

    srand(time(NULL)); // to randomize grasps

    numIKSolving = 0;
    numIKSolvingSuccess = 0;
    voxelFillCount = 0;
    trackGazeDirection = true;
    isConcurrent = false;
}

void TaskCreateHandoverspace::work()
{

    bool visuState = robotHuman->getUpdateVisualizationStatus();
    robotHuman->setUpdateVisualization(false); // updating visualization is performance killer #1

    measureHumanLeft.reset(new PoseQualityRosenbaum(robotHuman->getRobotNodeSet(human->getManipLeft()->getNodeSet()->getName())));
    measureHumanRight.reset(new PoseQualityRosenbaum(robotHuman->getRobotNodeSet(human->getManipRight()->getNodeSet()->getName())));

    jointValuesLeft = robotHuman->getRobotNodeSet(human->getManipLeft()->getNodeSet()->getName())->getJointValues();
    jointValuesRight = robotHuman->getRobotNodeSet(human->getManipRight()->getNodeSet()->getName())->getJointValues();

    createHandoverSpace();
    robotHuman->setUpdateVisualization(visuState);
}

void TaskCreateHandoverspace::createHandoverSpace()
{
    ManipulabilityPtr wsArmar = (armarTcpSide == Actor::LEFT_TCP) ? armar->getManipLeft() : armar->getManipRight();
    WorkspaceRepresentationPtr wsHumanLeft = human->getManipLeft();
    WorkspaceRepresentationPtr wsHumanRight = human->getManipRight();
    ManipulationObjectPtr object = armar->getObject();

    if (!wsArmar->getData()) {
        mutex->lock();
        VR_ERROR << "Task: " << getDescription() << ": No valid Workspace data found for Armar!" << endl;
        mutex->unlock();
        return;
    }   
    if (!wsHumanLeft->getData()) {
        mutex->lock();
        VR_ERROR << "Task: " << getDescription() << ": No valid Workspace data found for Human left side!" << endl;
        mutex->unlock();
        return;
    }
    if (!wsHumanRight->getData()) {
        mutex->lock();
        VR_ERROR << "Task: " << getDescription() << ": No valid Workspace data found for Human right side!" << endl;
        mutex->unlock();
        return;
    }
    if (!object) {
        mutex->lock();
        VR_ERROR << "Task: " << getDescription() << ": No valid manipulation object found!" << endl;
        mutex->unlock();
        return;
    }

    Sphere sphereObject = getSphere(object);
    Sphere sphereWsHumanLeft = getSphere(wsHumanLeft);
    Sphere sphereWsHumanRight = getSphere(wsHumanRight);

    WorkspaceRepresentationPtr handoverSpace = armar->getHandoverSpace();

    // the first thread to reach this point (if this task is used concurrently) gets the privilege to initialize an empty handover-space,
    // all other threads must wait.
    mutex->lock();
    if (!armar->getHandoverSpace()->getData())
    {
        float minB[6];
        float maxB[6];
        for (int i = 0; i < 6; i++)
        {
            minB[i] = wsArmar->getMinBound(i);
            maxB[i] = wsArmar->getMaxBound(i);
        }
        // use the same params as armars workspace
        RobotNodeSetPtr rns = wsArmar->getNodeSet();
        RobotNodePtr baseNode = rns->getKinematicRoot();
        RobotNodePtr tcpNode = rns->getTCP();

        handoverSpace->initialize(rns, discrStepTrans, discrStepRot, minB, maxB, SceneObjectSetPtr(),SceneObjectSetPtr(), baseNode, tcpNode, true);
        armar->clearCache();
    }
    mutex->unlock();

    float skippedPos = 0;
    float consideredPos = 0;
    int zStart = interval.first * handoverSpace->getNumVoxels(2);
    int zEnd = interval.second * handoverSpace->getNumVoxels(2);
    bool caching = true;

    // iterate through the freshly initialized handover-space
    for (int x = 0; x < handoverSpace->getNumVoxels(0); x++) {
        for (int y = 0; y < handoverSpace->getNumVoxels(1); y++) {
            for (int z = zStart; z < zEnd; z++) {

                // Before iterating through all orientations of the current voxel, we first check if the object is in range for the human.
                // First, we set the position of the objects bounding sphere to the current voxels global position (not pose)...
                unsigned int v[6] = {x, y, z};
                sphereObject.pos = handoverSpace->getPoseFromVoxel(v).block<3,1>(0,3);

                // ...then we check, if the objects bounding sphere touches one of the bounding spheres of the humans workspaces.
                float distLeftSquared = (sphereObject.pos - sphereWsHumanLeft.pos).squaredNorm();
                float distRightSquared = (sphereObject.pos - sphereWsHumanRight.pos).squaredNorm();
                float radiusSumLeft = sphereObject.r + sphereWsHumanLeft.r;
                float radiusSumRight = sphereObject.r + sphereWsHumanRight.r;

                if ((distLeftSquared > (radiusSumLeft * radiusSumLeft)) && (distRightSquared > (radiusSumRight * radiusSumRight))) {
                    // At this point the object is not in range for the human. We dont bother iterating through all orientations for the current voxel.
                    skippedPos++;
                    continue;
                }
                consideredPos++;

                for (int alpha = 0; alpha < handoverSpace->getNumVoxels(3); alpha++) {
                    for (int beta = 0; beta < handoverSpace->getNumVoxels(4); beta++) {
                        for (int gamma = 0; gamma < handoverSpace->getNumVoxels(5); gamma++) {

                            // get the global pose of the current voxel
                            unsigned int currentVoxel[6] = {x, y, z, alpha, beta, gamma};
                            Eigen::Matrix4f globalPose = handoverSpace->getPoseFromVoxel(currentVoxel);

                            if (wsArmar->isCovered(globalPose))
                            {
                                // we try several grasp combinations
                                for (int i = 0; i < graspCombinations; i++)
                                {
                                    Eigen::Matrix4f humanLeftGraspingPose = sampleHumanGraspingPose(globalPose, object, Actor::LEFT_TCP);
                                    unsigned char entryLeft = wsHumanLeft->getEntry(humanLeftGraspingPose);
                                    if (entryLeft > 0)
                                    {
                                        bool success = solveIK(robotHuman->getRobotNodeSet(wsHumanLeft->getNodeSet()->getName()), humanLeftGraspingPose);

                                        if (success)
                                        {
                                            measureHumanLeft->setObstacleDistanceVector(getObstacleDistanceVector(Actor::LEFT_TCP));
                                            entryLeft = (unsigned char)(measureHumanLeft->getPoseQuality() * (float)UCHAR_MAX + 0.5f);
//                                            entryLeft = (unsigned char)((float)entryLeft * measureHumanLeft->getPoseQuality());

                                            if (trackGazeDirection)
                                            {
                                                float visibilityLeft = getVisibility(humanLeftGraspingPose.block<3,1>(0,3));
                                                entryLeft *= visibilityLeft;
                                                entryLeft = (entryLeft > 0) ? entryLeft : 1;
                                            }
                                        }
                                        else
                                        {
                                            entryLeft = 1;
                                        }

                                    }

                                    Eigen::Matrix4f humanRightGraspingPose = sampleHumanGraspingPose(globalPose, object, Actor::RIGHT_TCP);
                                    unsigned char entryRight= wsHumanRight->getEntry(humanRightGraspingPose);
                                    if (entryRight > 0)
                                    {
                                        bool success = solveIK(robotHuman->getRobotNodeSet(wsHumanRight->getNodeSet()->getName()), humanRightGraspingPose);

                                        if (success)
                                        {
                                            measureHumanRight->setObstacleDistanceVector(getObstacleDistanceVector(Actor::RIGHT_TCP));
                                            entryRight = (unsigned char)(measureHumanRight->getPoseQuality() * (float)UCHAR_MAX + 0.5f);
//                                            entryRight = (unsigned char)((float)entryRight * measureHumanRight->getPoseQuality());

                                            if (trackGazeDirection)
                                            {
                                                float visibilityRight = getVisibility(humanRightGraspingPose.block<3,1>(0,3));
                                                entryRight *= visibilityRight;
                                                entryRight = (entryRight > 0) ? entryRight : 1;
                                            }
                                        }
                                        else
                                        {
                                            entryRight = 1;
                                        }
                                    }

                                    unsigned char entryHS = (entryLeft > entryRight) ? entryLeft : entryRight;
//                                    entryHS *= wsArmar->getEntry(globalPose);
                                    mutex->lock();
                                    if (handoverSpace->getVoxelEntry(x, y, z, alpha, beta, gamma) < entryHS) {
                                        handoverSpace->setVoxelEntry(currentVoxel, entryHS);
                                        voxelFillCount++;
                                        if (caching)
                                            caching = armar->cacheHSPose(globalPose);
                                    }
                                    mutex->unlock();
                                }
                            }

                        } // gamma
                    } // beta
                } // alpha
            } // z
        } // y
    } // x

    robotHuman->getRobotNodeSet(human->getManipLeft()->getNodeSet()->getName())->setJointValues(jointValuesLeft);
    robotHuman->getRobotNodeSet(human->getManipRight()->getNodeSet()->getName())->setJointValues(jointValuesRight);
}

Eigen::Matrix4f TaskCreateHandoverspace::sampleHumanGraspingPose(Eigen::Matrix4f& armarGraspingPose, ManipulationObjectPtr object, Actor::TCP humanTcpSide)
{
    Eigen::Matrix4f humanGraspingPose = armarGraspingPose;
    if (object)
    {   // TODO replace with Actor::getEEF(...)
        std::string eefArmar = (armarTcpSide == Actor::LEFT_TCP) ? "Hand L" : "Hand R";
        std::string eefHuman = (humanTcpSide == Actor::LEFT_TCP) ? "Hand L" : "Hand R";

        GraspPtr graspArmar = armar->getGrasp();
        // sample a random grasp if armar has no previously assigned grasp
        if (!graspArmar)
        {
            GraspSetPtr graspSetArmar = object->getGraspSet(armar->getRobot()->getType(), eefArmar);
            int indexArmar = rand() %  graspSetArmar->getSize();
            graspArmar = graspSetArmar->getGrasp(indexArmar);
        }

        GraspSetPtr graspSetHuman = object->getGraspSet(human->getRobot()->getType(), eefHuman);
        int indexHuman = rand() % graspSetHuman->getSize();
        GraspPtr graspHuman = graspSetHuman->getGrasp(indexHuman);

        Eigen::Matrix4f objectPose = graspArmar->getObjectTargetPoseGlobal(armarGraspingPose);
        humanGraspingPose = graspHuman->getTcpPoseGlobal(objectPose);
    }

    return humanGraspingPose;
}

TaskCreateHandoverspace::Sphere TaskCreateHandoverspace::getSphere(ManipulationObjectPtr object)
{
    BoundingBox bb = object->getCollisionModel()->getBoundingBox();
    Eigen::Vector3f max = bb.getMax();
    Eigen::Vector3f min = bb.getMin();
    float maxDist = 0;

    for (int i = 0; i < 3; i++)
    {
        float dist = fabs(max(i) - min(i));
        if (dist > maxDist) {
            maxDist = dist;
        }
    }

    Eigen::Vector3f pos = object->getGlobalPose().block<3,1>(0,3);
    float r = maxDist / 2;
    Sphere sphere;
    sphere.pos = pos;
    sphere.r = r;

    return sphere;
}

TaskCreateHandoverspace::Sphere TaskCreateHandoverspace::getSphere(WorkspaceRepresentationPtr ws)
{
    float minBounds[3];
    float maxBounds[3];
    float maxDist = 0;
    Eigen::Vector3f pos;

    // get the central pos and radius of the given workspace
    for (int i = 0; i < 3 ; i++)
    {
        minBounds[i] = ws->getMinBound(i);
        maxBounds[i] = ws->getMaxBound(i);

        float dist = fabs(maxBounds[i] - minBounds[i]);
        pos(i) = minBounds[i] + (dist / 2);

        if (dist > maxDist) {
            maxDist = dist;
        }
    }

    float r = maxDist / 2;
    Sphere sphere;
    // min/max bounds were given in local coords
    ws->toGlobalVec(pos);
    sphere.pos = pos;
    sphere.r = r;

    return sphere;
}

float TaskCreateHandoverspace::getVisibility(Eigen::Vector3f globalPos)
{
    float visibility = 0;

    Eigen::Vector3f faceToVoxel = globalPos - human->getGazeOriginGlobal();
    float cosAlpha = (faceToVoxel.dot(human->getGazeDirection())) / (faceToVoxel.norm() * human->getGazeDirection().norm());
    visibility = std::max(cosAlpha, 0.0f);

    return visibility;
}

bool TaskCreateHandoverspace::solveIK(RobotNodeSetPtr rns, Eigen::Matrix4f targetPose)
{
    bool success = false;
    HandoverIKSolverPtr ikSolver(new HandoverIKSolver(rns, JacobiProvider::eTranspose));

    ikSolver->setupJacobian(0.2f, 50);
    ikSolver->setMaximumError(discrStepTrans, discrStepRot);

    success = ikSolver->solve(targetPose, IKSolver::All, 4);
    if (success) {
        numIKSolvingSuccess++;
    }
    numIKSolving++;

    return success;
}

Eigen::Vector3f TaskCreateHandoverspace::getObstacleDistanceVector(Actor::TCP side)
{
    Eigen::Vector3f minDistVector = Eigen::Vector3f::Zero();

    // here we determine the obstacle distance vector
    SceneObjectSetPtr staticColModel = robotHuman->getRobotNodeSet("LegsHipTorsoHead_ColModel");
    SceneObjectSetPtr dynamicColModel = robotHuman->getRobotNodeSet(side == Actor::LEFT_TCP ? "LeftHand_ColModel" : "RightHand_ColModel");
    RobotNodeSetPtr rns = robotHuman->getRobotNodeSet(side == Actor::LEFT_TCP ? "LeftHand_ColModel" : "RightHand_ColModel");

    int id1;
    int id2;
    Eigen::Vector3f p1;
    Eigen::Vector3f p2;
    float d = staticColModel->getCollisionChecker()->calculateDistance(staticColModel, dynamicColModel, p1, p2, &id1, &id2);
    Eigen::Matrix4f obstDistPos1 = Eigen::Matrix4f::Identity();
    Eigen::Matrix4f obstDistPos2 = Eigen::Matrix4f::Identity();
    obstDistPos1.block(0, 3, 3, 1) = p1;
    obstDistPos2.block(0, 3, 3, 1) = p2;

    // transform to tcp
    Eigen::Matrix4f p1_tcp = rns->getTCP()->toLocalCoordinateSystem(obstDistPos1);
    Eigen::Matrix4f p2_tcp = rns->getTCP()->toLocalCoordinateSystem(obstDistPos2);
    minDistVector = p1_tcp.block(0, 3, 3, 1) - p2_tcp.block(0, 3, 3, 1);

    // cout << "minDistVector = " << minDistVector.norm() << (side == Actor::LEFT_TCP ? "   Left" : "   Right") << endl;
    return minDistVector;
}

std::string TaskCreateHandoverspace::getDescription()
{
    float ikSolverSuccessRate = (float) numIKSolvingSuccess / numIKSolving;
    if (qIsNaN(ikSolverSuccessRate)) {
        ikSolverSuccessRate = 0;
    }
    return "Create Handover-Space: Interval = (" + QString::number(interval.first, 'f', 2).toStdString() + "," + QString::number(interval.second, 'f', 2).toStdString()
            + "), ID = " + QString::number(concurrencyID).toStdString()
            + ", Voxel-Fill-Count = " + QString::number(voxelFillCount).toStdString()
            + ", IK Succes-rate = " + QString::number(ikSolverSuccessRate, 'f', 2).toStdString();
}

void TaskCreateHandoverspace::setConcurrent(QPair<float, float> interval, boost::shared_ptr<boost::mutex> mutex, RobotPtr clonedRobot)
{
    this->mutex = mutex;
    this->interval = interval;
    concurrencyID = (interval.second / (interval.second - interval.first)) - 0.5f;
    robotHuman = clonedRobot;
    isConcurrent = true;
}

