#ifndef SAVEWORKSPACEWORKER_H
#define SAVEWORKSPACEWORKER_H

#include "task.h"
#include "../actor.h"

/**
 * Saves the manipulabilty data of the specified Actor to a file.
 */
class TaskSaveWorkspace : public Task
{
public:
    TaskSaveWorkspace(ActorPtr actor, Actor::TCP side, QString fileName);
    virtual void work();
    virtual std::string getDescription();

private:
    ActorPtr actor;
    Actor::TCP side;
    QString fileName;

    void saveWorkspace();
};

typedef boost::shared_ptr<TaskSaveWorkspace> TaskSaveWorkspacePtr;

#endif // SAVEWORKSPACEWORKER_H
