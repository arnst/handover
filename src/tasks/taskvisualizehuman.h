#ifndef HUMANVISUALIZATIONWORKER_H
#define HUMANVISUALIZATIONWORKER_H

#include "task.h"
#include "../human.h"

#include <QObject>

using namespace VirtualRobot;

/**
 * Visualizes the specified Human with his workspaces, etc.
 */
class TaskVisualizeHuman : public QObject, public Task
{
    Q_OBJECT

public:
    TaskVisualizeHuman(HumanPtr human, float cutZWsLeft = 1, float cutZWsRight = 1);
    virtual void work();
    virtual std::string getDescription();

    void setCutZ(float wsLeft, float wsRight);

signals:
    void resultReady(HumanPtr human, SoNode* wsLeft, SoNode* wsRight);

private:
    HumanPtr human;
    float cutZWsLeft;
    float cutZWsRight;

    void buildVisualization();
};

#endif // HUMANVISUALIZATIONWORKER_H
