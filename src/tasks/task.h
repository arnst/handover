#ifndef WORKER_H
#define WORKER_H

#include <boost/shared_ptr.hpp>

/**
 * Can be fed to the Factory class to get some work done in parallel.
 */
class Task
{
public:
    Task();
    virtual void work() = 0;
    virtual std::string getDescription() = 0;
};

typedef boost::shared_ptr<Task> TaskPtr;

#endif // WORKER_H
