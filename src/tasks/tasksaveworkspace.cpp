#include "tasksaveworkspace.h"

TaskSaveWorkspace::TaskSaveWorkspace(ActorPtr actor, Actor::TCP side, QString fileName)
{
    this->actor = actor;
    this->side = side;
    this->fileName = fileName;
}

void TaskSaveWorkspace::work()
{
    saveWorkspace();
}

std::string TaskSaveWorkspace::getDescription()
{
    return "Save Workspace (" + fileName.toStdString() + ")";
}

void TaskSaveWorkspace::saveWorkspace()
{
    if (!fileName.isEmpty()) actor->saveWorkspace(side, fileName);
}
