#ifndef HUMAN_H
#define HUMAN_H

#include "actor.h"

using namespace VirtualRobot;

class Human : public Actor
{
public:
    Human(SoSeparator* sceneSep);
    ~Human();
    virtual void loadRobot(QString fileName);
    virtual QString getName();
    virtual std::string getEEF(Actor::TCP side);
    virtual void visualize();
    virtual void visualize(SoNode *wsLeft, SoNode *wsRight);
    virtual void visualizeGaze();
    void setGazeLength(int mm);
    int getGazeLength();
    Eigen::Vector3f getGazeOriginGlobal();
    /** returns normalized direction **/
    Eigen::Vector3f getGazeDirection();

    virtual void enableVisualization(bool enable);

private:
    SoSeparator* gazeDirectionSep;
    int gazeLength; // in mm

    void adjustHead();
    void kinematicRootWorkaround();    
};

typedef boost::shared_ptr<Human> HumanPtr;

#endif // HUMAN_H
