#include "human.h"

#include <VirtualRobot/IK/PoseQualityManipulability.h>
#include <VirtualRobot/Visualization/CoinVisualization/CoinVisualizationFactory.h>
#include <VirtualRobot/Visualization/CoinVisualization/CoinVisualization.h>
#include <VirtualRobot/XML/RobotIO.h>

#include <MMM/Model/ModelProcessorFactoryWinter.h>
#include <MMM/Model/ModelProcessorWinter.h>
#include <MMM/Model/ModelReaderXML.h>
#include <MMMSimoxTools/MMMSimoxTools.h>

using namespace VirtualRobot;

Human::Human(SoSeparator *sceneSep) : Actor(sceneSep)
{
    gazeDirectionSep = new SoSeparator();
    gazeDirectionSep->ref();
    sceneSep->addChild(gazeDirectionSep);
    gazeLength = 0;
}

Human::~Human()
{
    gazeDirectionSep->unref();
}

void Human::loadRobot(QString fileName)
{
    // create modelprocessor for Winter model
    MMM::ModelProcessorFactoryWinterPtr mpFactory(new MMM::ModelProcessorFactoryWinter());
    MMM::ModelProcessorPtr modelProcessor = mpFactory->createModelProcessor();
    MMM::ModelProcessorWinterPtr mpWinter = boost::dynamic_pointer_cast<MMM::ModelProcessorWinter>(modelProcessor);

    // parse model file
    MMM::ModelReaderXMLPtr reader(new MMM::ModelReaderXML());
    MMM::ModelPtr mmmOrigModel = reader->loadModel(fileName.toStdString());

    // create scaled model
    mpWinter->setup(1.74f,72);
    MMM::ModelPtr mmmModel = modelProcessor->convertModel(mmmOrigModel);

    // convert model to robot
    robot.reset();
    robot = MMM::SimoxTools::buildModel(mmmModel);
    kinematicRootWorkaround();
    adjustHead();
    startConfig = robot->getConfig();

    manipRight.reset(new Manipulability(robot));
    manipLeft.reset(new Manipulability(robot));

//    RobotIO::saveXML(robot, "mmm_174.xml", "/home/harry/workspace/handover/data/MMMData/Model/Winter_174", "models", true, true, true, true);
//    Actor::loadRobot("MMMData/Model/Winter_174/mmm_174.xml");
}

void Human::visualize()
{
    Actor::visualize();
    visualizeGaze();
}

void Human::visualize(SoNode *wsLeft, SoNode *wsRight)
{
    Actor::visualize(wsLeft, wsRight);
    visualizeGaze();
}

void Human::visualizeGaze()
{
    gazeDirectionSep->removeAllChildren();
    Eigen::Matrix4f arrowGlobalPose = Eigen::Matrix4f::Identity();
    arrowGlobalPose.block<3,1>(0,3) = getGazeOriginGlobal();
    gazeDirectionSep->addChild(CoinVisualizationFactory::getMatrixTransformScaleMM2M(arrowGlobalPose));
    gazeDirectionSep->addChild(CoinVisualizationFactory::CreateArrow(getGazeDirection(), gazeLength, 5, CoinVisualizationFactory::Color::Red()));
}

void Human::setGazeLength(int mm)
{
    if (gazeLength != mm)
    {
        gazeLength = mm;

        gazeDirectionSep->removeAllChildren();
        Eigen::Matrix4f arrowGlobalPose = Eigen::Matrix4f::Identity();
        arrowGlobalPose.block<3,1>(0,3) = getGazeOriginGlobal();
        gazeDirectionSep->addChild(CoinVisualizationFactory::getMatrixTransformScaleMM2M(arrowGlobalPose));
        gazeDirectionSep->addChild(CoinVisualizationFactory::CreateArrow(getGazeDirection(), gazeLength, 5, CoinVisualizationFactory::Color::Red()));
    }
}

int Human::getGazeLength()
{
    return gazeLength;
}

QString Human::getName()
{
    return "Human";
}

std::string Human::getEEF(Actor::TCP side)
{
    std::string eef = "";
    if (side == Actor::LEFT_TCP) {
        eef = "Hand L";
    }
    else if (side == Actor::RIGHT_TCP) {
        eef = "Hand R";
    }
    return eef;
}

Eigen::Vector3f Human::getGazeOriginGlobal()
{
    return robot->getRobotNode("MidHeadSegment_joint")->getGlobalPose().block<3,1>(0,3);
}

Eigen::Vector3f Human::getGazeDirection()
{
    Eigen::Vector3f gazeDirection;
    RobotNodePtr midHeadNode = robot->getRobotNode("MidHeadSegment_joint");
    RobotNodePtr leftEyeNode = robot->getRobotNode("LeftEyeSegmentY_joint");
    RobotNodePtr rightEyeNode = robot->getRobotNode("RightEyeSegmentY_joint");

    Eigen::Vector3f poseBetweenEyes = leftEyeNode->getGlobalPose().block<3,1>(0,3) +
            ((rightEyeNode->getGlobalPose().block<3,1>(0,3) - leftEyeNode->getGlobalPose().block<3,1>(0,3)) * 0.5f);

    gazeDirection = poseBetweenEyes - midHeadNode->getGlobalPose().block<3,1>(0,3);
    gazeDirection.normalize();

    return gazeDirection;
}

void Human::adjustHead()
{
    RobotNodePtr bunx = robot->getRobotNode("BLNx_joint");
    bunx->setJointValue(bunx->getJointLimitLo() + (std::fabs(bunx->getJointLimitHi() - bunx->getJointLimitLo()) / 2));

    RobotNodePtr blnx = robot->getRobotNode("BLNx_joint");
    blnx->setJointValue(blnx->getJointLimitLo() + (std::fabs(blnx->getJointLimitHi() - blnx->getJointLimitLo()) / 2));
}

void Human::kinematicRootWorkaround()
{
    robot->getRobotNodeSet("TorsoLeftArm")->setKinematicRoot(robot->getRobotNode("BTSegment_joint"));
//    robot->getRobotNodeSet("TorsoLeftArm_Bodies")->setKinematicRoot(robot->getRobotNode("root_joint"));

    robot->getRobotNodeSet("TorsoRightArm")->setKinematicRoot(robot->getRobotNode("BTSegment_joint"));
//    robot->getRobotNodeSet("TorsoRightArm_Bodies")->setKinematicRoot(robot->getRobotNode("root_joint"));

    robot->getRobotNodeSet("LeftArm")->setKinematicRoot(robot->getRobotNode("collarSegment_joint"));

    robot->getRobotNodeSet("RightArm")->setKinematicRoot(robot->getRobotNode("collarSegment_joint"));

//    robot->getRobotNodeSet("LeftHand_ColModel")->setKinematicRoot(robot->getRobotNode("root_joint"));
//    robot->getRobotNodeSet("RightHand_ColModel")->setKinematicRoot(robot->getRobotNode("root_joint"));
}


void Human::enableVisualization(bool enable)
{
    sceneSep->removeAllChildren();

    if (enable)
    {
        sceneSep->addChild(robotSep);
        sceneSep->addChild(manipRightSep);
        sceneSep->addChild(manipLeftSep);
        sceneSep->addChild(gazeDirectionSep);
    }
}
