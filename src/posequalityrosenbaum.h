#ifndef POSEQUALITYROSENBAUM_H
#define POSEQUALITYROSENBAUM_H

#include <VirtualRobot/IK/PoseQualityMeasurement.h>

#include <boost/shared_ptr.hpp>

using namespace VirtualRobot;

class PoseQualityRosenbaum : public PoseQualityMeasurement
{
public:
    PoseQualityRosenbaum(RobotNodeSetPtr rns);

    /**
        The main method for determining the pose quality.
        The current configuration of the corresponding RNS is analyzed and the quality is returned.
    */
    virtual float getPoseQuality();

private:
    std::vector<float> refJointValues;
    Eigen::Matrix4f refTcpPose; // global
    float maxTravelCost;
    float maxSafetyCost;

    float getSpatialErrorCost();
    float getTravelCost();
    float getMaxTravelCost();
    float getSafetyCost();
};

typedef boost::shared_ptr<PoseQualityRosenbaum> PoseQualityRosenbaumPtr;

#endif // POSEQUALITYROSENBAUM_H
