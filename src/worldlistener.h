#ifndef WORLDLISTENER_H
#define WORLDLISTENER_H

#include <boost/shared_ptr.hpp>

class World;

class WorldListener
{
public:
    virtual void onWorldUpdate() = 0;
    virtual void onWorldLock() = 0;
    virtual void onWorldUnlock() = 0;

    void setWorld(boost::shared_ptr<World> world) {this->world = world;}

protected:
    boost::shared_ptr<World> world;
};

#endif // WORLDLISTENER_H
