#ifndef POSEQUALITYCOMFORT_H
#define POSEQUALITYCOMFORT_H

#include <VirtualRobot/IK/PoseQualityMeasurement.h>
#include <VirtualRobot/IK/PoseQualityExtendedManipulability.h>

#include <QtGui>

using namespace VirtualRobot;

class PoseQualityComfort : public PoseQualityMeasurement
{
public:
    PoseQualityComfort(RobotNodeSetPtr rns, RobotNodeSetPtr rnsBodies);

    /**
        The main method for determining the pose quality.
        The current configuration of the corresponding RNS is analyzed and the quality is returned.
    */
    virtual float getPoseQuality();

    /**
        See PoseQualityExtendedManipulability::considerObstacles(bool enable, float alpha, float beta))
    */
    void considerObstacles(bool enable, float alpha  = 2, float beta = 2);

    virtual void setObstacleDistanceVector(const Eigen::Vector3f &directionSurfaceToObstance);
    void setLogWidget(QListWidget *logWidget);  

private:
    RobotNodeSetPtr rnsBodies;
    QListWidget *logWidget;
    bool logging;
    float maxTorque;
    PoseQualityExtendedManipulabilityPtr extManip;
    float obstacleAlpha;
    float obstacleBeta;

    float getNetTorque();
    void checkForMaxTorque(int steps);
    void randomizeRnsConfig();
    bool isParent(RobotNodePtr child, RobotNodePtr parent);
    void log(std::string text, int lvl = 0);
    void highlight(std::string text, int lvl = 0);
};

typedef boost::shared_ptr<PoseQualityComfort> PoseQualityComfortPtr;

#endif // POSEQUALITYCOMFORT_H
