#include "handoveriksolver.h"

#include <VirtualRobot/RobotConfig.h>

HandoverIKSolver::HandoverIKSolver(RobotNodeSetPtr rns, JacobiProvider::InverseJacobiMethod invJacMethod)
    : GenericIKSolver(rns, invJacMethod)
{
    jointWeights = Eigen::VectorXf(rns->getSize());
    for (int i = 0; i < rns->getSize(); i++) {
        jointWeights(i) = 1;
    }

    srand(time(NULL)); // to randomize rns config
}

bool HandoverIKSolver::solve(const Eigen::Matrix4f &globalPose, IKSolver::CartesianSelection selection, int maxLoops)
{
    jacobian->setGoal(globalPose, tcp, selection, maxErrorPositionMM, maxErrorOrientationRad);
    jacobian->checkImprovements(true);

    RobotConfigPtr bestConfig(new RobotConfig(rns->getRobot(), "bestConfig"));
    rns->getJointValues(bestConfig);
    float bestError = jacobian->getMeanErrorPosition();

    // first check reachability
    if (!checkReachable(globalPose))
    {
        return false;
    }

    // first run: start with current joint angles
    if (trySolve())
    {
        return true;
    }

    // if here we failed
    for (int i = 1; i < maxLoops; i++)
    {
        randomizeJointsWeighted();

        if (trySolve())
        {
            return true;
        }

        float currentError = jacobian->getMeanErrorPosition();

        if (currentError < bestError)
        {
            rns->getJointValues(bestConfig);
            bestError = jacobian->getMeanErrorPosition();
        }
    }

    // set best config
    rns->setJointValues(bestConfig);
    return false;
}

void HandoverIKSolver::setJointWeights(Eigen::VectorXf weights)
{
    this->jointWeights = weights;
    jacobian->setJointWeights(weights);
}

void HandoverIKSolver::randomizeJointsWeighted()
{
    float minJointValue = 0;
    float maxJointValue = 0;
    std::vector<float> v(rns->getSize());

    for (int i = 0; i < rns->getSize(); i++)
    {
        minJointValue = (*rns)[i]->getJointLimitLo();
        maxJointValue = (*rns)[i]->getJointLimitHi();
        float r = ((float) rand() / (float) RAND_MAX) - 0.5f;
        v[i] = minJointValue + ((maxJointValue - minJointValue) * r);
        v[i] = jointWeights(i) * v[i] + (1 - jointWeights(i)) * (*rns)[i]->getJointValue();
    }

    rns->setJointValues(v);

    if (translationalJoint)
    {
        translationalJoint->setJointValue(initialTranslationalJointValue);
    }
}
