#ifndef POSEQUALITYREACHABILITY_H
#define POSEQUALITYREACHABILITY_H

#include <VirtualRobot/IK/PoseQualityMeasurement.h>

using namespace VirtualRobot;

class PoseQualityReachability : public PoseQualityMeasurement
{
public:
    PoseQualityReachability(RobotNodeSetPtr rns);

    /**
        The main method for determining the pose quality.
        The current configuration of the corresponding RNS is analyzed and the quality is returned.
    */
    virtual float getPoseQuality();
};

typedef boost::shared_ptr<PoseQualityReachability> PoseQualityReachabilityPtr;

#endif // POSEQUALITYREACHABILITY_H
