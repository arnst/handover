#ifndef ACTOR_H
#define ACTOR_H

#include <VirtualRobot/Robot.h>
#include <VirtualRobot/Workspace/Manipulability.h>
#include <VirtualRobot/Visualization/CoinVisualization/CoinVisualization.h>

#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoNode.h>

#include <QObject>
#include <QString>

using namespace VirtualRobot;

class Actor
{

public:
    enum TCP {
        LEFT_TCP,
        RIGHT_TCP,
        BOTH_TCP,
        NO_TCP
    };

    Actor(SoSeparator* sceneSep);
    virtual ~Actor();
    RobotPtr getRobot();
    void setManipulability(Actor::TCP side, ManipulabilityPtr manip);
    ManipulabilityPtr getManipulability(Actor::TCP side);
    ManipulabilityPtr getManipLeft();
    ManipulabilityPtr getManipRight();
    virtual void transformRobot(float transX, float transY, float transZ, float rotX, float rotY, float rotZ);
    void resetWorkspace();
    void enableVisualizationWS(bool enable);
    bool isEnabledVisualizationWS();
    void loadWorkspace(Actor::TCP side, QString fileName);
    void loadWorkspaceLeft(QString fileName);
    void loadWorkspaceRight(QString fileName);
    void saveWorkspace(Actor::TCP side, QString fileName);
    void saveWorkspaceLeft(QString fileName);
    void saveWorkspaceRight(QString fileName);
    void smoothWorkspace(unsigned int minNeighbors);

    virtual void loadRobot(QString fileName);
    virtual void visualize();
    virtual void visualize(SoNode *wsLeft, SoNode *wsRight);
    virtual QString getName() = 0;
    virtual void resetRobotConfig();

    virtual std::string getEEF(Actor::TCP side) = 0;
    virtual void enableVisualization(bool enable) = 0;

protected:
    RobotPtr robot;
    ManipulabilityPtr manipLeft;
    ManipulabilityPtr manipRight;
    RobotConfigPtr startConfig;

    SoSeparator* sceneSep;
    SoSeparator* robotSep;
    SoSeparator* manipRightSep;
    SoSeparator* manipLeftSep;

    SoNode* manipRightVisualization;
    SoNode* manipLeftVisualization;
    boost::shared_ptr<CoinVisualization> robotVisualization;
    bool visualizeWS; // visualization Handover-space

    Eigen::Matrix4f transform(const Eigen::Matrix4f &pose, float transX, float transY, float transZ, float rotX, float rotY, float rotZ);
};

typedef boost::shared_ptr<Actor> ActorPtr;

#endif // ACTOR_H
