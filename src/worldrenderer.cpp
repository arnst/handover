#include "worldrenderer.h"
#include "tasks/taskexecutor.h"
#include "tasks/taskvisualizehuman.h"
#include "tasks/taskvisualizearmar.h"
#include "world.h"

WorldRenderer::WorldRenderer(SoSeparator* sceneSep)
{
    this->sceneSep = sceneSep;

    wsHumanHeightLeft = 100;    // TODO change to 1 (for all)
    wsHumanHeightRight = 100;
    wsArmarHeightLeft = 100;
    wsArmarHeightRight = 100;
    hsHeight = 100;

    // for visualizationtasks in queued connections
    qRegisterMetaType<HumanPtr>("HumanPtr");
    qRegisterMetaType<ArmarPtr>("ArmarPtr");

    initSceneGraph();
}

void WorldRenderer::setWorld(boost::weak_ptr<World> world)
{
    this->world = world;
}

void WorldRenderer::initSceneGraph()
{
    sceneSep->ref();
    environmentSep = new SoSeparator();
    sceneSep->addChild(environmentSep);

    handoverPoseSep = new SoSeparator();
    sceneSep->addChild(handoverPoseSep);

    ladderSep = new SoSeparator();
    sceneSep->addChild(ladderSep);
}

boost::shared_ptr<World> WorldRenderer::getWorld()
{
    WorldPtr w = world.lock();
    if (!w)
    {
        VR_ERROR << "World is NULL!" << endl;
    }
    return w;
}

void WorldRenderer::render()
{
    renderArmar();
    renderHuman();
    //renderHandoverPose(); // this causes a veeeeery ugly bug atm.
}

void WorldRenderer::renderArmar()
{
    WorldPtr w = getWorld();

    TaskExecutor* factory = new TaskExecutor();
    ArmarPtr armar = w->getArmar();

    boost::shared_ptr<TaskVisualizeArmar> taskArmar(new TaskVisualizeArmar(armar));
    taskArmar->setCutZ(wsArmarHeightLeft / 100.0, wsArmarHeightRight / 100.0, hsHeight / 100.0);
    factory->addTask(taskArmar);

    connect(taskArmar.get(), SIGNAL(resultReady(ArmarPtr, SoNode*, SoNode*, SoNode*, SoNode*)), this, SLOT(handleResultVisualizationTask(ArmarPtr,SoNode*,SoNode*,SoNode*,SoNode*)));

    connect(factory, SIGNAL(finished()), factory, SLOT(deleteLater()));
    factory->start();
}

void WorldRenderer::renderHuman()
{
    WorldPtr w = getWorld();

    TaskExecutor* factory = new TaskExecutor();
    HumanPtr human = w->getHuman();

    boost::shared_ptr<TaskVisualizeHuman> taskHuman(new TaskVisualizeHuman(human));
    taskHuman->setCutZ(wsHumanHeightLeft / 100.0, wsHumanHeightRight / 100.0);
    factory->addTask(taskHuman);

    connect(taskHuman.get(), SIGNAL(resultReady(HumanPtr, SoNode*, SoNode*)), this, SLOT(handleResultVisualizationTask(HumanPtr,SoNode*,SoNode*)));

    connect(factory, SIGNAL(finished()), factory, SLOT(deleteLater()));
    factory->start();
}

void WorldRenderer::renderHandoverPose()
{
    WorldPtr w = getWorld();
    Eigen::Matrix4f pose = w->getHandoverPose();

    handoverPoseSep->removeAllChildren();

    if (pose != Eigen::Matrix4f::Identity())
    {
        SoMaterial* matBody = new SoMaterial;
        matBody->diffuseColor.setValue(0, 1, 0);
        matBody->ambientColor.setValue(0, 1, 0);
        matBody->transparency.setValue(0);
        SoNode* n = CoinVisualizationFactory::CreateEllipse(0.05, 0.05, 0.05, matBody, false, 0, 0);
        handoverPoseSep->addChild(CoinVisualizationFactory::getMatrixTransformScaleMM2M(pose));
        handoverPoseSep->addChild(n);
    }
}

void WorldRenderer::handleResultVisualizationTask(HumanPtr human, SoNode *wsLeft, SoNode *wsRight)
{
    human->visualize(wsLeft, wsRight);
}

void WorldRenderer::handleResultVisualizationTask(ArmarPtr armar, SoNode *wsLeft, SoNode *wsRight, SoNode *hs, SoNode *object)
{
    armar->visualize(wsLeft, wsRight, hs, object);
}
